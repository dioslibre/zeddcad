﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Engine.Application;
using Engine.Data.Model;
using Engine.Data.View;
using Engine.Documents;
using Engine.Properties;
using Engine.Utils;
using NPOI.XSSF.UserModel;
using SQLite;
using App = Autodesk.AutoCAD.ApplicationServices.Application;
using PropertyInfo = Engine.Data.Model.PropertyInfo;

// ReSharper disable FunctionComplexityOverflow

namespace Engine.Data
{
    public enum XlsxColumn
    {
        GoldenId,
        PropertyId,
        OwnersFr,
        NameAr,
        NameFr,
        AdresseAr,
        AdresseFr,
        Mappe,
        Contenance,
        Consistance,
        Observations,
        Requisition,
        Indice
    }

    public static class Controller
    {
        #region Parse Html

        //public static bool LoadGpsPoints(string html, string reference)
        //{
        //    var document = App.DocumentManager.MdiActiveDocument;
        //    var db = document.Database;
        //    using (document.LockDocument())
        //    {
        //        using (var transaction = db.TransactionManager.StartOpenCloseTransaction())
        //        {
        //            var project = ProjectInfo.LoadFrom(db);
        //            var sqlite = new SQLiteConnection(project.DbPath);
        //            sqlite.BeginTransaction();

        //            var msid = SymbolUtilityServices.GetBlockModelSpaceId(db);
        //            var modelSpace = (BlockTableRecord) transaction.GetObject(msid, OpenMode.ForWrite);

        //            var htmlDocument = new HtmlDocument();
        //            TextReader tr = new StreamReader(html);
        //            try
        //            {
        //                htmlDocument.Load(tr);

        //                var table = htmlDocument.DocumentNode
        //                    .SelectNodes("//table")
        //                    .Where(t => t.Attributes.Contains("borderColor"))
        //                    .ElementAt(1);

        //                var rows = table.SelectNodes(".//tr").Skip(1).ToList();
        //                using (var lom = new LongOperationManager("Lecture du rappart GPS : "))
        //                {
        //                    lom.SetTotalOperations(rows.Count());
        //                    foreach (var row in rows)
        //                    {
        //                        var col = row.SelectNodes(".//td");
        //                        var info = new PointInfo
        //                        {
        //                            PointId = long.Parse((col[1].InnerText + col[3].InnerText)
        //                                .Replace(",", "")
        //                                .Replace("m", "")
        //                                .Replace(" ", "")),

        //                            Name = col[0].InnerText.Trim(),

        //                            X = col[1].InnerText
        //                                .Trim()
        //                                .Replace(",", ".")
        //                                .Replace("m", ""),

        //                            Y = col[3].InnerText
        //                                .Trim()
        //                                .Replace(",", ".")
        //                                .Replace("m", ""),
        //                        };

        //                        sqlite.Insert(info, typeof (PointInfo));

        //                        var point = new Point3d(double.Parse(info.X), double.Parse(info.Y), 0.0);
        //                        var dbpoint = new DBPoint(point);

        //                        var text = new MText
        //                        {
        //                            Contents = info.Name,
        //                            Location = point + new Vector3d(0.0, 2.0, 0.0),
        //                            Attachment = AttachmentPoint.BottomLeft,
        //                            TextHeight = 2
        //                        };

        //                        modelSpace.AppendEntity(dbpoint);
        //                        transaction.AddNewlyCreatedDBObject(dbpoint, true);
        //                        modelSpace.AppendEntity(text);
        //                        transaction.AddNewlyCreatedDBObject(text, true);
        //                    }
        //                }
        //                tr.Close();
        //            }
        //            catch (Exception exception)
        //            {
        //                tr.Close();
        //                sqlite.Rollback();
        //                sqlite.Close();
        //                transaction.Abort();
        //                transaction.Dispose();
        //                document.Editor.WriteMessage("\n" + exception);
        //                return false;
        //            }

        //            db.Pdmode = 32;
        //            db.Pdsize = 2;
        //            sqlite.Commit();
        //            sqlite.Close();
        //            transaction.Commit();
        //        }
        //    }

        //    return true;
        //}

        #endregion


        public static bool LoadDatProperties(string dat, ProjectInfo project)
        {
            var db = HostApplicationServices.WorkingDatabase;
            var sqlConnection = new SQLiteConnection(project.DbPath);

            var properties = new List<Property>();
            var parcels = new List<Parcel>();
            var points = new HashSet<Point>();

            var lineNumber = 1;
            var globalLineNumber = 0;
            var pointCount = 0;

            var parcel = new Parcel();

            #region Text Parsing

            var textreader = new StreamReader(dat);

            try
            {
                string line;
                var currentPropertyId = String.Empty;

                while (!string.IsNullOrEmpty(line = textreader.ReadLine()))
                {
                    globalLineNumber++;
                    switch (lineNumber)
                    {
                        case 1:
                            line = line
                                .ToUpper()
                                .Replace("PLLE", "");

                            line = new string(line.Where(c => char.IsLetterOrDigit(c) || c == '/').ToArray());

                            currentPropertyId = line.Contains("P")
                                ? line.Remove(line.IndexOf("P", StringComparison.InvariantCulture))
                                : line;

                            if (properties.FirstOrDefault(p => p.PropertyId == currentPropertyId) == null)
                                properties.Add(new Property
                                {
                                    PropertyId = currentPropertyId
                                });

                            if (char.IsDigit(line[0]))
                                parcels.Add(parcel = new Parcel
                                    {
                                        ParcelId = 0,
                                        PropertyId = currentPropertyId,
                                        Part = line.Contains("P") ? int.Parse("" + line.Last()) : 0
                                    });
                            else if (line.StartsWith("R") && parcels.All(pr => pr.RequisitionId != currentPropertyId))
                                parcels.Add(parcel = new Parcel
                                    {
                                        ParcelId = 0,
                                        PropertyId = currentPropertyId,
                                        Part = line.Contains("P") ? int.Parse("" + line.Last()) : 0
                                    });
                            else if (line.StartsWith("T") && parcels.All(pr => pr.TitreId != currentPropertyId))
                                parcels.Add(parcel = new Parcel
                                    {
                                        ParcelId = 0,
                                        PropertyId = currentPropertyId,
                                        Part = line.Contains("P") ? int.Parse("" + line.Last()) : 0
                                    });
                            else
                                parcels.Add(parcel = new Parcel
                                    {
                                        ParcelId = 0,
                                        PropertyId = currentPropertyId,
                                        Part = line.Contains("P") ? int.Parse("" + line.Last()) : 0
                                    });

                            lineNumber++;
                            break;
                        case 2:
                            pointCount = short.Parse(line);
                            lineNumber++;
                            break;
                        case 3:
                            pointCount--;
                            if (pointCount == 0)
                                lineNumber = -1;
                            if (pointCount >= 0)
                            {
                                var coord = line.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                if (coord.Length != 3)
                                    throw new Exception();

                                var lId = long.Parse((coord[1] + "" + coord[2]).Replace(".", ""));
                                var name = coord[0].Contains(currentPropertyId)
                                    ? coord[0].Remove(coord[0].IndexOf(currentPropertyId, StringComparison.InvariantCulture))
                                    : coord[0];

                                var point = new Point(new PointInfo
                                {
                                    PointId = lId,
                                    Name = name.IndexOf("SS", StringComparison.InvariantCulture) > -1
                                        ? name.ToUpper().Remove(name.IndexOf("SS", StringComparison.InvariantCulture))
                                        : name.ToUpper(),
                                    X = coord[1],
                                    Y = coord[2],
                                });

                                if (!parcel.Points.Contains(point))
                                    parcel.Points.Add(point);

                                points.Add(point);
                            }
                            break;
                        case -1:
                            lineNumber = 1;
                            parcel.Contenance = int.Parse(line.Trim());
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                App.DocumentManager.MdiActiveDocument.Editor
                    .WriteMessage("{0}\n{1}\nLigne : {2}", ex.Message, ex.StackTrace, globalLineNumber);
                textreader.Close();
                return false;
            }

            textreader.Close();

            #endregion

            #region Data Insertion

            var document = App.DocumentManager.MdiActiveDocument;
            using (document.LockDocument())
            {
                using (var transaction = db.TransactionManager.StartTransaction())
                {
                    sqlConnection.BeginTransaction();
                    try
                    {
                        var msId = SymbolUtilityServices.GetBlockModelSpaceId(db);
                        var modelSpace = (BlockTableRecord)transaction.GetObject(msId, OpenMode.ForWrite);
                        var styleTable = (TextStyleTable)transaction.GetObject(db.TextStyleTableId, OpenMode.ForRead);
                        var layerTable = (LayerTable)transaction.GetObject(db.LayerTableId, OpenMode.ForRead);
                        var blockTable = (BlockTable)transaction.GetObject(db.BlockTableId, OpenMode.ForRead);

                        var toBeRemoved = new List<string>();

                        var planDeMasse = new PlanDeMasse(transaction, modelSpace, layerTable, styleTable);

                        using (var lom = new LongOperationManager("Insertion des donnée dans la document : "))
                        {
                            lom.SetTotalOperations(parcels.Count + 3 * points.Count);
                            parcels.ForEach(p =>
                            {
                                if (p.Points.All(pt => !pt.Name.StartsWith("B")))
                                {
                                    toBeRemoved.Add(p.PropertyId);
                                    if (!lom.Tick()) throw new Exception("Opération interrompu. Annulation ...");
                                    return;
                                }
                                var pcl = sqlConnection.Table<ParcelInfo>().FirstOrDefault(pi => pi.PropertyId == p.PropertyId && pi.Part == p.Part);
                                if (pcl == null)
                                    planDeMasse.CreatePropertyIds(p, "-");
                                planDeMasse.CreateParcelLimites(p);
                                if (!lom.Tick()) throw new Exception("Opération interrompu. Annulation ...");
                            });

                            parcels.RemoveAll(p => toBeRemoved.Contains(p.PropertyId));
                            properties.RemoveAll(p => toBeRemoved.Contains(p.PropertyId));

                            var crlId = blockTable[Settings.Default.BlkRect];
                            foreach (var p in points)
                            {
                                if (!p.Name.StartsWith("B")) continue;
                                var blk = new BlockReference(new Point3d(p.P2D.X, p.P2D.Y, 0.0), crlId)
                                {
                                    ScaleFactors = new Scale3d(.8),
                                    LayerId = layerTable[Settings.Default.Blk]
                                };
                                modelSpace.AppendEntity(blk);
                                transaction.AddNewlyCreatedDBObject(blk, true);
                                if (!lom.Tick()) throw new Exception("Opération interrompu. Annulation ...");
                            }

                            foreach (var p in points)
                            {
                                if (sqlConnection.Get<PointInfo>(p.PointId) != null)
                                {
                                    if (!lom.Tick()) throw new Exception("Opération interrompu. Annulation ...");
                                    continue;
                                }
                                var p1 = p;
                                var sharing = parcels.Where(l => l.Points.Contains(p1));
                                var scores = sharing.Select(s => new ParcelScore(s, new Point(p))).ToArray();
                                var winner = scores.FirstOrDefault(c => Math.Abs(c.Score - scores.Max(r => r.Score)) < 0.001);
                                if (winner == null) continue;
                                var text = Functions.CreateTextForBornes(winner.Point, winner.Parcel,
                                    p.Name.StartsWith("B") ? 0.0015 : 0.001, 2000);
                                text.ColorIndex = p.Name.StartsWith("B") ? 2 : 1;
                                text.LayerId = layerTable[Settings.Default.BrnId];
                                text.TextStyleId = styleTable["PE_" + Settings.Default.BrnId];
                                modelSpace.AppendEntity(text);
                                transaction.AddNewlyCreatedDBObject(text, true);
                                if (!lom.Tick()) throw new Exception("Opération interrompu. Annulation ...");
                            }
                            if (sqlConnection.Get<PropertyInfo>(properties[0].PropertyId) == null)
                                planDeMasse.CreateLengths(parcels, lom);
                        }

                        using (var lom = new LongOperationManager("Insertion des donnée dans la base : "))
                        {
                            lom.SetTotalOperations(properties.Count + parcels.Count + points.Count);

                            properties.ForEach(p =>
                            {
                                p.Contenance = p.Parcels.Sum(pcl => pcl.Contenance);
                                if (sqlConnection.Get<PropertyInfo>(p.PropertyId) == null)
                                    sqlConnection.Insert(p, typeof(PropertyInfo));

                                if (!lom.Tick()) throw new Exception("Opération interrompu. Annulation ...");
                            });
                            parcels.ForEach(p =>
                            {
                                try
                                {
                                    var pcl = sqlConnection.Table<ParcelInfo>()
                                        .FirstOrDefault(pi => pi.PropertyId == p.PropertyId && pi.Part == p.Part);
                                    if (pcl == null)
                                    {
                                        sqlConnection.Insert(p, typeof(ParcelInfo));
                                        var lastId = sqlConnection.Table<ParcelInfo>().Last().ParcelId;
                                        p.Points.ForEach(pt => sqlConnection.Insert(new LinkInfo { ParcelId = lastId, PointId = pt.PointId }));
                                    }
                                    else
                                    {
                                        var oId = db.GetObjectId(false, new Handle(pcl.Handle), 0);
                                        var poly = transaction.GetObject(oId, OpenMode.ForWrite);
                                        if (poly != null)
                                        {
                                            poly.Erase();
                                            poly.Dispose();
                                        }
                                        var toDelete = sqlConnection.Table<LinkInfo>()
                                                .Where(l => l.ParcelId == pcl.ParcelId)
                                                .ToList();
                                        foreach (var l in toDelete)
                                        {
                                            sqlConnection.Delete<LinkInfo>(l.LinkId);
                                        }
                                        p.ParcelId = pcl.ParcelId;
                                        sqlConnection.Update(p, typeof(ParcelInfo));
                                        p.Points.ForEach(pt => sqlConnection.Insert(new LinkInfo { ParcelId = pcl.ParcelId, PointId = pt.PointId }, typeof(LinkInfo)));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    App.DocumentManager.MdiActiveDocument.Editor.WriteMessage("\n" + ex.Message + p.PropertyId);
                                    properties.RemoveAll(prt => prt.PropertyId == p.PropertyId);
                                }
                                if (!lom.Tick()) throw new Exception("Opération interrompu. Annulation ...");
                            });
                            foreach (var p in points)
                            {
                                if (sqlConnection.Get<PointInfo>(p.PointId) == null)
                                    sqlConnection.Insert(p, typeof(PointInfo));

                                if (!lom.Tick()) throw new Exception("Opération interrompu. Annulation ...");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        sqlConnection.Rollback();
                        sqlConnection.Close();
                        document.Editor.WriteMessage("\n" + ex);
                        return false;
                    }

                    sqlConnection.Commit();
                    sqlConnection.Close();
                    transaction.Commit();
                }
            }
            return true;

            #endregion
        }

        public static bool LoadEtatParcelaire(string file, Dictionary<XlsxColumn, int> columns)
        {
            var document = App.DocumentManager.MdiActiveDocument;
            var database = document.Database;
            var project = ProjectInfo.LoadFrom(database);
            var sqLite = new SQLiteConnection(project.DbPath);
            sqLite.BeginTransaction();
            var fs = new FileStream(file, FileMode.Open, FileAccess.Read);
            var wbook = new XSSFWorkbook(fs);
            var sheet = wbook.GetSheetAt(0);

            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (document.LockDocument())
                {
                    using (var transaction = database.TransactionManager.StartTransaction())
                    {
                        var properties = new List<Property>();

                        using (var lom = new LongOperationManager("Lecture de l'état parcelaire : "))
                        {
                            lom.SetTotalOperations(sheet.PhysicalNumberOfRows);
                            var index = 1;
                            var row = sheet.GetRow(index);
                            while (row != null)
                            {
                                var currentCell = row.GetCell(columns[XlsxColumn.PropertyId] - 1);
                                var id = currentCell == null ? "" : currentCell.ToString().ToUpper();
                                if (string.IsNullOrEmpty(id))
                                {
                                    index++;
                                    row = sheet.GetRow(index);
                                    continue;
                                }

                                var x = id.IndexOfAny(" -P".ToCharArray());

                                var part = 0;

                                if (x > 0)
                                {
                                    int.TryParse(id.Last().ToString(), out part);
                                    id = id.Remove(x).Trim();
                                }

                                var property = string.IsNullOrEmpty(id) ? null : GetProperty(id, sqLite);
                                if (property != null)
                                {
                                    var old = property.OwnersFr;

                                    currentCell = row.GetCell(columns[XlsxColumn.NameFr] - 1);
                                    property.NameFr = currentCell == null 
                                        ? property.NameFr 
                                        : currentCell.ToString().ToUpper();

                                    currentCell = row.GetCell(columns[XlsxColumn.NameAr] - 1);
                                    property.NameAr = currentCell == null 
                                        ? property.NameAr 
                                        : currentCell.ToString();

                                    currentCell = row.GetCell(columns[XlsxColumn.AdresseAr] - 1);
                                    property.AddressAr = currentCell == null
                                        ? property.AddressAr 
                                        : currentCell.ToString();

                                    currentCell = row.GetCell(columns[XlsxColumn.AdresseFr] - 1);
                                    property.AddressFr = currentCell == null 
                                        ? property.AddressFr 
                                        : currentCell.ToString();

                                    currentCell = row.GetCell(columns[XlsxColumn.Observations] - 1);
                                    property.Observations = currentCell == null 
                                        ? property.Observations 
                                        : currentCell.ToString().ToUpper();

                                    currentCell = row.GetCell(columns[XlsxColumn.Consistance] - 1);
                                    var consistance = currentCell == null ? "" : currentCell.ToString();

                                    currentCell = row.GetCell(columns[XlsxColumn.Contenance] - 1);
                                    var contenance = currentCell == null ? "" : currentCell.NumericCellValue.ToString(CultureInfo.InvariantCulture);

                                    var parcel = property.Parcels.FirstOrDefault(p => p.Part == part);
                                    if (parcel != null)
                                    {
                                        parcel.Consistance = consistance;
                                        parcel.Contenance = contenance == "" ? parcel.Contenance : (int)(double.Parse(contenance) * 10000);
                                        sqLite.Update(parcel, typeof(ParcelInfo));
                                    }

                                    currentCell = row.GetCell(columns[XlsxColumn.OwnersFr] - 1);
                                    var line = currentCell == null 
                                        ? "" 
                                        : currentCell.ToString();

                                    if (!string.IsNullOrEmpty(line))
                                    {
                                        property.OwnersFr = "";

                                        if (line.Contains("REP PAR"))
                                            line.Remove(line.IndexOf("REP PAR", StringComparison.InvariantCulture));

                                        var lines = line.Split("\n;".ToCharArray(),
                                            StringSplitOptions.RemoveEmptyEntries);

                                        var first = lines.FirstOrDefault();
                                        if (first != null)
                                        {
                                            var values = first
                                            .Replace('.', ',')
                                            .Replace('é', 'e')
                                            .Replace('è', 'e')
                                            .ToUpper()
                                            .Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                                            .Select(v => v.Trim())
                                            .ToList();

                                            values.RemoveAll(v => v.StartsWith("NE LE") || v.Any(char.IsDigit));

                                            if (values.Any())
                                                property.OwnersFr = string.Join(" ", values).Trim() + "\n";
                                            if (lines.Length > 1)
                                                property.OwnersFr += "ET CONSORTS\n";
                                        }
                                        property.OwnersFr = property.OwnersFr.Trim();

                                        if (string.IsNullOrEmpty(old) || property.OwnersFr != old)
                                            properties.Add(property);
                                    }

                                    property.Contenance = property.Parcels.Sum(p => p.Contenance);
                                    sqLite.Update(property, typeof(PropertyInfo));
                                }
                                if (!lom.Tick()) throw new Exception("Opération interrompu. Annulation ...");

                                index++;
                                row = sheet.GetRow(index);
                            }
                        }

                        using (var lom = new LongOperationManager("Ajout des noms des propriétaires : "))
                        {
                            lom.SetTotalOperations(properties.Count);
                            var msId = SymbolUtilityServices.GetBlockModelSpaceId(database);
                            var modelSpace = (BlockTableRecord)transaction.GetObject(msId, OpenMode.ForWrite);
                            var styleTable = (TextStyleTable)transaction.GetObject(database.TextStyleTableId, OpenMode.ForRead);
                            var layerTable = (LayerTable)transaction.GetObject(database.LayerTableId, OpenMode.ForRead);
                            var planDeMasse = new PlanDeMasse(transaction, modelSpace, layerTable, styleTable);
                            foreach (var property in properties)
                            {
                                planDeMasse.CreatePropriétaires(property, ' ');
                                if (!lom.Tick()) throw new Exception("Opération interrompu. Annulation ...");
                            }
                        }
                        fs.Close();
                        sqLite.Commit();
                        sqLite.Close();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception e)
            {
                fs.Close();
                document.Editor.WriteMessage("\n" + e);
                return false;
            }
            return true;
        }

        public static bool CopyIds(string xlsx, Dictionary<XlsxColumn, int> columns)
        {
            var document = App.DocumentManager.MdiActiveDocument;
            var db = document.Database;
            var editor = document.Editor;
            var project = ProjectInfo.LoadFrom(db);
            var sql = new SQLiteConnection(project.DbPath);
            var dictionary = new Dictionary<string, string>();

            try
            {
                using (document.LockDocument())
                {
                    using (var transaction = db.TransactionManager.StartTransaction())
                    {
                        var texts = new List<DBText>();
                        var textIds = Functions.GetAllEntitiesFromLayer("P1TXT1");
                        if (textIds == null) throw new Exception("La couche \"P1TXT1\" est vide ou inéxistante. ");
                        using (var lom = new LongOperationManager("Liaison des Numéros : ", 100))
                        {
                            lom.SetTotalOperations(textIds.Length);

                            foreach (var id in textIds)
                            {
                                if (!lom.Tick()) throw new Exception("Opération annulée.");
                                if (id.ObjectClass.DxfName != "TEXT") continue;

                                var tx = (DBText)transaction.GetObject(id, OpenMode.ForRead);
                                if (tx != null) texts.Add(tx);
                            }
                        }

                        List<Parcel> parcels;
                        var parcelIds = Functions.GetAllEntitiesFromLayer(Settings.Default.Lim);
                        using (var lom = new LongOperationManager("Liaison des Numéros : ", 100))
                        {
                            lom.SetTotalOperations(dictionary.Count);
                            parcels = GetParcels(parcelIds, sql, lom).Where(p => char.IsDigit(p.PropertyId[0])).ToList();
                        }

                        using (var lom = new LongOperationManager("Liaison des Numéros : ", 100))
                        {

                            // ReSharper disable once PossibleNullReferenceException
                            lom.SetTotalOperations(parcels.Count);
                            foreach (var parcel in parcels)
                            {
                                if (!lom.Tick()) throw new Exception("Opération annulée.");
                                var inTexts = texts
                                    .Where(t => Functions.PointInPolygon(parcel.GetP3DCollection(), t.Position))
                                    .Select(t => t.TextString.Trim(". ".ToCharArray()))
                                    .Distinct()
                                    .ToList();
                                editor.WriteMessage("\n");
                                if (inTexts.Count == 1 && !dictionary.ContainsKey(parcel.PropertyId))
                                    dictionary.Add(parcel.PropertyId, inTexts[0].Contains("-")
                                        ? inTexts[0].Remove(inTexts[0].LastIndexOf("-", StringComparison.InvariantCulture)).ToUpper()
                                        : inTexts[0].ToUpper());
                            }
                        }

                        var counts = dictionary.Values
                                .Distinct()
                                .ToDictionary(v => v, v => dictionary.Values.Count(t => t == v));

                        var final = new Dictionary<string, string>();
                        using (var lom = new LongOperationManager("Liaison des Numéros : ", 100))
                        {

                            lom.SetTotalOperations(dictionary.Count);
                            foreach (var entry in dictionary)
                            {
                                if (!lom.Tick()) throw new Exception("Opération annulée.");
                                var pId = entry.Key;
                                var gId = entry.Value;
                                var count = counts[gId];

                                if (count == 1)
                                    final.Add(gId, pId);
                            }
                        }

                        XSSFWorkbook wbook;
                        using (var fsOpen = new FileStream(xlsx, FileMode.Open, FileAccess.Read))
                        {
                            wbook = new XSSFWorkbook(fsOpen);
                            fsOpen.Close();
                        }

                        var sheet = wbook.GetSheetAt(0);

                        using (var lom = new LongOperationManager("Liaison des Numéros : ", 100))
                        {
                            lom.SetTotalOperations(sheet.PhysicalNumberOfRows);

                            var index = 1;
                            var row = sheet.GetRow(index);
                            while (row != null)
                            {
                                if (!lom.Tick()) throw new Exception("Opération annulée.");
                                var currentCell = row.GetCell(columns[XlsxColumn.GoldenId] - 1);
                                if (currentCell == null)
                                {
                                    index++;
                                    row = sheet.GetRow(index);
                                    continue;
                                }

                                var gId = currentCell.ToString()
                                    .Replace(" ", "")
                                    .Replace("SS" + project.SubSector + "-", "")
                                    .Replace("-", "")
                                    .ToUpper();

                                if (final.ContainsKey(gId))
                                {
                                    currentCell = row.GetCell(columns[XlsxColumn.PropertyId] - 1);
                                    currentCell = currentCell ?? row.CreateCell(columns[XlsxColumn.PropertyId] - 1);
                                    currentCell.SetCellValue(final[gId]);
                                }

                                index++;
                                row = sheet.GetRow(index);
                            }
                        }

                        using (var stream = new MemoryStream())
                        {
                            wbook.Write(stream);
                            var array = stream.ToArray();
                            // ReSharper disable AssignNullToNotNullAttribute
                            var path = Path.Combine(Path.GetDirectoryName(xlsx), Path.GetFileNameWithoutExtension(xlsx));
                            // ReSharper restore AssignNullToNotNullAttribute
                            if (Directory.Exists(path))
                                Functions.DeleteDirectory(path);
                            Directory.CreateDirectory(path);
                            Functions.UnzipFromStream(new MemoryStream(array), path);
                            Functions.CreateSample(xlsx, path);
                            Functions.DeleteDirectory(path);
                        }
                        transaction.Abort();
                    }
                }
                editor.WriteMessage("\nOpérations términée.");
                return true;
            }
            catch (Exception ex)
            {
                editor.WriteMessage("\nErreur : " + ex.Message);
                return false;
            }
        }

        public static IEnumerable<Parcel> GetParcels(ObjectId[] oIds, SQLiteConnection sqLite, LongOperationManager lom = null)
        {
            foreach (var t in oIds)
            {
                if (lom != null && !lom.Tick()) throw new Exception("Opération annulée.");
                var t1 = t;
                var parInfo = sqLite.Table<ParcelInfo>().FirstOrDefault(p => p.Handle == t1.Handle.Value);
                if (parInfo == null)
                    continue;
                var parcel = new Parcel(parInfo);
                var links = sqLite.Table<LinkInfo>().Where(l => l.ParcelId == parcel.ParcelId);
                foreach (var l in links)
                {
                    var pInfo = sqLite.Get<PointInfo>(l.PointId);
                    parcel.Points.Add(new Point(pInfo));
                }
                yield return parcel;
            }
        }

        public static Property GetProperty(string id, SQLiteConnection sql)
        {
            var propInfo = sql.Get<PropertyInfo>(id);
            if (propInfo == null)
            {
                App.DocumentManager.MdiActiveDocument.Editor
                    .WriteMessage(string.Format("\nImpossible de récupérer la propriété {0}.", id));
                return null;
            }
            var property = new Property(propInfo);

            property.Parcels = sql.Table<ParcelInfo>()
                .Where(p => p.PropertyId == property.PropertyId)
                .Select(p => new Parcel(p))
                .OrderBy(p => p.Part)
                .ToList();

            if (property.Parcels.Count == 0) return null;

            property.Parcels.ForEach(p =>
            {
                var links = sql.Table<LinkInfo>().Where(l => l.ParcelId == p.ParcelId).ToList();
                foreach (var link in links)
                {
                    var pInfo = sql.Get<PointInfo>(link.PointId);
                    p.Points.Add(new Point(pInfo));
                }
            });

            return property;
        }

        public static IEnumerable<Property> GetProperties(IEnumerable<string> propertyIds, SQLiteConnection sql)
        {
            var properties = propertyIds.Select(id => GetProperty(id, sql));
            return properties.Where(p => p != null);
        }

        public static IEnumerable<Property> GetAllPropertiesFromSubSector(SQLiteConnection sql)
        {
            var infos = sql.Query<PropertyInfo>("SELECT PropertyId FROM PropertyInfo").Where(p => char.IsDigit(p.PropertyId[0]));
            var properties = new List<Property>();
            foreach (var propertyInfo in infos)
            {
                var property = GetProperty(propertyInfo.PropertyId, sql);
                if (property != null && property.Parcels.Any())
                    properties.Add(property);
            }
            return properties;
        }

        public static IEnumerable<Property> GetProperties(string idFrom, string idTo, SQLiteConnection sql)
        {
            var sQuety =
                string.Format("SELECT PropertyId FROM PropertyInfo WHERE PropertyId >= '{0}' AND PropertyId <= '{1}'",
                    idFrom, idTo);
            return sql.Query<PropertyInfo>(sQuety)
                .Select(p => GetProperty(p.PropertyId, sql))
                .Where(p => p != null);
        }

        public static bool LoadEtatJuridique(string file, Dictionary<XlsxColumn, int> columns)
        {
            var document = App.DocumentManager.MdiActiveDocument;
            var database = document.Database;
            var project = ProjectInfo.LoadFrom(database);
            var sqLite = new SQLiteConnection(project.DbPath);
            sqLite.BeginTransaction();
            var fs = new FileStream(file, FileMode.Open, FileAccess.Read);
            var wbook = new XSSFWorkbook(fs);
            var sheet = wbook.GetSheetAt(0);

            try
            {
                using (document.LockDocument())
                {
                    using (var transaction = database.TransactionManager.StartTransaction())
                    {
                        var properties = new List<Property>();

                        using (var lom = new LongOperationManager("Lecture de l'état juridique : "))
                        {
                            lom.SetTotalOperations(sheet.PhysicalNumberOfRows);
                            var index = 1;
                            var row = sheet.GetRow(index);
                            while (row != null)
                            {
                                var currentCell = row.GetCell(columns[XlsxColumn.PropertyId] - 1);
                                var id = currentCell == null ? "" : currentCell.ToString().ToUpper();
                                if (string.IsNullOrEmpty(id))
                                {
                                    index++;
                                    row = sheet.GetRow(index);
                                    continue;
                                }

                                var x = id.IndexOfAny(" -P".ToCharArray());

                                if (x > 0)
                                    id = id.Remove(x).Trim();

                                var property = string.IsNullOrEmpty(id) ? null : GetProperty(id, sqLite);
                                if (property != null)
                                {
                                    var old = property.RequisitionId;

                                    currentCell = row.GetCell(columns[XlsxColumn.Requisition] - 1);
                                    property.RequisitionId = currentCell == null ? "" : currentCell.ToString();

                                    if (property.RequisitionId != "")
                                    {
                                        currentCell = row.GetCell(columns[XlsxColumn.Indice] - 1);
                                        property.RequisitionId += currentCell == null ? "" : "/" + currentCell;

                                        property.Parcels.ForEach(p => p.RequisitionId = property.RequisitionId);

                                        sqLite.Update(property, typeof(PropertyInfo));
                                        foreach (var parcel in property.Parcels)
                                        {
                                            sqLite.Update(parcel, typeof(ParcelInfo));
                                        }

                                        if (string.IsNullOrEmpty(old) || property.RequisitionId != old)
                                            properties.Add(property);
                                    }
                                }

                                if (!lom.Tick()) throw new Exception("Opération interrompu. Annulation ...");

                                index++;
                                row = sheet.GetRow(index);
                            }
                        }

                        using (var lom = new LongOperationManager("Ajout des numéros de réquisitions : "))
                        {
                            lom.SetTotalOperations(properties.Count);
                            var msId = SymbolUtilityServices.GetBlockModelSpaceId(database);
                            var modelSpace = (BlockTableRecord)transaction.GetObject(msId, OpenMode.ForWrite);
                            var styleTable = (TextStyleTable)transaction.GetObject(database.TextStyleTableId, OpenMode.ForRead);
                            var layerTable = (LayerTable)transaction.GetObject(database.LayerTableId, OpenMode.ForRead);
                            var planDeMasse = new PlanDeMasse(transaction, modelSpace, layerTable, styleTable);
                            foreach (var property in properties)
                            {
                                planDeMasse.CreateRquisitionIds(property, '-');
                                if (!lom.Tick()) throw new Exception("Opération interrompu. Annulation ...");
                            }
                        }
                        fs.Close();
                        sqLite.Commit();
                        sqLite.Close();
                        transaction.Commit();
                    }
                }
            }
            catch (Exception e)
            {
                fs.Close();
                document.Editor.WriteMessage("\n" + e);
                return false;
            }
            return true;
        }

        public static void ReplaceIds(string suffix, SQLiteConnection sqLite)
        {
            var acadObject = App.AcadApplication;
            acadObject.GetType().InvokeMember(@"ZoomExtents", BindingFlags.InvokeMethod, null, acadObject, null);
            Thread.Sleep(2000);

            var document = App.DocumentManager.MdiActiveDocument;
            var db = document.Database;
            var editor = document.Editor;
            var parcelIds = Functions.GetAllEntitiesFromLayer(Settings.Default.Riv);
            var frameIds = Functions.GetAllEntitiesFromLayer(Settings.Default.Frm);

            var rectangleIds = frameIds.Where(id => id.ObjectClass.DxfName == "LWPOLYLINE").ToArray();
            var textIds = frameIds.Where(id => id.ObjectClass.DxfName == "MTEXT").ToArray();

            try
            {
                using (document.LockDocument())
                {
                    using (var transaction = db.TransactionManager.StartTransaction())
                    {
                        using (var lom = new LongOperationManager("Mise à jour des plans individuels"))
                        {
                            lom.SetTotalOperations(rectangleIds.Length + textIds.Length);

                            var texts = textIds
                                // ReSharper disable once AccessToDisposedClosure
                                .Select(id => (MText)transaction.GetObject(id, OpenMode.ForRead))
                                .Where(tx => tx.XData != null)
                                .ToList();


                            foreach (var id in rectangleIds)
                            {
                                if (!lom.Tick()) throw new Exception("Opérations annulée.");

                                var rectangle = (Polyline)transaction.GetObject(id, OpenMode.ForRead);
                                if (rectangle.XData == null) continue;

                                var propertyId = rectangle.GetPropertyId();
                                if (propertyId == null) continue;

                                var property = GetProperty(propertyId, sqLite);
                                if (property == null) continue;

                                var contained = texts
                                    .Where(tx => Functions.PointInPolygon(rectangle.GetPoint3DCollection(), tx.Location))
                                    .ToList();

                                foreach (var text in contained)
                                {
                                    var designation = text.GetString();
                                    if (designation == null) continue;

                                    text.UpgradeOpen();

                                    switch (designation)
                                    {
                                        case "PditeFr":
                                            text.Contents += property.NameFr;
                                            break;
                                        case "PditeAr":
                                            text.Contents = string.Format(" {0} : الملك المسمى", property.NameAr);
                                            break;
                                        case "ReqFr":
                                            text.Contents += property.RequisitionId;
                                            break;
                                        case "ReqAr":
                                            text.Contents = property.RequisitionId + text.Contents;
                                            break;
                                    }
                                }
                            }
                            foreach (var id in parcelIds)
                            {
                                if (!lom.Tick()) throw new Exception("Opérations annulée.");

                                var text = (MText)transaction.GetObject(id, OpenMode.ForRead);
                                var contents = text.Contents;
                                if (!contents.StartsWith("Plle")) continue;

                                var index = text.Contents.Contains(suffix)
                                    ? contents.IndexOf(suffix, StringComparison.InvariantCulture)
                                    : contents.IndexOf("\n", StringComparison.InvariantCulture);

                                var propertyId = contents.Substring(5, index - 5);

                                var property = GetProperty(propertyId, sqLite);
                                if (property == null || string.IsNullOrEmpty(property.RequisitionId)) continue;

                                contents = "R" + property.RequisitionId + "\n" + contents;

                                text.UpgradeOpen();

                                text.Contents = contents;
                            }
                        }
                        transaction.Commit();
                    }
                }
                document.CloseAndSave(document.Database.Filename);
            }
            catch (Exception e)
            {
                editor.WriteMessage(e.ToString());
            }
        }
    }
}
