﻿using Autodesk.AutoCAD.Geometry;
using Engine.Data.Model;
using GeoAPI.Geometries;

namespace Engine.Data.View
{
    public class Point : PointInfo
    {
        public Point2d P2D { get; set; }
        public Point3d P3D { get; set; }
        public Coordinate Coordinate { get; set; }

        public Point(PointInfo info)
        {
            PointId = info.PointId;
            Name = info.Name;
            X = info.X;
            Y = info.Y;
            PointType = info.PointType;

            P2D = new Point2d(double.Parse(X), double.Parse(Y));
            P3D = new Point3d(P2D.X, P2D.Y, 0.0);
            Coordinate = new Coordinate(P3D.X, P3D.Y);
        }

        public Point()
        {
            
        }
    }
}
