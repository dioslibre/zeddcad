﻿using System.Collections.Generic;
using Engine.Data.Model;

namespace Engine.Data.View
{
    using System;

    public class Property : PropertyInfo
    {
        public List<Parcel> Parcels { get; set; }

        public Property()
        {
            Parcels = new List<Parcel>();
        }

        public Property(PropertyInfo info) : this()
        {
            PropertyId = info.PropertyId;
            GoldenId = info.GoldenId;
            OwnersFr = info.OwnersFr;
            OwnersAr = info.OwnersAr;
            NameFr = info.NameFr;
            NameAr = info.NameAr;
            AddressAr = info.AddressAr;
            AddressFr = info.AddressFr;
            RequisitionId = info.RequisitionId;
            Observations = info.Observations;
        }

        public string Log()
        {
            var nl = Environment.NewLine;
            var str = PropertyId + nl;
            Parcels.ForEach(p =>
                { 
                    str += "P" + p.Part + nl;
                    str += p.Points.Count + nl;
                    p.Points.ForEach(pt => { str += string.Format( "{0} {1} {2}" + nl, pt.Name, pt.X, pt.Y); });
                    str += p.Contenance + nl;
                });
            return str;
        }
    }
}
