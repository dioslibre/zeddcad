﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Engine.Data.Model;
using GeoAPI.Geometries;
using NetTopologySuite.Geometries;

namespace Engine.Data.View
{
    public class Parcel : ParcelInfo
    {
        public List<Point> Points { get; set; }
        public string Mappe { get; set; }

        public Parcel()
        {
            Points = new List<Point>();
            Consistance = "T.C";
        }

        public Parcel(ParcelInfo info) : this()
        {
            ParcelId = info.ParcelId;
            GoldenId = info.GoldenId;
            PropertyId = info.PropertyId;
            RequisitionId = info.RequisitionId;
            TitreId = info.TitreId;
            Handle = info.Handle;
            Part = info.Part;
            Consistance = info.Consistance;
            GraphicArea = info.GraphicArea;
            Contenance = info.Contenance;
        }

        public Point GetNext(Point point)
        {
            var count = Points.Count;
            return Points[(Points.IndexOf(point) + 1) % count];
        }

        public Point GetPrevious(Point point)
        {
            var count = Points.Count;
            return Points[(Points.IndexOf(point) - 1 + count) % count];
        }

        public Point GetNextBorne(Point point)
        {
            while (true)
            {
                var next = GetNext(point);
                if (next.Name.StartsWith("B")) return next;
                point = next;
            }
        }

        public Point GetPreviousBorne(Point point)
        {
            while (true)
            {
                var prev = GetPrevious(point);
                if (prev.Name.StartsWith("B")) return prev;
                point = prev;
            }
        }

        public Point3dCollection GetP3DCollection()
        {
            return new Point3dCollection(Points.Select(p => p.P3D).ToArray());
        }

        public IGeometry GetPolygon()
        {
            var coordinates = Points.Select(p => p.Coordinate).ToList();
            coordinates.Add(coordinates[0]);
            return new LinearRing(coordinates.ToArray());
        }

        public Polyline GetPolyline()
        {
            var polyline = new Polyline();
            for (var i = 0; i < Points.Count; i++)
            {
                polyline.AddVertexAt(i, Points[i].P2D, 0, 0, 0);
            }
            polyline.Closed = true;

            return polyline;
        }
    }
}
