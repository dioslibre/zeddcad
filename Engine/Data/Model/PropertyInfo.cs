﻿using SQLite;

namespace Engine.Data.Model
{
    public class PropertyInfo
    {
        protected bool Equals(PropertyInfo other)
        {
            return string.Equals(PropertyId, other.PropertyId);
        }

        public override int GetHashCode()
        {
            return (PropertyId != null ? PropertyId.GetHashCode() : 0);
        }

        [PrimaryKey]
        public string PropertyId { get; set; }
        
        [Indexed]
        public string GoldenId { get; set; }
        public string AddressFr { get; set; }
        public string AddressAr { get; set; }
        public string OwnersFr { get; set; }
        public string OwnersAr { get; set; }
        public string NameFr { get; set; }
        public string NameAr { get; set; }
        public int Contenance { get; set; }
        public string RequisitionId { get; set; }
        public string Observations { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((PropertyInfo) obj);
        }
    }
}
