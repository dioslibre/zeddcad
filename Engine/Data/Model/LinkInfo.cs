﻿using System;
using SQLite;

namespace Engine.Data.Model
{
    public class LinkInfo
    {
        [PrimaryKey, AutoIncrement]
        public int LinkId { get; set; }
        [Indexed]
        public int ParcelId { get; set; }
        [Indexed]
        public Int64 PointId { get; set; }
    }
}
