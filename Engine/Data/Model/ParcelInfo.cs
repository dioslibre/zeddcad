﻿using SQLite;

namespace Engine.Data.Model
{
    public class ParcelInfo
    {
        protected bool Equals(ParcelInfo other)
        {
            return ParcelId == other.ParcelId;
        }

        public override int GetHashCode()
        {
            return ParcelId;
        }

        [PrimaryKey, AutoIncrement]
        public int ParcelId { get; set; }
        
        [Indexed]
        public string GoldenId { get; set; }
        
        [Indexed]
        public string PropertyId { get; set; }
        
        [Indexed]
        public string RequisitionId { get; set; }
        
        [Indexed]
        public string TitreId { get; set; }
        
        [Indexed, NotNull]
        public long Handle { get; set; }

        public int Part { get; set; }
        public string Consistance { get; set; }
        public float GraphicArea { get; set; }
        public int Contenance { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ParcelInfo) obj);
        }
    }
}
