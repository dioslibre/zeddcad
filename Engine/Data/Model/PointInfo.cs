﻿using System;
using SQLite;

namespace Engine.Data.Model
{
    public enum PointType
    {
        Plantée,
        Marquée,
        Autre
    }

    public class PointInfo
    {
        protected bool Equals(PointInfo other)
        {
            return PointId == other.PointId;
        }

        public override int GetHashCode()
        {
            return PointId.GetHashCode();
        }

        [PrimaryKey]
        public Int64 PointId { get; set; }
        
        public string Name { get; set; }
        
        public string X { get; set; }
        public string Y { get; set; }
        public string PointType { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return ((PointInfo) obj).PointId == PointId;
        }
    }
}
