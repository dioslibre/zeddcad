﻿using System.IO;
using System.Xml.Serialization;

namespace Engine.Utils
{
    public abstract class XmlSupport<T>
    {
        public static T ReadDocument(string fileName)
        {
            var xs = new XmlSerializer(typeof(T));
            using (Stream stream = File.OpenRead(fileName))
            {
                return (T)xs.Deserialize(stream);
            }
        }

        public void WriteDocument(string fileName)
        {
            var xs = new XmlSerializer(typeof(T));
            using (Stream stream = File.OpenWrite(fileName))
            {
                xs.Serialize(stream, this);
            }
        }
    }
}
