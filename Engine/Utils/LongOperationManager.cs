﻿using System;
using System.Windows.Forms;
using Autodesk.AutoCAD.Runtime;

namespace Engine.Utils
{
    public class LongOperationManager : IDisposable, IMessageFilter
    {
        // The message code corresponding to a keypress
        const int WmKeydown = 0x0100;

        // Internal members for metering progress
        private readonly ProgressMeter _pm;
        private int _progressMeterIncrements;
        private long _updateIncrement;
        private long _currentInc;

        // External flag for checking cancelled status
        public bool Cancelled;

        // Constructor

        public LongOperationManager(string message, int progressMeterIncrements = 600)
        {
            _progressMeterIncrements = progressMeterIncrements;
            System.Windows.Forms.Application.AddMessageFilter(this);
            _pm = new ProgressMeter();
            _pm.Start(message);
            _pm.SetLimit(_progressMeterIncrements);
            _currentInc = 0;
        }

        // System.IDisposable.Dispose

        public void Dispose()
        {
            _pm.Stop();
            _pm.Dispose();
            System.Windows.Forms.Application.
              RemoveMessageFilter(this);
        }

        // Set the total number of operations

        public void SetTotalOperations(long totalOps)
        {
            // We really just care about when we need
            // to update the timer
            _updateIncrement =
              (totalOps > _progressMeterIncrements ?
                totalOps / _progressMeterIncrements :
                totalOps
              );
        }

        // This function is called whenever an operation
        // is performed

        public bool Tick()
        {
            if (++_currentInc == _updateIncrement)
            {
                _pm.MeterProgress();
                _currentInc = 0;
                System.Windows.Forms.Application.DoEvents();
            }
            // Check whether the filter has set the flag
            if (Cancelled)
                _pm.Stop();

            return !Cancelled;
        }

        // The message filter callback

        public bool PreFilterMessage(
          ref Message m
        )
        {
            if (m.Msg == WmKeydown)
            {
                // Check for the Escape keypress
                Keys kc =
                  (Keys)(int)m.WParam &
                  Keys.KeyCode;

                if (m.Msg == WmKeydown &&
                    kc == Keys.Escape)
                {
                    Cancelled = true;
                }

                // Return true to filter all keypresses
                return true;
            }
            // Return false to let other messages through
            return false;
        }
    }
}
