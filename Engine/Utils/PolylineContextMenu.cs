﻿using System;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Windows;
using App = Autodesk.AutoCAD.ApplicationServices.Application;

namespace Engine.Utils
{
    public class PolylineContextMenu
    {
        private static ContextMenuExtension _cme;

        public static void Attach()
        {
            _cme = new ContextMenuExtension();
            var mi = new MenuItem("Supprimer La/Les Parcelle(s)");
            mi.Click += OnRemove;
            _cme.MenuItems.Add(mi);
            var rxc = RXObject.GetClass(typeof(Polyline));
            App.AddObjectContextMenuExtension(rxc, _cme);
        }

        public static void Detach()
        {
            var rxc = RXObject.GetClass(typeof(Polyline));
            App.RemoveObjectContextMenuExtension(rxc, _cme);
        }

        private static void OnRemove(Object o, EventArgs e)
        {
            var doc = App.DocumentManager.MdiActiveDocument;
            doc.SendStringToExecute("_.REMOVEPARCEL\n", true, false, false);
        }
    }
}