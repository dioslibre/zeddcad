﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Engine.Data;
using Engine.Data.View;
using GeoAPI.Geometries;
using NetTopologySuite.Algorithm;
using NetTopologySuite.Geometries;
using NetTopologySuite.Geometries.Utilities;
using SQLite;
using App = Autodesk.AutoCAD.ApplicationServices.Application;
using Color = System.Drawing.Color;
using Control = System.Windows.Forms.Control;
using Path = System.Collections.Generic.List<Engine.Utils.IntPoint>;
using Paths = System.Collections.Generic.List<System.Collections.Generic.List<Engine.Utils.IntPoint>>;
using Point = Engine.Data.View.Point;
using Settings = Engine.Properties.Settings;

#pragma warning disable 618

namespace Engine.Utils
{
    using ICSharpCode.SharpZipLib.Core;
    using ICSharpCode.SharpZipLib.Zip;

    public class PassageInfo
    {
        public Point[] Points { get; set; }
        public int Width { get; set; }
        public string Side { get; set; }
        public Parcel Parcel { get; set; }
    }
    public static class Functions
    {
        public static bool HasWriteAccessToFolder(string folderPath)
        {
            try
            {
                Directory.GetAccessControl(folderPath);
                return true;
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }
        }

        public static IEnumerable<T> GetChildControls<T>(this Control control) where T : Control
        {
            var children = control.Controls.OfType<T>().ToList<T>();
            return children.SelectMany(GetChildControls<T>).Concat(children);
        }

        public static List<Point> GetFreePoints(Parcel parcel, List<Parcel> riverainsParcels)
        {
            var freePoints = new List<Point>();

            var points = parcel.Points;
            var count = points.Count;
            for (var i = 0; i < count; i++)
            {
                var i1 = i;
                var first = riverainsParcels.Where(r => r.Points.Contains(points[i1]));
                var second = riverainsParcels.Where(r => r.Points.Contains(points[(i1 + 1) % count]));
                if (first.Intersect(second).Any()) continue;
                if (!freePoints.Contains(points[i])) freePoints.Add(points[i]);
                if (!freePoints.Contains(points[(i + 1) % count])) freePoints.Add(points[(i + 1) % count]);
            }

            return freePoints.Count > 1
                ? freePoints
                : new List<Point>();
        }

        public static ObjectId[] GetEntitiesFromLayersWindow(Point3dCollection points, params string[] layers)
        {
            var tvs = new[] { new TypedValue((int)DxfCode.LayerName, string.Join(",", layers)) };

            var editor = App.DocumentManager.MdiActiveDocument.Editor;
            var result = editor.SelectWindowPolygon(points, new SelectionFilter(tvs));
            return result.Status == PromptStatus.OK ? result.Value.GetObjectIds() : null;
        }

        public static ObjectId[] GetEntitiesFromLayersCrossing(Point3dCollection points, params string[] layers)
        {
            var tvs = new[] { new TypedValue((int)DxfCode.LayerName, string.Join(",", layers)) };

            var editor = App.DocumentManager.MdiActiveDocument.Editor;
            var result = editor.SelectCrossingPolygon(points, new SelectionFilter(tvs));
            return result.Status == PromptStatus.OK ? result.Value.GetObjectIds() : null;
        }

        public static bool PointInPolygon(Point3dCollection polygon, Point3d point)
        {
            polygon.Add(polygon[0]);

            var result = false;
            var j = polygon.Count - 1;
            for (var i = 0; i < polygon.Count; i++)
            {
                if (polygon[i].Y < point.Y && polygon[j].Y >= point.Y
                    || polygon[j].Y < point.Y && polygon[i].Y >= point.Y)
                {
                    if (polygon[i].X
                        + (point.Y - polygon[i].Y) / (polygon[j].Y - polygon[i].Y) * (polygon[j].X - polygon[i].X)
                        < point.X)
                    {
                        result = !result;
                    }
                }
                j = i;
            }
            return result;
        }

        public static MText CreateTextForBornes(Point point, Parcel parcel, double textHeight, int scaleNumber)
        {
            var next = parcel.GetNext(point);
            var prev = parcel.GetPrevious(point);

            var _vY = point.P3D.Y;
            var _nY = next.P3D.Y;
            var _pY = prev.P3D.Y;

            var _nX = next.P3D.X;
            var _pX = prev.P3D.X;

            var nline = new Line(point.P3D, next.P3D);
            var pline = new Line(point.P3D, prev.P3D);

            var id = point.Name;

            var mtext = new MText
                            {
                                Contents = "TR".Contains(parcel.PropertyId[0]) ? "(" + id + ")" : id,
                                TextHeight = textHeight * scaleNumber
                            };

            if ((_pY - _vY) * (_nY - _vY) > 0.0)
            {
                var p1 = nline.GetPointAtParameter(0.001);
                var points = new Point3dCollection();
                pline.IntersectWith(
                    new Line(p1, p1.Add(new Vector3d(10.0, 0.0, 0.0))),
                    Intersect.ExtendBoth,
                    points,
                    IntPtr.Zero,
                    IntPtr.Zero);
                var p2 = points[0];

                var txWidth = mtext.Contents.Length * textHeight * scaleNumber;
                var parameter = txWidth * 0.001 / p1.DistanceTo(p2);

                p1 = parameter < nline.EndParam ? nline.GetPointAtParameter(parameter) : next.P3D;

                points = new Point3dCollection();
                pline.IntersectWith(
                    new Line(p1, p1.Add(new Vector3d(10.0, 0.0, 0.0))),
                    Intersect.ExtendBoth,
                    points,
                    IntPtr.Zero,
                    IntPtr.Zero);
                p2 = points[0];

                var mediane = p1.Add((p2 - p1) / 2);
                // vertex is above /\
                if (_vY >= _nY)
                {
                    // next is to the left ->/\->
                    if (_pX <= _nX)
                    {
                        mtext.Location = mediane;
                        mtext.Attachment = AttachmentPoint.TopCenter;
                    }
                    // next is to the right <-/\-<
                    else
                    {
                        mtext.Location = point.P3D.Add(scaleNumber * .002 * Vector3d.YAxis);
                        mtext.Attachment = AttachmentPoint.BottomCenter;
                    }
                }
                // vertex is bellow \/
                else
                {
                    // next is to the left ->\/->
                    if (_pX >= _nX)
                    {
                        mtext.Location = mediane;
                        mtext.Attachment = AttachmentPoint.BottomCenter;
                    }
                    // next is to the right <-\/-<
                    else
                    {
                        mtext.Location = point.P3D.Add(-scaleNumber * .002 * Vector3d.YAxis);
                        mtext.Attachment = AttachmentPoint.TopCenter;
                    }
                }
            }
            else
            {
                var n = nline.GetPointAtParameter(0.001);
                var p = pline.GetPointAtParameter(0.001);
                var mediane = n.Add((p - n) / 2.0);
                var delta = mediane - point.P3D;
                var vector = mtext.TextHeight * 1.2 * delta.DivideBy(delta.Length);

                // previous is above >
                if (_vY >= _nY)
                {
                    // mediane is to the left  / <-
                    mtext.Location = delta.X <= 0 ? point.P3D.Add(vector) : point.P3D.Add(-vector);
                    mtext.Attachment = AttachmentPoint.MiddleRight;
                }
                else
                {
                    // mediane is to the right  -> /
                    mtext.Location = delta.X <= 0 ? point.P3D.Add(-vector) : point.P3D.Add(vector);
                    mtext.Attachment = AttachmentPoint.MiddleLeft;
                }
            }

            nline.Dispose();
            pline.Dispose();
            return mtext;
        }

        public static MText CreateTextForPropriétaires(IGeometry geometry, string contents, int scaleNumber = 2000)
        {
            contents = contents.EndsWith("\n") ? contents : contents + "\n";
            var centroid = geometry.Centroid;
            if (!PointInPolygon(geometry.Coordinates, centroid))
            {
                geometry = GetBiggestConvexPolygon(geometry.Coordinates.ToList());
                centroid = geometry.Centroid;
            }

            geometry = GetMinimumBoundingRectangle(geometry);

            var p1 = geometry.Coordinates[0];
            var p2 = geometry.Coordinates[1];
            var p3 = geometry.Coordinates[2];

            var vectorX = new Point3d(p2.X, p2.Y, 0.0) - new Point3d(p1.X, p1.Y, 0.0);
            var vectorY = new Point3d(p3.X, p3.Y, 0.0) - new Point3d(p2.X, p2.Y, 0.0);

            var wVector = vectorX.Length > vectorY.Length ? vectorX : vectorY;
            var hVector = vectorX.Length < vectorY.Length ? vectorX : vectorY;

            contents = hVector.Length > .45 * wVector.Length
                ? contents
                    .Replace(" B ", " BEN ")
                    .Replace(" BEN ", "\nBEN ")
                    .Replace(" ET ", "\nET ")
                    .Replace(" BET ", "\nBENT ")
                    .Replace(" BENT ", "\nBENT ")
                : contents;

            var textHeight = 0.00325 * scaleNumber;
            double width;

            var ben = contents.Contains("BEN\n") || contents.Contains("BENT\n");

            do
            {
                textHeight -= .00025 * scaleNumber;
                width = ben
                    ? textHeight * (contents.IndexOf("BEN", StringComparison.InvariantCulture) - 1)
                    : textHeight * contents.IndexOf("\n", StringComparison.InvariantCulture);
            }
            while (width > wVector.Length && textHeight >= 2.1);

            contents = hVector.Length > .45 * wVector.Length
                ? contents
                    .Replace("HTIERS ", "HTIERS\n")
                    .Replace("HT ", "HT\n")
                : contents;

            var rotation = Vector3d.XAxis.GetAngleTo(wVector, Vector3d.ZAxis);
            return new MText
            {
                Contents = contents.Trim("\n".ToCharArray()),
                Location = new Point3d(centroid.X, centroid.Y, 0.0),
                TextHeight = textHeight,
                Rotation = Math.Cos(rotation) >= 0.0 ? rotation : rotation + Math.PI,
                Attachment = AttachmentPoint.MiddleCenter,
            };
        }

        public static bool PointInPolygon(Coordinate[] polygon, IPoint point)
        {
            var result = false;
            var j = polygon.Length - 1;
            for (var i = 0; i < polygon.Length; i++)
            {
                if (polygon[i].Y < point.Y && polygon[j].Y >= point.Y
                    || polygon[j].Y < point.Y && polygon[i].Y >= point.Y)
                {
                    if (polygon[i].X
                        + (point.Y - polygon[i].Y) / (polygon[j].Y - polygon[i].Y) * (polygon[j].X - polygon[i].X)
                        < point.X)
                    {
                        result = !result;
                    }
                }
                j = i;
            }
            return result;
        }

        public static IGeometry GetMinimumBoundingRectangle(IGeometry geometry)
        {
            var centroid = geometry.Centroid;
            var minimum = new MinimumDiameter(geometry).Diameter;
            var p1 = minimum.StartPoint.Y > minimum.EndPoint.Y ? minimum.StartPoint : minimum.EndPoint;
            var p2 = minimum.StartPoint.Y < minimum.EndPoint.Y ? minimum.StartPoint : minimum.EndPoint;
            var p3D1 = new Point3d(p2.X, p2.Y, 0.0);
            var p3D2 = new Point3d(p1.X, p1.Y, 0.0);
            var vector = p3D2 - p3D1;
            var a = Vector3d.XAxis.GetAngleTo(vector, Vector3d.ZAxis) + Math.PI / 2;


            var transform = new AffineTransformation();
            transform.SetToRotation(-a, centroid.X, centroid.Y);

            var rotated = transform.Transform(geometry);

            var minX = rotated.Coordinates.Min(c => c.X);
            var maxX = rotated.Coordinates.Max(c => c.X);
            var minY = rotated.Coordinates.Min(c => c.Y);
            var maxY = rotated.Coordinates.Max(c => c.Y);

            var minimumRectangle =
                new LinearRing(
                    new[]
                        {
                            new Coordinate(minX, minY), new Coordinate(maxX, minY), new Coordinate(maxX, maxY),
                            new Coordinate(minX, maxY), new Coordinate(minX, minY)
                        });

            transform.SetToRotation(a, centroid.X, centroid.Y);
            return transform.Transform(minimumRectangle);
        }

        public static bool PolygonIsConvex(Coordinate[] points)
        {
            var gotNegative = false;
            var gotPositive = false;
            var numPoints = points.Length;
            for (var a = 0; a < numPoints; a++)
            {
                var b = (a + 1) % numPoints;
                var c = (b + 1) % numPoints;

                var crossProduct = CrossProductLength(
                    points[a].X, points[a].Y, points[b].X, points[b].Y, points[c].X, points[c].Y);
                if (crossProduct < 0)
                {
                    gotNegative = true;
                }
                else if (crossProduct > 0)
                {
                    gotPositive = true;
                }
                if (gotNegative && gotPositive) return false;
            }

            return true;
        }

        private static double CrossProductLength(double ax, double ay, double bx, double @by, double cx, double cy)
        {
            // Get the vectors' coordinates.
            var bAx = ax - bx;
            var bAy = ay - @by;
            var bCx = cx - bx;
            var bCy = cy - @by;

            // Calculate the Z coordinate of the cross product.
            return (bAx * bCy - bAy * bCx);
        }

        public static double PolygonGraphicArea(Coordinate[] corners)
        {
            var n = corners.Length;
            var area = 0.0;
            for (var i = 0; i < n; i++)
            {
                var j = (i + 1) % n;
                area += corners[i].X * corners[j].Y;
                area -= corners[j].Y * corners[i].Y;
            }
            area = Math.Abs(area) / 2.0;
            return area;
        }

        public static void AddLayersForPlanDeMasse(Database db)
        {
            var layers = new[]
                             {
                                 CreateLayer(Settings.Default.Blk, Settings.Default.PeColorPrps),
                                 CreateLayer(Settings.Default.BrnId),
                                 CreateLayer(Settings.Default.Cst, Settings.Default.PeColorCst),
                                 CreateLayer(Settings.Default.Lgth, Settings.Default.PeColorLgth),
                                 CreateLayer(Settings.Default.Lim, Settings.Default.PeColorLim),
                                 CreateLayer(Settings.Default.Lima, Settings.Default.PeColorLima),
                                 CreateLayer(Settings.Default.PclId, Settings.Default.PeColorParId),
                                 CreateLayer(Settings.Default.Prps, Settings.Default.PeColorPrps),
                                 CreateLayer(Settings.Default.Rqtr),
                                 CreateLayer(Settings.Default.Dtl)
                             };

            using (var transaction = db.TransactionManager.StartTransaction())
            {
                var ltId = db.LayerTableId;
                var lt = (LayerTable)transaction.GetObject(ltId, OpenMode.ForWrite);
                Array.ForEach(
                    layers,
                    l =>
                    {
                        if (lt.Has(l.Name)) return;
                        lt.Add(l);
                        transaction.AddNewlyCreatedDBObject(l, true);
                    });
                transaction.Commit();
            }
        }

        private static LayerTableRecord CreateLayer(string name, Color? color = null)
        {
            return new LayerTableRecord
                       {
                           Name = name,
                           Color =
                               color == null
                                   ? Autodesk.AutoCAD.Colors.Color.FromColorIndex(ColorMethod.ByAci, 7)
                                   : Autodesk.AutoCAD.Colors.Color.FromColor(color.Value)
                       };
        }

        public static void CopyTextStyles(Database source, Database destination, string prefix)
        {
            var sourceStId = source.TextStyleTableId;
            var destStId = destination.TextStyleTableId;

            var tsIds = new ObjectIdCollection();

            var sourceStTable = (TextStyleTable)sourceStId.Open(OpenMode.ForRead);
            foreach (var oId in sourceStTable)
            {
                var ts = (TextStyleTableRecord)oId.Open(OpenMode.ForRead);
                if (ts.Name.StartsWith(prefix)) tsIds.Add(oId);
                ts.Close();
            }
            sourceStTable.Close();

            source.WblockCloneObjects(tsIds, destStId, new IdMapping(), DuplicateRecordCloning.Ignore, false);
        }

        public static void AddSquareBlock(Database database)
        {
            using (var transaction = database.TransactionManager.StartTransaction())
            {
                var btId = database.BlockTableId;
                var blockTable = (BlockTable)transaction.GetObject(btId, OpenMode.ForWrite);

                const double radius = 0.00068 * 2000;
                var points = new Point2dCollection
                                 {
                                     new Point2d(-radius, -radius),
                                     new Point2d(radius, -radius),
                                     new Point2d(radius, radius),
                                     new Point2d(-radius, radius),
                                     new Point2d(-radius, -radius)
                                 };

                var wipeout = new Wipeout();
                wipeout.SetFrom(points, Vector3d.ZAxis);

                var circle = new Circle(Point3d.Origin, Vector3d.ZAxis, 0.00008 * 2000);

                var btr = new BlockTableRecord
                              {
                                  Name = Settings.Default.BlkRect,
                                  Explodable = true,
                                  BlockScaling = BlockScaling.Uniform
                              };
                blockTable.Add(btr);
                transaction.AddNewlyCreatedDBObject(btr, true);

                btr.AppendEntity(wipeout);
                transaction.AddNewlyCreatedDBObject(wipeout, true);

                var id = btr.AppendEntity(circle);
                transaction.AddNewlyCreatedDBObject(circle, true);

                var ids = new ObjectIdCollection { id };

                var hatch = new Hatch
                                {
                                    HatchObjectType = HatchObjectType.HatchObject,
                                    Normal = Vector3d.ZAxis,
                                    ColorIndex = 7
                                };

                hatch.SetHatchPattern(HatchPatternType.PreDefined, "SOLID");

                btr.AppendEntity(hatch);
                transaction.AddNewlyCreatedDBObject(hatch, true);

                hatch.Associative = false;
                hatch.AppendLoop(HatchLoopTypes.Default, ids);
                hatch.EvaluateHatch(true);

                transaction.Commit();
            }
        }

        public static Line GetLonguestLine(IList<Point> shared, double tolerance)
        {
            if (shared.Count == 2) return new Line(shared[0].P3D, shared[1].P3D);

            var points = AggregatePoints(shared.Select(p => p.P3D).ToList(), tolerance);

            if (points.Count == 2) return new Line(points[0], points[1]);

            var dists =
                points.Take(points.Count - 1)
                      .ToDictionary(p => p, p => p.DistanceTo(points[points.IndexOf(p) + 1]))
                      .OrderByDescending(p => p.Value);

            var point = dists.First().Key;
            return new Line(point, points[points.IndexOf(point) + 1]);
        }

        public static Dictionary<Parcel, Point3d[]> GetPiste(
            Property property, Dictionary<Parcel, List<Parcel>> riverainsParcels, SQLiteConnection sqLite)
        {
            if (property.Parcels.Count == 1)
                return GetPisteForParcel(property, property.Parcels[0], riverainsParcels, sqLite,
                    GetFreePoints(property.Parcels[0], riverainsParcels[property.Parcels[0]]));

            var dict = property.Parcels.ToDictionary(p => p, p => GetFreePoints(p, riverainsParcels[p]))
                .OrderByDescending(e => e.Value.Count)
                .ToList();
            return GetPisteForParcel(property, dict[0].Key, riverainsParcels, sqLite, dict[0].Value);
        }

        public static Dictionary<Parcel, Point3d[]> GetPisteForParcel(Property property,
            Parcel parcel, Dictionary<Parcel, List<Parcel>> riverainsParcels, SQLiteConnection sqLite, List<Point> freePoints)
        {
            var piste = new Dictionary<Parcel, Point3d[]>();

            freePoints = OrderPoints(freePoints, parcel);
            if (freePoints.Count <= 1) return piste;

            var buffer = ExtrudePoints(freePoints);
            var buffer1 = buffer;
            var coords = Enumerable
                .Range(0, buffer1.Count)
                .Select(i => buffer1[i])
                .Select(p => new Coordinate(p.X, p.Y, 0));

            var hull = new ConvexHull(coords.ToArray(), new GeometryFactory()).GetConvexHull();

            var points = hull.Coordinates.Select(c => new Point3d(c.X, c.Y, 0));

            buffer = new Point3dCollection(points.ToArray());
            var oIds = GetEntitiesFromLayersCrossing(buffer, Settings.Default.Lim, Settings.Default.Lima);

            if (oIds == null || oIds.Length == 0) return piste;

            foreach (
                var entry in
                    FilterEntities(property, oIds, buffer, sqLite, riverainsParcels)
                        .Where(entry => !piste.Keys.Contains(entry.Key) && entry.Value.Length > 0))
            {
                piste.Add(entry.Key, entry.Value);
            }

            return piste;
        }

        private static Dictionary<Parcel, Point3d[]> FilterEntities(
            Property property,
            ObjectId[] oIds,
            Point3dCollection buffer,
            SQLiteConnection sqLite,
            Dictionary<Parcel, List<Parcel>> riverainsParcels)
        {
            var parcels = Controller.GetParcels(oIds, sqLite).ToList();
            parcels.RemoveAll(p => property.Parcels.Contains(p));
            foreach (var rivs in riverainsParcels)
            {
                parcels.RemoveAll(r => rivs.Value.Any(riv => riv.Points.Intersect(r.Points).Any()));
            }

            return parcels.ToDictionary(
                parcel => parcel,
                parcel =>
                OrderPoints(parcel.Points.Where(p => PointInPolygon(buffer, p.P3D)).ToList(), parcel)
                    .Select(p => p.P3D)
                    .ToArray());
        }

        public static Point3dCollection ExtrudePoints(IEnumerable<Point> freePoints)
        {
            var result = new Point3dCollection();
            var points3D = freePoints.Select(p => p.P3D).ToArray();
            var count = points3D.Length;

            var pointBuffer = new Point3d[points3D.Length];

            var first = (points3D[1] - points3D[0]);
            first = first / first.Length;

            var postFirst = first.MultiplyBy(21).RotateBy(Math.PI / 2, Vector3d.ZAxis);
            pointBuffer[0] = points3D[0].Add(-first * 10).Add(postFirst);

            for (var i = 1; i < points3D.Length - 1; i++)
            {
                var inter = (points3D[i + 1] - points3D[i - 1]).DivideBy((points3D[i + 1] - points3D[i - 1]).Length);
                var postInter = inter.MultiplyBy(21).RotateBy(Math.PI / 2, Vector3d.ZAxis);
                pointBuffer[i] = points3D[i].Add(postInter);
            }

            var last =
                (points3D[count - 1] - points3D[count - 2]).DivideBy((points3D[count - 1] - points3D[count - 2]).Length);
            var postLast = last.MultiplyBy(21).RotateBy(Math.PI / 2, Vector3d.ZAxis);
            pointBuffer[points3D.Length - 1] = points3D[points3D.Length - 1].Add(last * 10).Add(postLast);

            pointBuffer = pointBuffer.Reverse().ToArray();
            result.Add(points3D[0].Add(-first * 10));
            for (var i = 1; i < points3D.Length - 1; i++)
            {
                result.Add(points3D[i]);
            }
            result.Add(points3D[points3D.Length - 1].Add(last * 10));
            for (var i = 0; i < points3D.Length; i++)
            {
                result.Add(pointBuffer[i]);
            }

            return result;
        }

        public static List<Point> OrderPoints(List<Point> points, Parcel parcel)
        {
            if (points.Count <= 1) return points;

            if (points.First().Equals(points.Last()) || !points.Contains(parcel.Points[0]))
                return points.OrderBy(p => parcel.Points.IndexOf(p)).ToList();

            if (points.Count == parcel.Points.Count) return parcel.Points;

            var queue = new Queue<Point>(parcel.Points);

            do
            {
                queue.Enqueue(queue.Dequeue());
            }
            while (points.Contains(queue.Last()));

            return points.OrderBy(p => queue.ToList().IndexOf(p)).ToList();
        }

        public static Dictionary<Parcel, List<Parcel>> GetRiverainsParcels(
            Property property, IGeometry hull, SQLiteConnection sqLite)
        {
            var riverainsParcels = new Dictionary<Parcel, List<Parcel>>();
            var offset = OffsetParcel(hull);
            var oIds = GetEntitiesFromLayersCrossing(offset, Settings.Default.Lim, Settings.Default.Lima);

            foreach (var parcel in property.Parcels)
            {
                if (oIds == null)
                    riverainsParcels.Add(parcel, new List<Parcel>());
                else
                {
                    var thisParcel = parcel;
                    var parcels = Controller.GetParcels(oIds, sqLite).Where(p => p.ParcelId != thisParcel.ParcelId);

                    riverainsParcels.Add(parcel, parcels.Where(p => p.Points.Intersect(thisParcel.Points).Any()).ToList());
                }
            }
            return riverainsParcels;
        }

        public static Point3dCollection OffsetParcel(IGeometry hull, double offset = 10)
        {
            var path = hull.Coordinates
                .Select(p => new IntPoint(Convert.ToInt32(p.X * 100), Convert.ToInt32(p.Y * 100)))
                .ToList();

            var paths = new Paths { path };
            var solution = new List<List<IntPoint>>();
            var clipper = new ClipperOffset();
            clipper.AddPaths(paths, JoinType.jtSquare, EndType.etClosedPolygon);
            clipper.Execute(ref solution, offset * 100);
            return new Point3dCollection(solution[0].Select(p => new Point3d(p.X / 100.0, p.Y / 100.0, 0.0)).ToArray());
        }

        public static IGeometry SetConvexHull(Property property, Dictionary<Parcel, Point3d[]> piste)
        {
            var coordinates = new List<Coordinate>();
            property.Parcels.ForEach(p => coordinates.AddRange(p.Points.Select(t => t.Coordinate)));
            piste.Values.ToList().ForEach(a => coordinates.AddRange(a.Select(p => new Coordinate(p.X, p.Y))));

            var algorithm = new ConvexHull(coordinates.ToArray(), new GeometryFactory());
            return algorithm.GetConvexHull();
        }

        public static int[] GetCropArea(RasterImage acRaster, Polyline polyline)
        {
            var values = new int[4];

            var p0 = polyline.GetPoint3dAt(3);
            var p2 = polyline.GetPoint3dAt(1);

            var x = (int)(p0.TransformBy(acRaster.PixelToModelTransform.Inverse()).X);
            var y = (int)(p0.TransformBy(acRaster.PixelToModelTransform.Inverse()).Y);

            var width = (int)Math.Abs((p2.TransformBy(acRaster.PixelToModelTransform.Inverse()).X) - x);
            var height = (int)Math.Abs((p2.TransformBy(acRaster.PixelToModelTransform.Inverse()).Y) - y);

            values[0] = x;
            values[1] = y;
            values[2] = width;
            values[3] = height;

            return values;
        }

        // ReSharper disable once LocalizableElement
        //[DllImport(@"CropLib.dll", EntryPoint = "?fnCropLib@@YAHPEAXPEADQEAH@Z",
        //    CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        //public static extern int Crop(IntPtr img, string filename, int[] rectangle);

        // ReSharper disable once LocalizableElement
        [DllImport(@"CropLib.dll", EntryPoint = "?fnCropLib@@YGHPAXPADQAH@Z",
            CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int Crop(IntPtr img, string filename, int[] rectangle);

        public static void SetRaster(Database db, Transaction transaction, out RasterImage raster, out IntPtr pointer)
        {
            pointer = IntPtr.Zero;
            raster = null;

            var imageDict = RasterImageDef.GetImageDictionary(db);

            if (imageDict == ObjectId.Null) return;


            var imageDic = (DBDictionary)transaction.GetObject(imageDict, OpenMode.ForRead);
            RasterImageDef imageDef = null;
            foreach (var def in imageDic)
            {
                imageDef = transaction.GetObject(def.Value, OpenMode.ForRead) as RasterImageDef;
                if (imageDef != null) break;
            }

            if (imageDef == null) return;
            var ids = imageDef.GetPersistentReactorIds();

            var owner = ObjectId.Null;
            for (var index = 0; index < ids.Count; index++)
            {
                var id = ids[index];
                var reactor = transaction.GetObject(id, OpenMode.ForRead);
                var name = reactor.GetRXClass().DxfName;
                if (String.Compare(name, "IMAGEDEF_REACTOR", StringComparison.OrdinalIgnoreCase) != 0) continue;

                if (reactor.OwnerId.IsErased) continue;

                var obj = transaction.GetObject(reactor.OwnerId, OpenMode.ForRead);

                if (!(obj is RasterImage)) continue;
                owner = reactor.OwnerId;
                break;
            }
            raster = transaction.GetObject(owner, OpenMode.ForRead) as RasterImage;
            pointer = imageDef.ImageCopy(false);
        }

        public static void AttachRasterImage(
            string strFileName,
            Point3d insPt,
            Database acCurDb,
            Transaction acTrans,
            DBDictionary acImgDict,
            bool isVertical,
            int scaleNumber = 2000)
        {
            var bRasterDefCreated = false;
            var strImgName = strFileName.Substring(strFileName.LastIndexOf('\\') + 1).Replace(".png", "");

            SymbolUtilityServices.ValidateSymbolName(strImgName, false);

            RasterImageDef acRasterDef;
            ObjectId acImgDefId;
            if (acImgDict.Contains(strImgName))
            {
                acImgDefId = acImgDict.GetAt(strImgName);
                acRasterDef = (RasterImageDef)acTrans.GetObject(acImgDefId, OpenMode.ForWrite);
            }

            else
            {
                var acRasterDefNew = new RasterImageDef { SourceFileName = strFileName };
                acRasterDefNew.Load();
                acImgDict.UpgradeOpen();
                acImgDefId = acImgDict.SetAt(strImgName, acRasterDefNew);
                acTrans.AddNewlyCreatedDBObject(acRasterDefNew, true);
                acRasterDef = acRasterDefNew;
                bRasterDefCreated = true;
            }

            var acBlkTbl = acTrans.GetObject(acCurDb.BlockTableId, OpenMode.ForRead) as BlockTable;

            var acBlkTblRec =
                (BlockTableRecord)acTrans.GetObject(acBlkTbl[BlockTableRecord.ModelSpace], OpenMode.ForWrite);

            using (var acRaster = new RasterImage())
            {
                acRaster.ImageDefId = acImgDefId;

                var width = isVertical ? new Vector3d(.18 * scaleNumber, 0, 0) : new Vector3d(.26 * scaleNumber, 0, 0);
                var height = isVertical ? new Vector3d(0, .24 * scaleNumber, 0) : new Vector3d(0, .16 * scaleNumber, 0);

                var location = isVertical
                    ? insPt.Add(new Vector3d(.015 * scaleNumber, .0285 * scaleNumber, 0))
                    : insPt.Add(new Vector3d(.025 * scaleNumber, (.26 + 0.0185) * scaleNumber, 0));

                var coordinateSystem = new CoordinateSystem3d(location, width, height);

                acRaster.Orientation = coordinateSystem;

                acRaster.Rotation = isVertical ? 0 : -Math.PI / 2.0;

                acBlkTblRec.AppendEntity(acRaster);
                acTrans.AddNewlyCreatedDBObject(acRaster, true);

                RasterImage.EnableReactors(true);
                acRaster.AssociateRasterDef(acRasterDef);

                if (bRasterDefCreated)
                {
                    acRasterDef.Dispose();
                }
            }
        }

        public static MText CopyMtext(MText text)
        {
            return new MText
                       {
                           Attachment = text.Attachment,
                           Location = text.Location,
                           Rotation = text.Rotation,
                           Contents = text.Contents,
                           TextHeight = text.TextHeight
                       };
        }

        public static string[] CalculateAllMappes(List<Parcel> list, int scaleNumber)
        {
            var mappes = new List<String>();
            var big = new[] { "AB", "CD" };
            var small = new[] { "abcd", "efgh", "ijkl", "mnop" };

            foreach (var item in list)
            {
                foreach (var v in item.Points)
                {
                    var x = v.P2D.X;
                    var y = v.P2D.Y;
                    int l1;
                    int l2;
                    string s;
                    switch (scaleNumber)
                    {
                        case 20000:
                            l1 = (int)y / 12000;
                            l2 = (int)x / 18000;
                            s = string.Format("{0}-{1}", l1 + 1, l2 + 1);
                            if (!mappes.Contains(s)) mappes.Add(s);
                            break;

                        case 10000:
                            l1 = (int)y / 12000;
                            l2 = (int)x / 18000;
                            s = string.Format("{0}-{1}", l1 + 1, l2 + 1);

                            y -= l1 * 12000;
                            x -= l2 * 18000;
                            l1 = (int)y / 6000;
                            l2 = (int)x / 9000;
                            s += "-" + big[1 - l1][l2];

                            if (!mappes.Contains(s)) mappes.Add(s);
                            break;

                        case 5000:
                            l1 = (int)y / 12000;
                            l2 = (int)x / 18000;
                            s = string.Format("{0}-{1}", l1 + 1, l2 + 1);

                            y -= l1 * 12000;
                            x -= l2 * 18000;
                            l1 = (int)y / 3000;
                            l2 = (int)x / 4500;
                            s += "-" + small[3 - l1][l2];

                            if (!mappes.Contains(s)) mappes.Add(s);
                            break;

                        case 2000:
                            l1 = (int)y / 12000;
                            l2 = (int)x / 18000;
                            s = string.Format("{0}-{1}", l1 + 1, l2 + 1);

                            y -= l1 * 12000;
                            x -= l2 * 18000;
                            l1 = (int)y / 1200;
                            l2 = (int)x / 1800;
                            s += "-" + ((9 - l1) * 10 + l2 + 1);

                            if (!mappes.Contains(s)) mappes.Add(s);
                            break;
                        case 1000:
                            l1 = (int)y / 12000;
                            l2 = (int)x / 18000;
                            s = string.Format("{0}-{1}", l1 + 1, l2 + 1);

                            y -= l1 * 12000;
                            x -= l2 * 18000;
                            l1 = (int)y / 1200;
                            l2 = (int)x / 1800;
                            s += "-" + ((9 - l1) * 10 + l2 + 1);

                            y -= l1 * 1200;
                            x -= l2 * 1800;
                            l1 = (int)y / 600;
                            l2 = (int)x / 900;
                            s += "-" + big[1 - l1][l2];

                            if (!mappes.Contains(s)) mappes.Add(s);
                            break;
                        case 500:
                            l1 = (int)y / 12000;
                            l2 = (int)x / 18000;
                            s = string.Format("{0}-{1}", l1 + 1, l2 + 1);

                            y -= l1 * 12000;
                            x -= l2 * 18000;
                            l1 = (int)y / 1200;
                            l2 = (int)x / 1800;
                            s += "-" + ((9 - l1) * 10 + l2 + 1);

                            y -= l1 * 1200;
                            x -= l2 * 1800;
                            l1 = (int)y / 300;
                            l2 = (int)x / 450;
                            s += "-" + small[3 - l1][l2];

                            if (!mappes.Contains(s)) mappes.Add(s);
                            break;
                    }
                }
            }
            return mappes.ToArray();
        }

        public static IGeometry GetBiggestConvexPolygon(List<Coordinate> coordinates)
        {
            var tess = new LibTessDotNet.Tess();
            var numPoints = coordinates.Count;
            var contour = new LibTessDotNet.ContourVertex[numPoints];
            for (var i = 0; i < numPoints; i++)
            {
                contour[i].Position = new LibTessDotNet.Vec3
                {
                    X = (float)coordinates[i].X,
                    Y = (float)coordinates[i].Y,
                    Z = 0.0f
                };
            }

            tess.AddContour(contour, LibTessDotNet.ContourOrientation.Clockwise);

            tess.Tessellate(LibTessDotNet.WindingRule.EvenOdd, LibTessDotNet.ElementType.Polygons, numPoints);

            var output = new List<List<Coordinate>>();
            for (var i = 0; i < tess.ElementCount; i++)
            {
                var poly = new List<Coordinate>();
                for (var j = 0; j < numPoints; j++)
                {
                    var index = tess.Elements[i * numPoints + j];
                    if (index == -1) continue;
                    var v = new Coordinate { X = tess.Vertices[index].Position.X, Y = tess.Vertices[index].Position.Y, };
                    poly.Add(v);
                }
                output.Add(poly);
            }

            var polygons =
                output.ToDictionary(l => l, l => PolygonGraphicArea(l.ToArray())).OrderByDescending(e => e.Value);
            var first = polygons.First().Key;
            first.Add(first.First());
            return new LinearRing(first.ToArray());
        }

        public static ObjectId[] GetAllEntitiesFromLayer(params string[] layers)
        {
            var tvs = new[] { new TypedValue((int)DxfCode.LayerName, string.Join(",", layers)) };

            var editor = App.DocumentManager.MdiActiveDocument.Editor;
            var result = editor.SelectAll(new SelectionFilter(tvs));
            return result.Status == PromptStatus.OK ? result.Value.GetObjectIds() : null;
        }

        // ReSharper disable once InconsistentNaming
        public static List<Point[]> SeparateFreePoints(Parcel parcel, List<Point> orderedFreePoints)
        {
            var separated = new List<List<Point>>();
            if (orderedFreePoints.Count <= 2)
            {
                separated.Add(orderedFreePoints);
                return separated.Select(s => s.ToArray()).ToList();
            }

            var entry = new List<Point>();
            for (var i = 0; i < orderedFreePoints.Count - 2; i++)
            {
                var p1 = orderedFreePoints[i];
                var p2 = orderedFreePoints[i + 1];
                var p3 = orderedFreePoints[i + 2];

                var index1 = parcel.Points.IndexOf(p1);
                var index2 = parcel.Points.IndexOf(p2);
                var index3 = parcel.Points.IndexOf(p3);

                var diff1 = Math.Abs(index1 - index2);
                var diff2 = Math.Abs(index2 - index3);
                var n = parcel.Points.Count - 1;

                if ((diff1 == 1 || diff1 == n) && (diff2 == 1 || diff2 == n))
                {
                    var vec1 = p2.P3D - p1.P3D;
                    var vec2 = p3.P3D - p2.P3D;

                    var cos = vec1.DotProduct(vec2) / (vec2.Length * vec1.Length);

                    if (!p3.Name.StartsWith("B") && entry.Any(p => p.Name.StartsWith("" + p3.Name[0])))
                    {
                        if (!entry.Contains(p1)) entry.Add(p1);
                        if (!entry.Contains(p2)) entry.Add(p2);
                        if (!entry.Contains(p3)) entry.Add(p3);
                    }
                    else if (cos > Math.Cos(Math.PI / 4))
                    {
                        if (!entry.Contains(p1)) entry.Add(p1);
                        if (!entry.Contains(p2)) entry.Add(p2);
                        if (!entry.Contains(p3)) entry.Add(p3);
                    }
                    else
                    {
                        if (!entry.Contains(p1)) entry.Add(p1);
                        if (!entry.Contains(p2)) entry.Add(p2);
                        if (!entry.Contains(p3) && !p2.Name.StartsWith("B") && p3.Name.StartsWith("B")) entry.Add(p3);
                        separated.Add(entry);
                        entry = new List<Point>();
                    }
                }
                else if (diff1 == 1)
                {
                    if (!entry.Contains(p1)) entry.Add(p1);
                    if (!entry.Contains(p2)) entry.Add(p2);
                    separated.Add(entry);
                    entry = new List<Point>();
                }
                else
                {
                    if (entry.Count > 1)
                        separated.Add(entry);
                    entry = new List<Point>();
                }
            }
            if (!separated.Contains(entry))
                separated.Add(OrderPoints(entry, parcel));
            return separated.Select(s => s.ToArray()).ToList();
        }

        public static List<Point3d> AggregatePoints(List<Point3d> points, double tolerance)
        {
            for (var i = 1; i < points.Count - 1; i++)
            {
                var line = new Line(points[i - 1], points[i + 1]);
                var closest = line.GetClosestPointTo(points[i], true);
                if (points[i].DistanceTo(closest) < tolerance)
                {
                    points.RemoveAt(i);
                    i--;
                }
                line.Dispose();
            }
            return points;
        }

        public static IGeometry GetConvexHull(Property property)
        {
            var coordinates = new List<Coordinate>();
            property.Parcels.ForEach(p => coordinates.AddRange(p.Points.Select(t => t.Coordinate)));

            var algorithm = new ConvexHull(coordinates.ToArray(), new GeometryFactory());
            return algorithm.GetConvexHull();
        }

        public static void UnzipFromStream(Stream zipStream, string outFolder)
        {
            var zipInputStream = new ZipInputStream(zipStream);
            var zipEntry = zipInputStream.GetNextEntry();
            while (zipEntry != null)
            {
                var entryFileName = zipEntry.Name;
                var buffer = new byte[4096];

                var fullZipToPath = System.IO.Path.Combine(outFolder, entryFileName);
                var directoryName = System.IO.Path.GetDirectoryName(fullZipToPath);
                if (!string.IsNullOrEmpty(directoryName))
                    // ReSharper disable once AssignNullToNotNullAttribute
                    Directory.CreateDirectory(directoryName);

                using (var streamWriter = File.Create(fullZipToPath))
                {
                    StreamUtils.Copy(zipInputStream, streamWriter, buffer);
                }
                zipEntry = zipInputStream.GetNextEntry();
            }
        }

        public static void DeleteDirectory(string directoryName)
        {
            var info = new DirectoryInfo(directoryName);

            foreach (var file in info.GetFiles())
            {
                file.Delete();
            }
            foreach (var dir in info.GetDirectories())
            {
                dir.Delete(true);
            }
            Directory.Delete(directoryName);
        }

        public static void CreateSample(string outPathname, string folderName)
        {
            var fsOut = File.Create(outPathname);
            var zipStream = new ZipOutputStream(fsOut);

            zipStream.SetLevel(9);

            var folderOffset = folderName.Length + (folderName.EndsWith("\\") ? 0 : 1);

            CompressFolder(folderName, zipStream, folderOffset);

            zipStream.IsStreamOwner = true;
            zipStream.Close();
        }

        private static void CompressFolder(string path, ZipOutputStream zipStream, int folderOffset)
        {
            var files = Directory.GetFiles(path);

            foreach (var filename in files)
            {
                var fi = new FileInfo(filename);

                var entryName = filename.Substring(folderOffset);
                entryName = ZipEntry.CleanName(entryName);
                var newEntry = new ZipEntry(entryName) { DateTime = fi.LastWriteTime };

                zipStream.UseZip64 = UseZip64.Off;
                newEntry.Size = fi.Length;

                zipStream.PutNextEntry(newEntry);

                var buffer = new byte[4096];
                using (var streamReader = File.OpenRead(filename))
                {
                    StreamUtils.Copy(streamReader, zipStream, buffer);
                }
                zipStream.CloseEntry();
            }
            var folders = Directory.GetDirectories(path);
            foreach (var folder in folders)
            {
                CompressFolder(folder, zipStream, folderOffset);
            }
        }

        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            return false;
        }

        public static Polyline SplitPolyline(Point3dCollection offset, Polyline subj)
        {
            var clip = new List<List<IntPoint>>(1) { new List<IntPoint>() };
            for (var i = 0; i < offset.Count; i++)
            {
                var p = offset[i];
                clip[0].Add(new IntPoint((long)(p.X * 100.0), (long)(p.Y * 100.0)));
            }


            var subject = new List<List<IntPoint>>(1) { new List<IntPoint>() };
            for (var i = 0; i < subj.NumberOfVertices; i++)
            {
                var p = subj.GetPoint3dAt(i);
                subject[0].Add(new IntPoint((long)(p.X * 100.0), (long)(p.Y * 100.0)));
            }

            var solution = new List<List<IntPoint>>();

            var clipper = new Clipper();
            clipper.AddPaths(subject, PolyType.ptSubject, false);
            clipper.AddPaths(clip, PolyType.ptClip, true);
            clipper.Execute(ClipType.ctIntersection, solution, PolyFillType.pftEvenOdd, PolyFillType.pftEvenOdd);

            var n = solution[0].Count;

            if (n < 2) return null;

            var polyline = new Polyline();
            for (var i = 0; i < n; i++)
            {
                polyline.AddVertexAt(i, new Point2d(solution[0][i].X / 100.0, solution[0][i].Y / 100.0), 0, 0, 0);
            }

            return polyline;
        }

        public static Polyline SplitPolyline(Point3dCollection offset, Line subj)
        {
            var clip = new List<List<IntPoint>>(1) { new List<IntPoint>() };
            for (var i = 0; i < offset.Count; i++)
            {
                var p = offset[i];
                clip[0].Add(new IntPoint((long)(p.X * 100.0), (long)(p.Y * 100.0)));
            }


            var subject = new List<List<IntPoint>>(1) { new List<IntPoint>() };
            subject[0].Add(new IntPoint((long)(subj.StartPoint.X * 100.0), (long)(subj.StartPoint.Y * 100.0)));
            subject[0].Add(new IntPoint((long)(subj.EndPoint.X * 100.0), (long)(subj.EndPoint.Y * 100.0)));

            var solution = new List<List<IntPoint>>();

            var clipper = new Clipper();
            clipper.AddPaths(subject, PolyType.ptSubject, false);
            clipper.AddPaths(clip, PolyType.ptClip, true);
            clipper.Execute(ClipType.ctIntersection, solution, PolyFillType.pftEvenOdd, PolyFillType.pftEvenOdd);

            var n = solution[0].Count;

            if (n < 2) return null;

            var polyline = new Polyline();
            for (var i = 0; i < n; i++)
            {
                polyline.AddVertexAt(i, new Point2d(solution[0][i].X / 100.0, solution[0][i].Y / 100.0), 0, 0, 0);
            }

            return polyline;
        }

        public static Polyline SplitPolyline(Point3dCollection offset, Polyline2d subj)
        {
            var clip = new List<List<IntPoint>>(1) { new List<IntPoint>() };
            for (var i = 0; i < offset.Count; i++)
            {
                var p = offset[i];
                clip[0].Add(new IntPoint((long)(p.X * 100.0), (long)(p.Y * 100.0)));
            }


            var subject = new List<List<IntPoint>>(1) { new List<IntPoint>() };
            subject[0].Add(new IntPoint((long)(subj.StartPoint.X * 100.0), (long)(subj.StartPoint.Y * 100.0)));
            subject[0].Add(new IntPoint((long)(subj.EndPoint.X * 100.0), (long)(subj.EndPoint.Y * 100.0)));

            var solution = new List<List<IntPoint>>();

            var clipper = new Clipper();
            clipper.AddPaths(subject, PolyType.ptSubject, false);
            clipper.AddPaths(clip, PolyType.ptClip, true);
            clipper.Execute(ClipType.ctIntersection, solution, PolyFillType.pftEvenOdd, PolyFillType.pftEvenOdd);

            var n = solution[0].Count;

            if (n < 2) return null;

            var polyline = new Polyline();
            for (var i = 0; i < n; i++)
            {
                polyline.AddVertexAt(i, new Point2d(solution[0][i].X / 100.0, solution[0][i].Y / 100.0), 0, 0, 0);
            }

            return polyline;
        }

        // ReSharper disable once InconsistentNaming
        public static List<PassageInfo> GetPisteWidth(Parcel parcel, Property property, Dictionary<Parcel, Point3d[]> piste,
            Dictionary<Parcel, List<Parcel>> riverainsParcels, List<Point> freePoints)
        {
            var directions = new[] { new[] { "Nord", "Sud" }, new[] { "Ouest", "Est" } };

            var dictionary = new List<PassageInfo>();
            var buffer = ExtrudePoints(freePoints);

            //var editor = App.DocumentManager.MdiActiveDocument.Editor;

            if (property.Parcels.Count > 1)
                foreach (var p in property.Parcels.Where(p => !p.Equals(parcel)))
                {
                    piste.Add(p, p.Points.Where(pt => PointInPolygon(buffer, pt.P3D))
                        .Select(pt => pt.P3D).ToArray());
                }

            var separated = SeparateFreePoints(parcel, OrderPoints(freePoints, parcel));

            var found = false;
            foreach (var entry in separated)
            {
                if (entry.Length < 2) continue;

                var p1 = entry[entry.Length / 2].P3D;
                var p2 = entry[entry.Length / 2 - 1].P3D;
                var vec1 = p2 - p1;

                foreach (var e in piste)
                {
                    var pts = e.Value;
                    if (pts.Length < 2) continue;
                    for (var i = 0; i < pts.Length - 1; i++)
                    {
                        var p3 = pts[i];
                        var p4 = pts[i + 1];
                        var vec2 = p4 - p3;

                        var cos = Math.Abs(vec1.DotProduct(vec2) / (vec2.Length * vec1.Length));
                        if (cos < Math.Cos(Math.PI / 90)) continue;

                        var vec3 = p1 - p3;
                        cos = Math.Abs(vec1.DotProduct(vec3) / (vec3.Length * vec1.Length));
                        if (cos > Math.Cos(Math.PI / 90)) continue;

                        var line = new Line(p3, p4);
                        var closest = line.GetClosestPointTo(p1, true);
                        var width = p1.DistanceTo(closest);
                        line.Dispose();

                        if (!(width < 22)) continue;

                        p3 = entry.First().P3D;
                        p4 = entry.Last().P3D;
                        vec2 = p4 - p3;

                        var vec4 = vec2.RotateBy(Math.PI / 2, Vector3d.ZAxis) / vec2.Length;
                        var mid = p3.Add(vec2 / 2).Add(vec4);

                        var angle = Vector3d.XAxis.GetAngleTo(vec2, Vector3d.ZAxis);
                        var sign1 = Math.Abs(Math.Tan(angle)) > Math.Tan(Math.PI / 4) ? 1 : 0;

                        var a = vec2.Y / vec2.X;
                        var b = p3.Y - a * p3.X;

                        var above = mid.Y > a * mid.X + b;
                        var positive = Math.Tan(angle) > 0;

                        var sign2 = above ? 1 : 0;
                        if (sign1 == 1)
                        {
                            if (above && positive)
                            {
                                sign2 = 1;
                            }
                            else if (above)
                            {
                                sign2 = 0;
                            }
                            else if (positive)
                            {
                                sign2 = 0;
                            }
                            else
                            {
                                sign2 = 1;
                            }
                        }
                        

                        dictionary.Add(new PassageInfo { Points = entry, Side = directions[sign1][sign2], Width = (int)Math.Round(width) });

                        found = true;
                        break;
                    }
                    if (!found) continue;
                    found = false;
                    break;
                }
                if (dictionary.All(p => p.Points != entry))
                {
                    var p3 = entry.First().P3D;
                    var p4 = entry.Last().P3D;
                    var vec2 = p4 - p3;

                    var vec4 = vec2.RotateBy(Math.PI / 2, Vector3d.ZAxis) / vec2.Length;
                    var mid = p3.Add(vec2 / 2).Add(vec4);

                    var angle = Vector3d.XAxis.GetAngleTo(vec2, Vector3d.ZAxis);
                    var sign1 = Math.Abs(Math.Tan(angle)) > Math.Tan(Math.PI / 4) ? 1 : 0;

                    var a = vec2.Y / vec2.X;
                    var b = p3.Y - a * p3.X;

                    var above = mid.Y > a * mid.X + b;
                    var positive = Math.Tan(angle) > 0;

                    var sign2 = above ? 1 : 0;
                    if (sign1 == 1)
                    {
                        if (above && positive)
                        {
                            sign2 = 1;
                        }
                        else if (above)
                        {
                            sign2 = 0;
                        }
                        else if (positive)
                        {
                            sign2 = 0;
                        }
                        else
                        {
                            sign2 = 1;
                        }
                    }

                    dictionary.Add(new PassageInfo { Points = entry, Side = directions[sign1][sign2], Width = 0 });
                }
            }

            foreach (var p in property.Parcels.Where(p => piste.Keys.Contains(p)))
                piste.Remove(p);

            return dictionary;
        }
    }
}
