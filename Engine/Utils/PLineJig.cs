﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;

namespace Engine.Utils
{
    public class PLineJig : EntityJig
    {
        // Maintain a list of vertices...
        // Not strictly necessary, as these will be stored in the
        // polyline, but will not adversely impact performance
        readonly Point3dCollection _mPts;
        // Use a separate variable for the most recent point...
        // Again, not strictly necessary, but easier to reference
        Point3d _mTempPoint;
        readonly Plane _mPlane;

        public PLineJig(Matrix3d ucs)
            : base(new Polyline())
        {
            // Create a point collection to store our vertices
            _mPts = new Point3dCollection();

            // Create a temporary plane, to help with calcs
            var origin = new Point3d(0, 0, 0);
            var normal = new Vector3d(0, 0, 1);
            normal = normal.TransformBy(ucs);
            _mPlane = new Plane(origin, normal);

            // Create polyline, set defaults, add dummy vertex
            var pline = (Polyline)Entity;
            pline.SetDatabaseDefaults();
            pline.Normal = normal;
            pline.AddVertexAt(0, new Point2d(0, 0), 0, 0, 0);
        }

        protected override SamplerStatus Sampler(JigPrompts prompts)
        {
            var jigOpts = new JigPromptPointOptions
                {
                    UserInputControls = (UserInputControls.Accept3dCoordinates |
                                         UserInputControls.NullResponseAccepted |
                                         UserInputControls.NoNegativeResponseAccepted
                        )
                };

            if (_mPts.Count == 0)
            {
                // For the first vertex, just ask for the point
                jigOpts.Message =
                    "\nPremière Borne : ";
            }
            else if (_mPts.Count > 0)
            {
                // For subsequent vertices, use a base point
                jigOpts.BasePoint = _mPts[_mPts.Count - 1];
                jigOpts.UseBasePoint = true;
                jigOpts.Message = "\nBorne/Point de détail Suivant : ";
            }
            else // should never happen
                return SamplerStatus.Cancel;

            // Get the point itself
            var res =
                prompts.AcquirePoint(jigOpts);

            // Check if it has changed or not
            // (reduces flicker)
            if (_mTempPoint == res.Value)
            {
                return SamplerStatus.NoChange;
            }
            else if (res.Status == PromptStatus.OK)
            {
                _mTempPoint = res.Value;
                return SamplerStatus.OK;
            }
            return SamplerStatus.Cancel;
        }

        protected override bool Update()
        {
            // Update the dummy vertex to be our
            // 3D point projected onto our plane
            var pline = (Polyline)Entity;
            pline.SetPointAt(
                pline.NumberOfVertices - 1,
                _mTempPoint.Convert2d(_mPlane)
                );
            return true;
        }

        public Entity GetEntity()
        {
            return Entity;
        }

        public void AddLatestVertex()
        {
            // Add the latest selected point to
            // our internal list...
            // This point will already be in the
            // most recently added pline vertex
            _mPts.Add(_mTempPoint);
            var pline = (Polyline)Entity;
            // Create a new dummy vertex...
            // can have any initial value
            pline.AddVertexAt(
                pline.NumberOfVertices,
                new Point2d(0, 0),
                0, 0, 0
                );
        }

        public void RemoveLastVertex()
        {
            // Let's remove our dummy vertex
            var pline = (Polyline)Entity;
            pline.RemoveVertexAt(_mPts.Count);
        }
    }
}