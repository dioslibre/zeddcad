﻿using System;
using System.ComponentModel;
using System.IO;
using System.Xml.Serialization;
using Autodesk.AutoCAD.DatabaseServices;
using Engine.Annotations;
// ReSharper disable InconsistentNaming

#pragma warning disable 618

namespace Engine.Application
{
    [Serializable]
    public class ProjectInfo : INotifyPropertyChanged
    {
        public string Secteur { get; set; }
        public string Path { get; set; }
        public string DbPath
        {
            get { return SSPath + @"\Base de Données\SS" + SubSector + ".dbz"; }
        }
        public string ProvinceFr { get; set; }
        public string ProvinceAr { get; set; }
        public string CommuneFr { get; set; }
        public string CommuneAr { get; set; }
        public string CercleFr { get; set; }
        public string CercleAr { get; set; }
        public string LivrablesPath { get { return SSPath + @"\Livrables"; } }
        public string Date { get; set; }
        public string EtablitPar { get; set; }
        public string Geometre { get; set; }
        public string Dessinateur { get { return "Automatique"; } }
        public string Carte { get; set; }
        public int SubSector { get; set; }
        public string Cadastre { get; set; }
        public string SSPath { get; set; }

        public static ProjectInfo LoadFrom(Database db)
        {
            var ndoId = db.NamedObjectsDictionaryId;
            ProjectInfo info;
            using (var tx = db.TransactionManager.StartTransaction())
            {
                var nod = (DBDictionary)tx.GetObject(ndoId, OpenMode.ForRead);
                var projectRecord = (Xrecord)tx.GetObject(nod.GetAt("Project"), OpenMode.ForRead);

                var rb = projectRecord.Data;

                var serializer = new XmlSerializer(typeof(ProjectInfo));
                using (var stream = new MemoryStream())
                {
                    var tvs = rb.AsArray();
                    if (tvs == null) throw new Exception();
                    if (tvs[0].TypeCode != (short)DxfCode.Text) throw new Exception();
                    TextWriter writer = new StreamWriter(stream);
                    for (var i = 1; i < tvs.Length; i++)
                    {
                        if (tvs[i].TypeCode != (short)DxfCode.Text) continue;
                        var xmlString = (string)tvs[i].Value;
                        writer.Write(xmlString);
                    }
                    writer.Flush();
                    stream.Position = 0;
                    info = serializer.Deserialize(stream) as ProjectInfo;
                }

                rb.Dispose();
            }
            return info;
        }

        public void Save(Database db)
        {
            using (var tx = db.TransactionManager.StartTransaction())
            {
                var rb = new ResultBuffer();
                using (var stream = new MemoryStream())
                {
                    var serializer = new XmlSerializer(typeof (ProjectInfo));
                    serializer.Serialize(stream, this);
                    stream.Position = 0;
                    TextReader reader = new StreamReader(stream);
                    var xmlString = reader.ReadToEnd();
                    rb.Add(new TypedValue((int) DxfCode.Text, GetType().FullName));
                    while (xmlString.Length > 0)
                    {
                        string buffer;
                        if (xmlString.Length >= 1024)
                        {
                            buffer = xmlString.Substring(0, 1024);
                            xmlString = xmlString.Substring(1024);
                        }
                        else
                        {
                            buffer = xmlString;
                            xmlString = "";
                        }
                        rb.Add(new TypedValue((int) DxfCode.Text, buffer));
                    }
                }

                var nodId = db.NamedObjectsDictionaryId;
                var record = new Xrecord {Data = rb};

                var nod = (DBDictionary) tx.GetObject(nodId, OpenMode.ForWrite);
                nod.SetAt("Project", record);

                rb.Dispose();
                record.Dispose();
                tx.Commit();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
