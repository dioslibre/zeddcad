﻿using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;
using Engine.Application;
using Engine.Properties;
using GeoAPI.Geometries;
using NetTopologySuite.Geometries;
using App = Autodesk.AutoCAD.ApplicationServices.Application;
using Polyline = Autodesk.AutoCAD.DatabaseServices.Polyline;

#pragma warning disable 618

namespace Engine
{
    using System.Linq;

    using Autodesk.AutoCAD.Colors;
    using System.IO;

    static class ExtensionMethods
    {
        public static bool IsProjectDatabase(this Database db)
        {
            using (var transaction = db.TransactionManager.StartTransaction())
            {
                var nodId = db.NamedObjectsDictionaryId;
                var nod = (DBDictionary) transaction.GetObject(nodId, OpenMode.ForRead);
                
                var isProject = nod.Contains("Project");

                if (!isProject)
                {
                    transaction.Dispose();
                    return false;
                }
            }

            var project = ProjectInfo.LoadFrom(db);
            project.SSPath = Path.GetDirectoryName(db.Filename);
            // ReSharper disable once AssignNullToNotNullAttribute
            project.Path = Directory.GetParent(Directory.GetParent(project.SSPath).FullName).FullName;
            project.Save(db);

            return true;
        }

        public static IGeometry GetGeometry(this Polyline polyline)
        {
            var points = new List<Coordinate>();
            for (var i = 0; i < polyline.NumberOfVertices; i++)
            {
                var point = polyline.GetPoint2dAt(i);
                points.Add(new Coordinate(point.X, point.Y));
            }
            points.Add(points[0]);

            return new LinearRing(points.ToArray());
        }

        public static Point3dCollection GetPoint3DCollection(this Polyline polyline)
        {
            var points = new Point3dCollection();
            for (var i = 0; i < polyline.NumberOfVertices; i++)
            {
                var point = polyline.GetPoint2dAt(i);
                points.Add(new Point3d(point.X, point.Y, 0.0));
            }

            return points;
        }

        public static void CreateCalqueLayers(this Database database, Transaction transaction)
        {
            var ltId = database.LayerTableId;
            var layerTable = (LayerTable)transaction.GetObject(ltId, OpenMode.ForWrite);
            LayerTableRecord layer;
            layerTable.Add(layer = new LayerTableRecord { Name = Settings.Default.Lim });
            transaction.AddNewlyCreatedDBObject(layer, true);
            layerTable.Add(layer = new LayerTableRecord { Name = Settings.Default.Blk });
            transaction.AddNewlyCreatedDBObject(layer, true);
            layerTable.Add(layer = new LayerTableRecord { Name = Settings.Default.BrnId });
            transaction.AddNewlyCreatedDBObject(layer, true);
            layerTable.Add(layer = new LayerTableRecord { Name = Settings.Default.Dtl });
            transaction.AddNewlyCreatedDBObject(layer, true);
            layerTable.Add(layer = new LayerTableRecord { Name = Settings.Default.Frm });
            transaction.AddNewlyCreatedDBObject(layer, true);
            layerTable.Add(layer = new LayerTableRecord { Name = Settings.Default.PrId });
            transaction.AddNewlyCreatedDBObject(layer, true);
            layerTable.Add(layer = new LayerTableRecord { Name = Settings.Default.Riv });
            transaction.AddNewlyCreatedDBObject(layer, true);
            layerTable.Add(layer = new LayerTableRecord { Name = Settings.Default.Txt });
            transaction.AddNewlyCreatedDBObject(layer, true);
            layerTable.Add(layer = new LayerTableRecord { Name = Settings.Default.Vbr });
            transaction.AddNewlyCreatedDBObject(layer, true);
        }

        public static void CreatePhotoA4Layers(this Database database)
        {
            using (var transaction = database.TransactionManager.StartTransaction())
            {
                var ltId = database.LayerTableId;
                var layerTable = (LayerTable)transaction.GetObject(ltId, OpenMode.ForWrite);
                LayerTableRecord layer;

                layerTable.Add(layer = new LayerTableRecord { Name = Settings.Default.Lim });
                transaction.AddNewlyCreatedDBObject(layer, true);
                layerTable.Add(layer = new LayerTableRecord { Name = Settings.Default.Frm, Color = Color.FromColorIndex(ColorMethod.ByAci, 7) });
                transaction.AddNewlyCreatedDBObject(layer, true);
                layerTable.Add(layer = new LayerTableRecord { Name = Settings.Default.Prps });
                transaction.AddNewlyCreatedDBObject(layer, true);
                layerTable.Add(layer = new LayerTableRecord { Name = Settings.Default.Txt, Color = Color.FromColorIndex(ColorMethod.ByAci, 7) });
                transaction.AddNewlyCreatedDBObject(layer, true);

                transaction.Commit();
            }
        }

        public static void CreateTextStyles(this Database database)
        {
            using (var transaction = database.TransactionManager.StartTransaction())
            {
                var stId = database.TextStyleTableId;
                var styleTable = (TextStyleTable)transaction.GetObject(stId, OpenMode.ForWrite);

                var str = new TextStyleTableRecord
                {
                    XScale = 0.75,
                    Name = "PH_" + Settings.Default.Prps
                };
                styleTable.Add(str);
                str.Font = new FontDescriptor("Times New Roman", true, false, 0, 0);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord { FileName = "Arial", Name = "PH_" + Settings.Default.Txt };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "romans.shx",
                    XScale = 1.125,
                    ObliquingAngle = 0.523598775598299,
                    Name = "CQ_" + Settings.Default.Riv
                };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "romans.shx",
                    XScale = 0.75,
                    ObliquingAngle = 0.523598775598299,
                    Name = "CQ_" + Settings.Default.Vbr
                };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "romans.shx",
                    XScale = 0.75,
                    Name = "CQ_" + Settings.Default.BrnId
                };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "romant.shx",
                    XScale = .45,
                    ObliquingAngle = 0.523598775598299,
                    Name = "CQ_" + Settings.Default.Pdite
                };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "romant.shx",
                    XScale = 0.45,
                    ObliquingAngle = 0.523598775598299,
                    Name = "CQ_" + Settings.Default.PditeV
                };

                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                // ROMANT
                str = new TextStyleTableRecord { Name = "CQ_ROMANT" };
                styleTable.Add(str);
                str.Font = new FontDescriptor("Times New Roman", false, true, 0, 0);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "romans.shx",
                    XScale = .65,
                    ObliquingAngle = 0.523598775598299,
                    Name = "CQ_" + Settings.Default.Sitn
                };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "romand.shx",
                    XScale = 1,
                    ObliquingAngle = 0,
                    Name = "CQ_" + Settings.Default.Titr
                };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "romand.shx",
                    XScale = 1,
                    ObliquingAngle = 0,
                    Name = "CQ_" + Settings.Default.Ech
                };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "romant.shx",
                    XScale = 1,
                    ObliquingAngle = 0,
                    Name = "CQ_" + Settings.Default.Cdtr
                };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "monotxt.shx",
                    XScale = .85,
                    ObliquingAngle = 0,
                    Name = "CQ_" + Settings.Default.Coord
                };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "romanc.shx",
                    XScale = .8,
                    ObliquingAngle = 0.349065850398866,
                    Name = "CQ_" + Settings.Default.Tbl
                };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "romanc.shx",
                    XScale = 1,
                    ObliquingAngle = 0,
                    Name = "CQ_" + Settings.Default.Ctnc
                };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "romans.shx",
                    XScale = 1.5,
                    ObliquingAngle = 0,
                    Name = "CQ_" + Settings.Default.Cst
                };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "italicc.shx",
                    XScale = 1,
                    ObliquingAngle = 0,
                    Name = "CQ_" + Settings.Default.PrId
                };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "romans.shx",
                    XScale = 1.125,
                    ObliquingAngle = 0.523598775598299,
                    Name = "CQ_" + Settings.Default.Map
                };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord { FileName = "mohammad bold art 1", Name = "CQ_" + Settings.Default.Txa };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "romans.shx",
                    XScale = 0.75,
                    ObliquingAngle = Math.PI / 6,
                    Name = "PE_" + Settings.Default.Lgth
                };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "romans.shx",
                    XScale = 0.8,
                    Name = "PE_" + Settings.Default.PclId
                };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    XScale = 0.75,
                    Name = "PE_" + Settings.Default.Prps
                };
                styleTable.Add(str);
                str.Font = new FontDescriptor("Times New Roman", true, false, 0, 0);
                transaction.AddNewlyCreatedDBObject(str, true);

                str = new TextStyleTableRecord
                {
                    FileName = "romans.shx",
                    XScale = 0.75,
                    Name = "PE_" + Settings.Default.BrnId
                };
                styleTable.Add(str);
                transaction.AddNewlyCreatedDBObject(str, true);

                transaction.Commit();
            }
        }

        public static void ZoomExtents(this Database db, Transaction tr)
        {
            db.TileMode = true;
            db.UpdateExt(true);
            var scrSize = (Point2d)App.GetSystemVariable("screensize");
            var ratio = scrSize.X / scrSize.Y;
            using (var line = new Line(db.Extmin, db.Extmax))
            {
                var vpt = (ViewportTable)tr.GetObject(db.ViewportTableId, OpenMode.ForRead);
                var vptr = (ViewportTableRecord)tr.GetObject(vpt["*Active"], OpenMode.ForWrite);
                var ext = line.GeometricExtents;
                ext.TransformBy(vptr.WorldToEye());
                var pmin = new Point2d(ext.MinPoint.X, ext.MinPoint.Y);
                var pmax = new Point2d(ext.MaxPoint.X, ext.MaxPoint.Y);
                var height = pmax.Y - pmin.Y;
                var width = pmax.X - pmin.X;
                if (width / height < ratio)
                {
                    vptr.Height = height;
                    vptr.Width = height * ratio;
                }
                else
                {
                    vptr.Width = width;
                    vptr.Height = width / ratio;
                }
                vptr.CenterPoint = pmin + (pmax - pmin) / 2.0;
                db.UpdateExt(true);
            }
        }

        public static Matrix3d WorldToEye(this ViewportTableRecord view)
        {
            return view.EyeToWorld().Inverse();
        }

        public static Matrix3d EyeToWorld(this ViewportTableRecord view)
        {
            return
                Matrix3d.Rotation(-view.ViewTwist, view.ViewDirection, view.Target) *
                Matrix3d.Displacement(view.Target - Point3d.Origin) *
                Matrix3d.PlaneToWorld(view.ViewDirection);
        }

        public static void CopyLineTypes(this Database database, Database source)
        {
            using (var tr = source.TransactionManager.StartTransaction())
            {
                var ltId = source.LinetypeTableId;
                var ltTable = (LinetypeTable)tr.GetObject(ltId, OpenMode.ForRead);
                var oIdCollection = new ObjectIdCollection();
                foreach (var id in ltTable)
                {
                    oIdCollection.Add(id);
                }

                source.WblockCloneObjects(oIdCollection, database.LinetypeTableId, new IdMapping(), DuplicateRecordCloning.Ignore, false);
            }
        }

        public static string GetPropertyId(this Polyline polyline)
        {
            var rb = polyline.XData;
            var values = rb.AsArray();
            rb.Dispose();

            return values.Any() ? values[1].Value.ToString() : null;
        }

        public static string GetString(this MText text)
        {
            var rb = text.XData;
            var values = rb.AsArray();
            rb.Dispose();

            return values.Any() ? values[1].Value.ToString() : null;
        }

        public static void SetPropertyId(this Polyline polyline, string propertyId, RegAppTable regAppTable, Transaction transaction)
        {
            if (!regAppTable.Has("Untitled"))
            {
                var record = new RegAppTableRecord { Name = "Untitled" };
                regAppTable.Add(record);
                transaction.AddNewlyCreatedDBObject(record, true);
            }

            var rb = new ResultBuffer(
              new TypedValue(1001, "Untitled"),
              new TypedValue(1000, propertyId)
            );
            polyline.XData = rb;
        }

        public static void SetString(this MText text, string s, RegAppTable regAppTable, Transaction transaction)
        {
            if (!regAppTable.Has("Untitled"))
            {
                var record = new RegAppTableRecord { Name = "Untitled" };
                regAppTable.Add(record);
                transaction.AddNewlyCreatedDBObject(record, true);
            }

            var rb = new ResultBuffer(
              new TypedValue(1001, "Untitled"),
              new TypedValue(1000, s)
            );
            text.XData = rb;
        }
    }
}
