﻿using System;
using System.Windows.Forms;
using System.Windows.Input;
using Autodesk.Windows;
using App = Autodesk.AutoCAD.ApplicationServices.Application;

namespace Engine
{
    public class AcadMainWindow : IWin32Window
    {
        public IntPtr Handle { get; set; }
    }

    class Handler : ICommand
    {

        bool ICommand.CanExecute(object parameter)
        {
            return true;
        }

        event EventHandler ICommand.CanExecuteChanged
        {
            add { }
            remove { }
        }

        void ICommand.Execute(object parameter)
        {
            var ribbonButton = parameter as RibbonButton;
            if (ribbonButton == null) return;
            var id = ribbonButton.Id;
            try
            {
                App.DocumentManager.MdiActiveDocument.SendStringToExecute(id + "\n", true, false, false);
            }
            catch (Exception e)
            {
                App.DocumentManager.MdiActiveDocument.Editor.WriteMessage(e.ToString());
            }
        }
    }
}