﻿using System;
using System.IO;
using System.Windows.Forms;
using Engine.Utils;

namespace Engine.Forms
{
    public partial class LoadGpsPoinsForm : Form
    {
        public OpenFileDialog FileDialog { get; set; }
        public LoadGpsPoinsForm()
        {
            InitializeComponent();
            FileDialog = new OpenFileDialog
            {
                CheckFileExists = true,
                Filter = @"Fichier HTML | *.html",
                Multiselect = false,
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            };
        }

        private void lb_browse_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var result = FileDialog.ShowDialog();
            if(result != DialogResult.OK) return;

            tx_path.Text = FileDialog.FileName;
        }

        private void bt_load_Click(object sender, EventArgs e)
        {
            if (Functions.IsFileLocked(new FileInfo(tx_path.Text)))
            {
                MessageBox.Show(@"Le fichier specifiée estt utilisé par une application. Fermez-le puis réessayez.", @"Erreur",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //DialogResult = Controller.LoadGpsPoints(tx_path.Text, tx_ref.Text)
            //    ? DialogResult.OK
            //    : DialogResult.Cancel;
        }

        private void bt_cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
