﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Autodesk.AutoCAD.DatabaseServices;
using Engine.Application;
using Engine.Data;
using Engine.Data.View;
using Engine.Processing;
using Engine.Properties;
using SQLite;
using App = Autodesk.AutoCAD.ApplicationServices.Application;
// ReSharper disable PossibleMultipleEnumeration

namespace Engine.Forms
{
    public partial class GeneratePvForm : Form
    {
        public ProjectInfo ProjectInfo { get; set; }
        public SQLiteConnection SqLiteConnection { get; set; }
        public Database Database { get; set; }
        public GeneratePvForm()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedSingle;

            Database = HostApplicationServices.WorkingDatabase;
            ProjectInfo = ProjectInfo.LoadFrom(Database);
            SqLiteConnection = new SQLiteConnection(ProjectInfo.DbPath);
            
            rd_unique.Checked = true;

            Database = HostApplicationServices.WorkingDatabase;
            ProjectInfo = ProjectInfo.LoadFrom(Database);
        }

        private void BtCancelClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void BtOkClick(object sender, EventArgs e)
        {
            Settings.Default.Save();

            var properties = GetProperties();
            if (properties.Any() && properties.First() == null)
            {
                MessageBox.Show(@"Identifiant propriété invalide.");
                return;
            }

            Processor.ProcessPVs(properties.ToList(), ProjectInfo, Database, SqLiteConnection);

            DialogResult = DialogResult.OK;
        }

        private IEnumerable<Property> GetProperties()
        {
            if (rd_unique.Checked)
                return new[] { Controller.GetProperty(tx_unique.Text, SqLiteConnection) };

            if (rd_multiple.Checked)
                return Controller.GetProperties(tx_multiple.Text.Split(";,_ ".ToCharArray(),
                    StringSplitOptions.RemoveEmptyEntries),
                    SqLiteConnection);

            if (rd_ss.Checked)
                return Controller.GetAllPropertiesFromSubSector(SqLiteConnection);

            return Controller.GetProperties(tx_from.Text, tx_to.Text, SqLiteConnection);
        }
    }
}
