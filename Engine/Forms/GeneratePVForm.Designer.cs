﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Engine.Forms
{
    partial class GeneratePvForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_cancel = new System.Windows.Forms.Button();
            this.bt_ok = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tx_from = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tx_to = new System.Windows.Forms.TextBox();
            this.tx_multiple = new System.Windows.Forms.RichTextBox();
            this.tx_unique = new System.Windows.Forms.TextBox();
            this.re_range = new System.Windows.Forms.RadioButton();
            this.rd_ss = new System.Windows.Forms.RadioButton();
            this.rd_multiple = new System.Windows.Forms.RadioButton();
            this.rd_unique = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bt_cancel
            // 
            this.bt_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bt_cancel.Location = new System.Drawing.Point(272, 299);
            this.bt_cancel.Margin = new System.Windows.Forms.Padding(2);
            this.bt_cancel.Name = "bt_cancel";
            this.bt_cancel.Size = new System.Drawing.Size(56, 31);
            this.bt_cancel.TabIndex = 9;
            this.bt_cancel.Text = "Annuler";
            this.bt_cancel.UseVisualStyleBackColor = true;
            this.bt_cancel.Click += new System.EventHandler(this.BtCancelClick);
            // 
            // bt_ok
            // 
            this.bt_ok.Location = new System.Drawing.Point(194, 299);
            this.bt_ok.Margin = new System.Windows.Forms.Padding(2);
            this.bt_ok.Name = "bt_ok";
            this.bt_ok.Size = new System.Drawing.Size(56, 31);
            this.bt_ok.TabIndex = 8;
            this.bt_ok.Text = "Générer";
            this.bt_ok.UseVisualStyleBackColor = true;
            this.bt_ok.Click += new System.EventHandler(this.BtOkClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tx_from);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tx_to);
            this.groupBox1.Controls.Add(this.tx_multiple);
            this.groupBox1.Controls.Add(this.tx_unique);
            this.groupBox1.Controls.Add(this.re_range);
            this.groupBox1.Controls.Add(this.rd_ss);
            this.groupBox1.Controls.Add(this.rd_multiple);
            this.groupBox1.Controls.Add(this.rd_unique);
            this.groupBox1.Location = new System.Drawing.Point(9, 26);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(319, 254);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Propriétées à traiter";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 86);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(240, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "(Utilisez l\'un des séparateurs : \" ; , . - _ \'espace\' \")";
            // 
            // tx_from
            // 
            this.tx_from.Location = new System.Drawing.Point(46, 221);
            this.tx_from.Margin = new System.Windows.Forms.Padding(2);
            this.tx_from.Name = "tx_from";
            this.tx_from.Size = new System.Drawing.Size(99, 20);
            this.tx_from.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(170, 223);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "à :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 223);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "de :";
            // 
            // tx_to
            // 
            this.tx_to.Location = new System.Drawing.Point(192, 221);
            this.tx_to.Margin = new System.Windows.Forms.Padding(2);
            this.tx_to.Name = "tx_to";
            this.tx_to.Size = new System.Drawing.Size(113, 20);
            this.tx_to.TabIndex = 7;
            // 
            // tx_multiple
            // 
            this.tx_multiple.Location = new System.Drawing.Point(19, 106);
            this.tx_multiple.Margin = new System.Windows.Forms.Padding(2);
            this.tx_multiple.Name = "tx_multiple";
            this.tx_multiple.Size = new System.Drawing.Size(283, 66);
            this.tx_multiple.TabIndex = 3;
            this.tx_multiple.Text = "";
            // 
            // tx_unique
            // 
            this.tx_unique.Location = new System.Drawing.Point(192, 28);
            this.tx_unique.Margin = new System.Windows.Forms.Padding(2);
            this.tx_unique.Name = "tx_unique";
            this.tx_unique.Size = new System.Drawing.Size(110, 20);
            this.tx_unique.TabIndex = 1;
            // 
            // re_range
            // 
            this.re_range.AutoSize = true;
            this.re_range.Location = new System.Drawing.Point(20, 199);
            this.re_range.Margin = new System.Windows.Forms.Padding(2);
            this.re_range.Name = "re_range";
            this.re_range.Size = new System.Drawing.Size(104, 17);
            this.re_range.TabIndex = 5;
            this.re_range.TabStop = true;
            this.re_range.Text = "Tous l\'intervalle :";
            this.re_range.UseVisualStyleBackColor = true;
            // 
            // rd_ss
            // 
            this.rd_ss.AutoSize = true;
            this.rd_ss.Location = new System.Drawing.Point(19, 177);
            this.rd_ss.Margin = new System.Windows.Forms.Padding(2);
            this.rd_ss.Name = "rd_ss";
            this.rd_ss.Size = new System.Drawing.Size(133, 17);
            this.rd_ss.TabIndex = 4;
            this.rd_ss.TabStop = true;
            this.rd_ss.Text = "Tous le Sous Secteur :";
            this.rd_ss.UseVisualStyleBackColor = true;
            // 
            // rd_multiple
            // 
            this.rd_multiple.AutoSize = true;
            this.rd_multiple.Location = new System.Drawing.Point(19, 55);
            this.rd_multiple.Margin = new System.Windows.Forms.Padding(2);
            this.rd_multiple.Name = "rd_multiple";
            this.rd_multiple.Size = new System.Drawing.Size(125, 17);
            this.rd_multiple.TabIndex = 2;
            this.rd_multiple.TabStop = true;
            this.rd_multiple.Text = "Plusieurs propriétés : ";
            this.rd_multiple.UseVisualStyleBackColor = true;
            // 
            // rd_unique
            // 
            this.rd_unique.AutoSize = true;
            this.rd_unique.Location = new System.Drawing.Point(19, 28);
            this.rd_unique.Margin = new System.Windows.Forms.Padding(2);
            this.rd_unique.Name = "rd_unique";
            this.rd_unique.Size = new System.Drawing.Size(114, 17);
            this.rd_unique.TabIndex = 0;
            this.rd_unique.TabStop = true;
            this.rd_unique.Text = "Propriétée unique :";
            this.rd_unique.UseVisualStyleBackColor = true;
            // 
            // GeneratePvForm
            // 
            this.AcceptButton = this.bt_ok;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bt_cancel;
            this.ClientSize = new System.Drawing.Size(340, 341);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bt_ok);
            this.Controls.Add(this.bt_cancel);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GeneratePvForm";
            this.Text = "Génération des Procés Verbaux";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Button bt_cancel;
        private Button bt_ok;
        private GroupBox groupBox1;
        private TextBox tx_from;
        private Label label2;
        private Label label1;
        private TextBox tx_to;
        private RichTextBox tx_multiple;
        private TextBox tx_unique;
        private RadioButton re_range;
        private RadioButton rd_ss;
        private RadioButton rd_multiple;
        private RadioButton rd_unique;
        private Label label10;
    }
}