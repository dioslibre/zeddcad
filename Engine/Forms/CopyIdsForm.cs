﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Engine.Data;
using Engine.Properties;

namespace Engine.Forms
{
    using System.IO;

    using Engine.Utils;

    public partial class CopyIdsForm : Form
    {
        public OpenFileDialog FileDialog { get; set; }
        
        public CopyIdsForm()
        {
            InitializeComponent();
            FileDialog = new OpenFileDialog
            {
                Multiselect = false,
                Filter = @"Document Excel | *.xlsx",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                CheckFileExists = true
            };

            gold_id.DataBindings.Add("Value", Settings.Default, "XlsxGoldId", false, DataSourceUpdateMode.OnPropertyChanged);
            num_id.DataBindings.Add("Value", Settings.Default, "XlsxId", false, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void bt_ok_Click(object sender, EventArgs e)
        {
            Settings.Default.Save();

            var columns = new Dictionary<XlsxColumn, int>
            {
                {XlsxColumn.PropertyId, (int) num_id.Value},
                {XlsxColumn.GoldenId, (int) gold_id.Value},
            };

            if (Functions.IsFileLocked(new FileInfo(tx_file.Text)))
            {
                MessageBox.Show(@"Le fichier specifié est ouvert dans une autre application. Fermez-le puis réessayez.", @"Erreur", 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (Controller.CopyIds(tx_file.Text, columns))
                DialogResult = DialogResult.OK;
        }

        private void lk_browse_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var result = FileDialog.ShowDialog(this);
            if (result == DialogResult.OK)
                tx_file.Text = FileDialog.FileName;
        }
    }
}
