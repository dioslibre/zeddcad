﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Autodesk.AutoCAD.DatabaseServices;
using Engine.Application;
using Engine.Data;
using Engine.Data.View;
using Engine.Processing;
using Engine.Properties;
using SQLite;
using App = Autodesk.AutoCAD.ApplicationServices.Application;
// ReSharper disable PossibleMultipleEnumeration

namespace Engine.Forms
{
    public partial class GenerateCalqueForm : Form
    {
        public ProjectInfo ProjectInfo { get; set; }
        public SQLiteConnection SqLiteConnection { get; set; }
        public Database Database { get; set; }
        public GenerateCalqueForm()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedSingle;

            Database = HostApplicationServices.WorkingDatabase;
            ProjectInfo = ProjectInfo.LoadFrom(Database);
            SqLiteConnection = new SQLiteConnection(ProjectInfo.DbPath);
            
            rd_unique.Checked = true;

            Database = HostApplicationServices.WorkingDatabase;
            ProjectInfo = ProjectInfo.LoadFrom(Database);

            cb_st.Items.AddRange(Settings.Default.CqFormatValues.Cast<object>().ToArray());
            cb_st.DataBindings.Add("SelectedIndex", Settings.Default, "CqFixedFormat",
                false, DataSourceUpdateMode.OnPropertyChanged);

            cb_st_max.Items.AddRange(Settings.Default.CqFormatValues.Cast<object>().ToArray());
            cb_st_max.DataBindings.Add("SelectedIndex", Settings.Default, "MaxST",
                false, DataSourceUpdateMode.OnPropertyChanged);

            cb_mode.DataBindings.Add("SelectedIndex", Settings.Default, "CqFormatMode",
                false, DataSourceUpdateMode.OnPropertyChanged);

            tx_echmin.DataBindings.Add("Text", Settings.Default, "CqMinScaleNumber",
                false, DataSourceUpdateMode.OnPropertyChanged);

            tx_echfix.DataBindings.Add("Text", Settings.Default, "CqFixedScaleNumber",
                false, DataSourceUpdateMode.OnPropertyChanged);

            num_plans.DataBindings.Add("Value", Settings.Default, "CqNumberOfPlans",
                false, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void BtCancelClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void BtOkClick(object sender, EventArgs e)
        {
            Settings.Default.Save();

            var properties = GetProperties();
            if (properties.Any() && properties.First() == null)
            {
                MessageBox.Show(@"Identifiant propriété invalide.");
                return;
            }

            Processor.ProcessPlanIndividuels(properties.ToList(), ProjectInfo, Database, SqLiteConnection);

            DialogResult = DialogResult.OK;
        }

        private IEnumerable<Property> GetProperties()
        {
            if (rd_unique.Checked)
                return new[] { Controller.GetProperty(tx_unique.Text, SqLiteConnection) };

            if (rd_multiple.Checked)
                return Controller.GetProperties(tx_multiple.Text.Split(";,_ ".ToCharArray(),
                    StringSplitOptions.RemoveEmptyEntries),
                    SqLiteConnection);

            if (rd_ss.Checked)
                return Controller.GetAllPropertiesFromSubSector(SqLiteConnection);

            return Controller.GetProperties(tx_from.Text, tx_to.Text, SqLiteConnection);
        }
    }
}
