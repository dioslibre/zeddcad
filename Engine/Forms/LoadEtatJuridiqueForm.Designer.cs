﻿namespace Engine.Forms
{
    partial class LoadEtatJuridiqueForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.num_indice = new System.Windows.Forms.NumericUpDown();
            this.num_req = new System.Windows.Forms.NumericUpDown();
            this.num_id = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tx_file = new System.Windows.Forms.TextBox();
            this.lk_browse = new System.Windows.Forms.LinkLabel();
            this.bt_cancel = new System.Windows.Forms.Button();
            this.bt_ok = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_indice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_req)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_id)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.num_indice);
            this.groupBox1.Controls.Add(this.num_req);
            this.groupBox1.Controls.Add(this.num_id);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(9, 76);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(274, 114);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Colonnes";
            // 
            // num_indice
            // 
            this.num_indice.Location = new System.Drawing.Point(190, 82);
            this.num_indice.Margin = new System.Windows.Forms.Padding(2);
            this.num_indice.Name = "num_indice";
            this.num_indice.Size = new System.Drawing.Size(66, 20);
            this.num_indice.TabIndex = 3;
            // 
            // num_req
            // 
            this.num_req.Location = new System.Drawing.Point(190, 53);
            this.num_req.Margin = new System.Windows.Forms.Padding(2);
            this.num_req.Name = "num_req";
            this.num_req.Size = new System.Drawing.Size(66, 20);
            this.num_req.TabIndex = 2;
            // 
            // num_id
            // 
            this.num_id.Location = new System.Drawing.Point(190, 26);
            this.num_id.Margin = new System.Windows.Forms.Padding(2);
            this.num_id.Name = "num_id";
            this.num_id.Size = new System.Drawing.Size(66, 20);
            this.num_id.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 81);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Indice :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 54);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Numéro de la réquisition :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Numéro de la propriété :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Fichier XLSX : ";
            // 
            // tx_file
            // 
            this.tx_file.Location = new System.Drawing.Point(9, 43);
            this.tx_file.Margin = new System.Windows.Forms.Padding(2);
            this.tx_file.Name = "tx_file";
            this.tx_file.Size = new System.Drawing.Size(276, 20);
            this.tx_file.TabIndex = 0;
            // 
            // lk_browse
            // 
            this.lk_browse.AutoSize = true;
            this.lk_browse.Location = new System.Drawing.Point(228, 24);
            this.lk_browse.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lk_browse.Name = "lk_browse";
            this.lk_browse.Size = new System.Drawing.Size(58, 13);
            this.lk_browse.TabIndex = 6;
            this.lk_browse.TabStop = true;
            this.lk_browse.Text = "Parcourir...";
            this.lk_browse.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lk_browse_LinkClicked);
            // 
            // bt_cancel
            // 
            this.bt_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bt_cancel.Location = new System.Drawing.Point(229, 205);
            this.bt_cancel.Margin = new System.Windows.Forms.Padding(2);
            this.bt_cancel.Name = "bt_cancel";
            this.bt_cancel.Size = new System.Drawing.Size(56, 32);
            this.bt_cancel.TabIndex = 5;
            this.bt_cancel.Text = "Annuler";
            this.bt_cancel.UseVisualStyleBackColor = true;
            // 
            // bt_ok
            // 
            this.bt_ok.Location = new System.Drawing.Point(160, 205);
            this.bt_ok.Margin = new System.Windows.Forms.Padding(2);
            this.bt_ok.Name = "bt_ok";
            this.bt_ok.Size = new System.Drawing.Size(56, 32);
            this.bt_ok.TabIndex = 4;
            this.bt_ok.Text = "Charger";
            this.bt_ok.UseVisualStyleBackColor = true;
            this.bt_ok.Click += new System.EventHandler(this.bt_ok_Click);
            // 
            // LoadEtatJuridiqueForm
            // 
            this.AcceptButton = this.bt_ok;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bt_cancel;
            this.ClientSize = new System.Drawing.Size(298, 245);
            this.Controls.Add(this.bt_ok);
            this.Controls.Add(this.bt_cancel);
            this.Controls.Add(this.lk_browse);
            this.Controls.Add(this.tx_file);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoadEtatJuridiqueForm";
            this.Text = "Charger l\'état Juridique ";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_indice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_req)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_id)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tx_file;
        private System.Windows.Forms.LinkLabel lk_browse;
        private System.Windows.Forms.NumericUpDown num_indice;
        private System.Windows.Forms.NumericUpDown num_req;
        private System.Windows.Forms.NumericUpDown num_id;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bt_cancel;
        private System.Windows.Forms.Button bt_ok;
    }
}