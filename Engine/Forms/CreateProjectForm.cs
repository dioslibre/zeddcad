﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Autodesk.AutoCAD.DatabaseServices;
using Engine.Application;
using Engine.Data.Model;
using Engine.Utils;
using SQLite;

namespace Engine.Forms
{
    public partial class CreateProjectForm : Form
    {
        public ProjectInfo ProjectInfo { get; set; }
        public OpenFileDialog FileDialog { get; set; }
        public FolderBrowserDialog BrowserDialog { get; set; }
        public string DwgPath { get; set; }

        public CreateProjectForm()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedSingle;

            ProjectInfo = new ProjectInfo();

            tx_secteur.DataBindings.Add("Text", ProjectInfo, "Secteur",
                false, DataSourceUpdateMode.OnPropertyChanged);
            tx_path.DataBindings.Add("Text", ProjectInfo, "Path",
                false, DataSourceUpdateMode.OnPropertyChanged);
            tx_prov_fr.DataBindings.Add("Text", ProjectInfo, "ProvinceFr",
                false, DataSourceUpdateMode.OnPropertyChanged);
            tx_prov_ar.DataBindings.Add("Text", ProjectInfo, "ProvinceAr",
                false, DataSourceUpdateMode.OnPropertyChanged);
            tx_cercle_fr.DataBindings.Add("Text", ProjectInfo, "CercleFr",
                false, DataSourceUpdateMode.OnPropertyChanged);
            tx_cercle_ar.DataBindings.Add("Text", ProjectInfo, "CercleAr",
                false, DataSourceUpdateMode.OnPropertyChanged);
            tx_com_fr.DataBindings.Add("Text", ProjectInfo, "CommuneFr",
                false, DataSourceUpdateMode.OnPropertyChanged);
            tx_com_ar.DataBindings.Add("Text", ProjectInfo, "CommuneAr",
                false, DataSourceUpdateMode.OnPropertyChanged);
            tx_date.DataBindings.Add("Text", ProjectInfo, "Date",
                false, DataSourceUpdateMode.OnPropertyChanged);
            tx_geom.DataBindings.Add("Text", ProjectInfo, "Geometre",
                false, DataSourceUpdateMode.OnPropertyChanged);
            tx_etab.DataBindings.Add("Text", ProjectInfo, "EtablitPar",
                false, DataSourceUpdateMode.OnPropertyChanged);
            tx_carte.DataBindings.Add("Text", ProjectInfo, "Carte",
                false, DataSourceUpdateMode.OnPropertyChanged);
            tx_cadastre.DataBindings.Add("Text", ProjectInfo, "Cadastre",
                false, DataSourceUpdateMode.OnPropertyChanged);

            FileDialog = new OpenFileDialog()
            {
                CheckFileExists = true,
                Multiselect = false,
                Filter = @"Document DWG | *.dwg",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            };

            BrowserDialog = new FolderBrowserDialog()
            {
                RootFolder = Environment.SpecialFolder.MyDocuments,
                ShowNewFolderButton = true
            };
        }

        private void bt_loadFromDWG_Click(object sender, EventArgs e)
        {
            var result = FileDialog.ShowDialog(this);
            if (result != DialogResult.OK) return;

            using (var db = new Database())
            {
                db.ReadDwgFile(FileDialog.FileName, FileOpenMode.OpenForReadAndAllShare, false, "");
                var info = ProjectInfo.LoadFrom(db);
                tx_secteur.Text = info.Secteur;
                tx_path.Text = info.Path;
                tx_prov_fr.Text = info.ProvinceFr;
                tx_prov_ar.Text = info.ProvinceAr;
                tx_com_fr.Text = info.CommuneFr;
                tx_com_ar.Text = info.CommuneAr;
                tx_cercle_fr.Text = info.CercleFr;
                tx_cercle_ar.Text = info.CercleAr;
                tx_date.Text = info.Date;
                tx_geom.Text = info.Geometre;
                tx_etab.Text = info.EtablitPar;
                tx_carte.Text = info.Carte;
                tx_cadastre.Text = info.Cadastre;
            }
        }

        private void bt_ok_Click(object sender, EventArgs e)
        {
            if (this.GetChildControls<TextBox>().Any(t => string.IsNullOrEmpty(t.Text)))
            {
                MessageBox.Show(this, @"Un ou plusieurs champs sont vides.", @"Erreur",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Functions.HasWriteAccessToFolder(ProjectInfo.Path))
            {
                MessageBox.Show(this, @"Vous n'avez pas accès a ce répertoire. Choisissez un autre.", @"Erreur",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            ProjectInfo.SubSector = (int)ss_id.Value;

            var absolutePath = Path.Combine(ProjectInfo.Path, ProjectInfo.Secteur);
            var directoryinfo = new DirectoryInfo(absolutePath);

            if (!Directory.Exists(absolutePath))
                directoryinfo.Create();

            var ssPath = Path.Combine(absolutePath, @"SS" + ss_id.Value);
            directoryinfo = new DirectoryInfo(ssPath);

            ProjectInfo.SSPath = ssPath;

            if (directoryinfo.Exists)
            {
                MessageBox.Show(this, @"Ce Sous Secteur existe dèja.", @"Erreur",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            directoryinfo.Create();
            directoryinfo.CreateSubdirectory(@"Base De Données");
            directoryinfo.CreateSubdirectory(@"Livrables");

            var livrablesPath = ProjectInfo.LivrablesPath;

            directoryinfo = new DirectoryInfo(livrablesPath);
            directoryinfo.CreateSubdirectory(@"Plans Individuels DWG");
            directoryinfo.CreateSubdirectory(@"Photos A4 DWG");
            directoryinfo.CreateSubdirectory(@"Photos A4 PDF");
            directoryinfo.CreateSubdirectory(@"Orthos PNG");
            directoryinfo.CreateSubdirectory(@"Procés Verbaux DOCX");

            DwgPath = Path.Combine(ssPath, string.Format("SS{0}.dwg", ss_id.Value));

            try
            {
                using (var db = new Database())
                {
                    var sqldbPath = ProjectInfo.DbPath;

                    Functions.AddLayersForPlanDeMasse(db);
                    db.CreateTextStyles();
                    Functions.AddSquareBlock(db);

                    ProjectInfo.Save(db);

                    var sqldb = new SQLiteConnection(sqldbPath);
                    sqldb.RunInTransaction(
                        () =>
                        {
                            sqldb.CreateTable<PropertyInfo>();
                            sqldb.CreateTable<ParcelInfo>();
                            sqldb.CreateTable<LinkInfo>();
                            sqldb.CreateTable<PointInfo>();
                        });
                    sqldb.Close();

                    db.SaveAs(DwgPath, DwgVersion.AC1800);
                }
                DialogResult = DialogResult.OK;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message + Environment.NewLine + exception.StackTrace);
            }
        }

        private void lb_browse_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var result = BrowserDialog.ShowDialog(this);
            if (result != DialogResult.OK) return;

            tx_path.Text = BrowserDialog.SelectedPath;
        }
    }
}
