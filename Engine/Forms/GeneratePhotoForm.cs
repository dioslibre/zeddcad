﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Autodesk.AutoCAD.DatabaseServices;
using Engine.Application;
using Engine.Data;
using Engine.Data.View;
using Engine.Processing;
using Engine.Properties;
using SQLite;
using App = Autodesk.AutoCAD.ApplicationServices.Application;

namespace Engine.Forms
{
    public partial class GeneratePhotoForm : Form
    {
        public ProjectInfo ProjectInfo { get; set; }
        public SQLiteConnection SqLiteConnection { get; set; }
        public Database Database { get; set; }
        public GeneratePhotoForm()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedSingle;

            Database = HostApplicationServices.WorkingDatabase;
            ProjectInfo = ProjectInfo.LoadFrom(Database);
            SqLiteConnection = new SQLiteConnection(ProjectInfo.DbPath);

            rd_unique.Checked = true;

            Database = HostApplicationServices.WorkingDatabase;
            ProjectInfo = ProjectInfo.LoadFrom(Database);

            cb_ech.SelectedIndex = 0;
            cb_ori.SelectedIndex = 0;

            num_plans.DataBindings.Add("Value", Settings.Default, "CqNumberOfPlans",
                false, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void BtCancelClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void BtOkClick(object sender, EventArgs e)
        {
            Settings.Default.Save();

            var properties = GetProperties().ToList();
            if (properties.Any() && properties.First() == null)
            {
                MessageBox.Show(@"Identifiant propriété invalide.");
                return;
            }

            Processor.ProcessPhotos(properties, ProjectInfo, Database, SqLiteConnection, cb_ech.SelectedItem.ToString(), cb_ori.SelectedItem.ToString());
            DialogResult = DialogResult.OK;
        }

        private IEnumerable<Property> GetProperties()
        {
            if (rd_unique.Checked)
                return new[] { Controller.GetProperty(tx_unique.Text, SqLiteConnection) };
            
            if (rd_multiple.Checked)
                return Controller.GetProperties(tx_multiple.Text.Split(";,_ -.".ToCharArray(),
                    StringSplitOptions.RemoveEmptyEntries),
                    SqLiteConnection);
            
            if (rd_ss.Checked)
                return Controller.GetAllPropertiesFromSubSector(SqLiteConnection);

            return Controller.GetProperties(tx_from.Text, tx_to.Text, SqLiteConnection);
        }
    }
}
