﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Engine.Forms
{
    partial class CreateProjectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tx_com_ar = new System.Windows.Forms.TextBox();
            this.tx_cercle_ar = new System.Windows.Forms.TextBox();
            this.tx_prov_ar = new System.Windows.Forms.TextBox();
            this.tx_com_fr = new System.Windows.Forms.TextBox();
            this.tx_cercle_fr = new System.Windows.Forms.TextBox();
            this.tx_prov_fr = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.bt_cancel = new System.Windows.Forms.Button();
            this.bt_ok = new System.Windows.Forms.Button();
            this.tx_secteur = new System.Windows.Forms.TextBox();
            this.tx_path = new System.Windows.Forms.TextBox();
            this.bt_loadFromDWG = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.ss_id = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tx_cadastre = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tx_carte = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tx_geom = new System.Windows.Forms.TextBox();
            this.tx_etab = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tx_date = new System.Windows.Forms.TextBox();
            this.lb_browse = new System.Windows.Forms.LinkLabel();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ss_id)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 58);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Secteur IFE";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 104);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Emplacement";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tx_com_ar);
            this.groupBox1.Controls.Add(this.tx_cercle_ar);
            this.groupBox1.Controls.Add(this.tx_prov_ar);
            this.groupBox1.Controls.Add(this.tx_com_fr);
            this.groupBox1.Controls.Add(this.tx_cercle_fr);
            this.groupBox1.Controls.Add(this.tx_prov_fr);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(16, 153);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(344, 108);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Situation";
            // 
            // tx_com_ar
            // 
            this.tx_com_ar.Location = new System.Drawing.Point(188, 78);
            this.tx_com_ar.Margin = new System.Windows.Forms.Padding(2);
            this.tx_com_ar.Name = "tx_com_ar";
            this.tx_com_ar.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tx_com_ar.Size = new System.Drawing.Size(102, 20);
            this.tx_com_ar.TabIndex = 9;
            // 
            // tx_cercle_ar
            // 
            this.tx_cercle_ar.Location = new System.Drawing.Point(188, 50);
            this.tx_cercle_ar.Margin = new System.Windows.Forms.Padding(2);
            this.tx_cercle_ar.Name = "tx_cercle_ar";
            this.tx_cercle_ar.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tx_cercle_ar.Size = new System.Drawing.Size(102, 20);
            this.tx_cercle_ar.TabIndex = 8;
            // 
            // tx_prov_ar
            // 
            this.tx_prov_ar.Location = new System.Drawing.Point(188, 22);
            this.tx_prov_ar.Margin = new System.Windows.Forms.Padding(2);
            this.tx_prov_ar.Name = "tx_prov_ar";
            this.tx_prov_ar.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tx_prov_ar.Size = new System.Drawing.Size(102, 20);
            this.tx_prov_ar.TabIndex = 7;
            // 
            // tx_com_fr
            // 
            this.tx_com_fr.Location = new System.Drawing.Point(68, 77);
            this.tx_com_fr.Margin = new System.Windows.Forms.Padding(2);
            this.tx_com_fr.Name = "tx_com_fr";
            this.tx_com_fr.Size = new System.Drawing.Size(102, 20);
            this.tx_com_fr.TabIndex = 6;
            // 
            // tx_cercle_fr
            // 
            this.tx_cercle_fr.Location = new System.Drawing.Point(68, 50);
            this.tx_cercle_fr.Margin = new System.Windows.Forms.Padding(2);
            this.tx_cercle_fr.Name = "tx_cercle_fr";
            this.tx_cercle_fr.Size = new System.Drawing.Size(102, 20);
            this.tx_cercle_fr.TabIndex = 5;
            // 
            // tx_prov_fr
            // 
            this.tx_prov_fr.Location = new System.Drawing.Point(68, 22);
            this.tx_prov_fr.Margin = new System.Windows.Forms.Padding(2);
            this.tx_prov_fr.Name = "tx_prov_fr";
            this.tx_prov_fr.Size = new System.Drawing.Size(102, 20);
            this.tx_prov_fr.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(301, 80);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "جماعة";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(303, 52);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "دائرة";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(306, 24);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "إقليم";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 80);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Commune";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 52);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Cercle";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 24);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Province";
            // 
            // bt_cancel
            // 
            this.bt_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bt_cancel.Location = new System.Drawing.Point(303, 449);
            this.bt_cancel.Margin = new System.Windows.Forms.Padding(2);
            this.bt_cancel.Name = "bt_cancel";
            this.bt_cancel.Size = new System.Drawing.Size(56, 23);
            this.bt_cancel.TabIndex = 16;
            this.bt_cancel.Text = "Annuler";
            this.bt_cancel.UseVisualStyleBackColor = true;
            // 
            // bt_ok
            // 
            this.bt_ok.Location = new System.Drawing.Point(242, 449);
            this.bt_ok.Margin = new System.Windows.Forms.Padding(2);
            this.bt_ok.Name = "bt_ok";
            this.bt_ok.Size = new System.Drawing.Size(56, 23);
            this.bt_ok.TabIndex = 15;
            this.bt_ok.Text = "Créer";
            this.bt_ok.UseVisualStyleBackColor = true;
            this.bt_ok.Click += new System.EventHandler(this.bt_ok_Click);
            // 
            // tx_secteur
            // 
            this.tx_secteur.Location = new System.Drawing.Point(16, 75);
            this.tx_secteur.Margin = new System.Windows.Forms.Padding(2);
            this.tx_secteur.Name = "tx_secteur";
            this.tx_secteur.Size = new System.Drawing.Size(228, 20);
            this.tx_secteur.TabIndex = 1;
            // 
            // tx_path
            // 
            this.tx_path.Location = new System.Drawing.Point(16, 120);
            this.tx_path.Margin = new System.Windows.Forms.Padding(2);
            this.tx_path.Name = "tx_path";
            this.tx_path.Size = new System.Drawing.Size(344, 20);
            this.tx_path.TabIndex = 3;
            // 
            // bt_loadFromDWG
            // 
            this.bt_loadFromDWG.Location = new System.Drawing.Point(17, 10);
            this.bt_loadFromDWG.Margin = new System.Windows.Forms.Padding(2);
            this.bt_loadFromDWG.Name = "bt_loadFromDWG";
            this.bt_loadFromDWG.Size = new System.Drawing.Size(342, 28);
            this.bt_loadFromDWG.TabIndex = 0;
            this.bt_loadFromDWG.Text = "Utiliser les Parmètres d\'un Projet Existant";
            this.bt_loadFromDWG.UseVisualStyleBackColor = true;
            this.bt_loadFromDWG.Click += new System.EventHandler(this.bt_loadFromDWG_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(270, 58);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(71, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "Sous Secteur";
            // 
            // ss_id
            // 
            this.ss_id.Location = new System.Drawing.Point(272, 76);
            this.ss_id.Margin = new System.Windows.Forms.Padding(2);
            this.ss_id.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.ss_id.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ss_id.Name = "ss_id";
            this.ss_id.Size = new System.Drawing.Size(87, 20);
            this.ss_id.TabIndex = 2;
            this.ss_id.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tx_cadastre);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.tx_carte);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.tx_geom);
            this.groupBox2.Controls.Add(this.tx_etab);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.tx_date);
            this.groupBox2.Location = new System.Drawing.Point(16, 273);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(344, 162);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Paramètres";
            // 
            // tx_cadastre
            // 
            this.tx_cadastre.Location = new System.Drawing.Point(8, 40);
            this.tx_cadastre.Margin = new System.Windows.Forms.Padding(2);
            this.tx_cadastre.Name = "tx_cadastre";
            this.tx_cadastre.Size = new System.Drawing.Size(148, 20);
            this.tx_cadastre.TabIndex = 10;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(5, 21);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "Cadastre";
            // 
            // tx_carte
            // 
            this.tx_carte.Location = new System.Drawing.Point(8, 128);
            this.tx_carte.Margin = new System.Windows.Forms.Padding(2);
            this.tx_carte.Name = "tx_carte";
            this.tx_carte.Size = new System.Drawing.Size(162, 20);
            this.tx_carte.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(5, 112);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(107, 13);
            this.label14.TabIndex = 15;
            this.label14.Text = "Carte Topographique";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(185, 67);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "Géomètre";
            // 
            // tx_geom
            // 
            this.tx_geom.Location = new System.Drawing.Point(188, 84);
            this.tx_geom.Margin = new System.Windows.Forms.Padding(2);
            this.tx_geom.Name = "tx_geom";
            this.tx_geom.Size = new System.Drawing.Size(148, 20);
            this.tx_geom.TabIndex = 13;
            // 
            // tx_etab
            // 
            this.tx_etab.Location = new System.Drawing.Point(8, 84);
            this.tx_etab.Margin = new System.Windows.Forms.Padding(2);
            this.tx_etab.Name = "tx_etab";
            this.tx_etab.Size = new System.Drawing.Size(162, 20);
            this.tx_etab.TabIndex = 12;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 67);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Etabli Par";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(185, 21);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Levé Le";
            // 
            // tx_date
            // 
            this.tx_date.Location = new System.Drawing.Point(188, 40);
            this.tx_date.Margin = new System.Windows.Forms.Padding(2);
            this.tx_date.Name = "tx_date";
            this.tx_date.Size = new System.Drawing.Size(146, 20);
            this.tx_date.TabIndex = 11;
            // 
            // lb_browse
            // 
            this.lb_browse.AutoSize = true;
            this.lb_browse.Location = new System.Drawing.Point(313, 103);
            this.lb_browse.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lb_browse.Name = "lb_browse";
            this.lb_browse.Size = new System.Drawing.Size(49, 13);
            this.lb_browse.TabIndex = 17;
            this.lb_browse.TabStop = true;
            this.lb_browse.Text = "Parcourir";
            this.lb_browse.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lb_browse_LinkClicked);
            // 
            // CreateProjectForm
            // 
            this.AcceptButton = this.bt_ok;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bt_cancel;
            this.ClientSize = new System.Drawing.Size(384, 482);
            this.Controls.Add(this.lb_browse);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.ss_id);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.bt_loadFromDWG);
            this.Controls.Add(this.tx_path);
            this.Controls.Add(this.tx_secteur);
            this.Controls.Add(this.bt_ok);
            this.Controls.Add(this.bt_cancel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateProjectForm";
            this.Text = "Créer un Nouveau Projet";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ss_id)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private Label label2;
        private GroupBox groupBox1;
        private TextBox tx_com_ar;
        private TextBox tx_cercle_ar;
        private TextBox tx_prov_ar;
        private TextBox tx_com_fr;
        private TextBox tx_cercle_fr;
        private TextBox tx_prov_fr;
        private Label label10;
        private Label label9;
        private Label label8;
        private Label label7;
        private Label label6;
        private Label label5;
        private Button bt_cancel;
        private Button bt_ok;
        private TextBox tx_secteur;
        private TextBox tx_path;
        private Button bt_loadFromDWG;
        private Label label13;
        private NumericUpDown ss_id;
        private GroupBox groupBox2;
        private TextBox tx_carte;
        private Label label14;
        private Label label12;
        private TextBox tx_geom;
        private TextBox tx_etab;
        private Label label11;
        private Label label3;
        private TextBox tx_date;
        private LinkLabel lb_browse;
        private TextBox tx_cadastre;
        private Label label15;
    }
}