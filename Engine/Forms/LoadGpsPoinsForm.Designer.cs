﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Engine.Forms
{
    partial class LoadGpsPoinsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tx_path = new System.Windows.Forms.TextBox();
            this.lb_browse = new System.Windows.Forms.LinkLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.tx_ref = new System.Windows.Forms.TextBox();
            this.bt_cancel = new System.Windows.Forms.Button();
            this.bt_load = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Rapport GPS";
            // 
            // tx_path
            // 
            this.tx_path.Location = new System.Drawing.Point(15, 45);
            this.tx_path.Name = "tx_path";
            this.tx_path.Size = new System.Drawing.Size(446, 22);
            this.tx_path.TabIndex = 1;
            // 
            // lb_browse
            // 
            this.lb_browse.AutoSize = true;
            this.lb_browse.Location = new System.Drawing.Point(395, 25);
            this.lb_browse.Name = "lb_browse";
            this.lb_browse.Size = new System.Drawing.Size(66, 17);
            this.lb_browse.TabIndex = 2;
            this.lb_browse.TabStop = true;
            this.lb_browse.Text = "Parcourir";
            this.lb_browse.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lb_browse_LinkClicked);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Référence";
            // 
            // tx_ref
            // 
            this.tx_ref.Location = new System.Drawing.Point(15, 100);
            this.tx_ref.Name = "tx_ref";
            this.tx_ref.Size = new System.Drawing.Size(446, 22);
            this.tx_ref.TabIndex = 4;
            // 
            // bt_cancel
            // 
            this.bt_cancel.Location = new System.Drawing.Point(376, 139);
            this.bt_cancel.Name = "bt_cancel";
            this.bt_cancel.Size = new System.Drawing.Size(85, 38);
            this.bt_cancel.TabIndex = 5;
            this.bt_cancel.Text = "Annuler";
            this.bt_cancel.UseVisualStyleBackColor = true;
            this.bt_cancel.Click += new System.EventHandler(this.bt_cancel_Click);
            // 
            // bt_load
            // 
            this.bt_load.Location = new System.Drawing.Point(265, 139);
            this.bt_load.Name = "bt_load";
            this.bt_load.Size = new System.Drawing.Size(85, 38);
            this.bt_load.TabIndex = 6;
            this.bt_load.Text = "Charger";
            this.bt_load.UseVisualStyleBackColor = true;
            this.bt_load.Click += new System.EventHandler(this.bt_load_Click);
            // 
            // LoadGpsPoinsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 189);
            this.Controls.Add(this.bt_load);
            this.Controls.Add(this.bt_cancel);
            this.Controls.Add(this.tx_ref);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lb_browse);
            this.Controls.Add(this.tx_path);
            this.Controls.Add(this.label1);
            this.Name = "LoadGpsPoinsForm";
            this.Text = "Charger un Rapport GPS";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private TextBox tx_path;
        private LinkLabel lb_browse;
        private Label label2;
        private TextBox tx_ref;
        private Button bt_cancel;
        private Button bt_load;
    }
}