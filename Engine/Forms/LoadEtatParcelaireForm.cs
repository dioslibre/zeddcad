﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Engine.Data;
using Engine.Properties;

namespace Engine.Forms
{
    using System.IO;

    using Engine.Utils;

    public partial class LoadEtatParcelaireForm : Form
    {
        public OpenFileDialog FileDialog { get; set; }
        public LoadEtatParcelaireForm()
        {
            InitializeComponent();
            FileDialog = new OpenFileDialog
            {
                Multiselect = false,
                Filter = @"Document Excel | *.xlsx",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                CheckFileExists = true
            };

            num_id.DataBindings.Add("Value", Settings.Default, "XlsxId", false, DataSourceUpdateMode.OnPropertyChanged);
            num_fr.DataBindings.Add("Value", Settings.Default, "XlsxFr", false, DataSourceUpdateMode.OnPropertyChanged);
            num_ar.DataBindings.Add("Value", Settings.Default, "XlsxAr", false, DataSourceUpdateMode.OnPropertyChanged);
            num_prop.DataBindings.Add("Value", Settings.Default, "XlsxProp", false, DataSourceUpdateMode.OnPropertyChanged);
            adr_ar.DataBindings.Add("Value", Settings.Default, "XlsxAdrAr", false, DataSourceUpdateMode.OnPropertyChanged);
            adr_fr.DataBindings.Add("Value", Settings.Default, "XlsxAdrFr", false, DataSourceUpdateMode.OnPropertyChanged);
            obs.DataBindings.Add("Value", Settings.Default, "XlsxObs", false, DataSourceUpdateMode.OnPropertyChanged);
            cst_num.DataBindings.Add("Value", Settings.Default, "XlsxCst", false, DataSourceUpdateMode.OnPropertyChanged);
            num_ctn.DataBindings.Add("Value", Settings.Default, "XlsxCtn", false, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void bt_ok_Click(object sender, EventArgs e)
        {
            Settings.Default.Save();

            var columns = new Dictionary<XlsxColumn, int>
            {
                {XlsxColumn.PropertyId, (int) num_id.Value},
                {XlsxColumn.NameFr, (int) num_fr.Value},
                {XlsxColumn.NameAr, (int) num_ar.Value},
                {XlsxColumn.OwnersFr, (int) num_prop.Value},
                {XlsxColumn.AdresseAr, (int) adr_ar.Value},
                {XlsxColumn.AdresseFr, (int) adr_fr.Value},
                {XlsxColumn.Consistance, (int) cst_num.Value},
                {XlsxColumn.Contenance, (int) num_ctn.Value},
                {XlsxColumn.Observations, (int) obs.Value}
            };

            if (Functions.IsFileLocked(new FileInfo(tx_file.Text)))
            {
                MessageBox.Show(@"Le fichier specifié est ouvert dans une autre application. Fermez-le puis réessayez.", @"Erreur",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (Controller.LoadEtatParcelaire(tx_file.Text, columns))
                DialogResult = DialogResult.OK;
        }

        private void lk_browse_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var result = FileDialog.ShowDialog(this);
            if (result == DialogResult.OK)
                tx_file.Text = FileDialog.FileName;
        }
    }
}
