﻿namespace Engine.Forms
{
    partial class LoadEtatParcelaireForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cst_num = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.obs = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.adr_fr = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.adr_ar = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.num_prop = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.num_ar = new System.Windows.Forms.NumericUpDown();
            this.num_fr = new System.Windows.Forms.NumericUpDown();
            this.num_id = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tx_file = new System.Windows.Forms.TextBox();
            this.lk_browse = new System.Windows.Forms.LinkLabel();
            this.bt_cancel = new System.Windows.Forms.Button();
            this.bt_ok = new System.Windows.Forms.Button();
            this.num_ctn = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cst_num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.obs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adr_fr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adr_ar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_prop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_ar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_fr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_id)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_ctn)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.num_ctn);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.cst_num);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.obs);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.adr_fr);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.adr_ar);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.num_prop);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.num_ar);
            this.groupBox1.Controls.Add(this.num_fr);
            this.groupBox1.Controls.Add(this.num_id);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(9, 76);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(274, 261);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Colonnes";
            // 
            // cst_num
            // 
            this.cst_num.Location = new System.Drawing.Point(190, 180);
            this.cst_num.Margin = new System.Windows.Forms.Padding(2);
            this.cst_num.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.cst_num.Name = "cst_num";
            this.cst_num.Size = new System.Drawing.Size(66, 20);
            this.cst_num.TabIndex = 7;
            this.cst_num.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 182);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Consistance : ";
            // 
            // obs
            // 
            this.obs.Location = new System.Drawing.Point(190, 229);
            this.obs.Margin = new System.Windows.Forms.Padding(2);
            this.obs.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.obs.Name = "obs";
            this.obs.Size = new System.Drawing.Size(66, 20);
            this.obs.TabIndex = 9;
            this.obs.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 231);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Observations : ";
            // 
            // adr_fr
            // 
            this.adr_fr.Location = new System.Drawing.Point(190, 156);
            this.adr_fr.Margin = new System.Windows.Forms.Padding(2);
            this.adr_fr.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.adr_fr.Name = "adr_fr";
            this.adr_fr.Size = new System.Drawing.Size(66, 20);
            this.adr_fr.TabIndex = 6;
            this.adr_fr.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 158);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Adresse (Fr) : ";
            // 
            // adr_ar
            // 
            this.adr_ar.Location = new System.Drawing.Point(190, 132);
            this.adr_ar.Margin = new System.Windows.Forms.Padding(2);
            this.adr_ar.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.adr_ar.Name = "adr_ar";
            this.adr_ar.Size = new System.Drawing.Size(66, 20);
            this.adr_ar.TabIndex = 5;
            this.adr_ar.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 131);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Adresse (Ar) : ";
            // 
            // num_prop
            // 
            this.num_prop.Location = new System.Drawing.Point(190, 106);
            this.num_prop.Margin = new System.Windows.Forms.Padding(2);
            this.num_prop.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num_prop.Name = "num_prop";
            this.num_prop.Size = new System.Drawing.Size(66, 20);
            this.num_prop.TabIndex = 4;
            this.num_prop.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 108);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Propriétaires (Fr) : ";
            // 
            // num_ar
            // 
            this.num_ar.Location = new System.Drawing.Point(190, 82);
            this.num_ar.Margin = new System.Windows.Forms.Padding(2);
            this.num_ar.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num_ar.Name = "num_ar";
            this.num_ar.Size = new System.Drawing.Size(66, 20);
            this.num_ar.TabIndex = 3;
            this.num_ar.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // num_fr
            // 
            this.num_fr.Location = new System.Drawing.Point(190, 53);
            this.num_fr.Margin = new System.Windows.Forms.Padding(2);
            this.num_fr.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num_fr.Name = "num_fr";
            this.num_fr.Size = new System.Drawing.Size(66, 20);
            this.num_fr.TabIndex = 2;
            this.num_fr.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // num_id
            // 
            this.num_id.Location = new System.Drawing.Point(190, 26);
            this.num_id.Margin = new System.Windows.Forms.Padding(2);
            this.num_id.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num_id.Name = "num_id";
            this.num_id.Size = new System.Drawing.Size(66, 20);
            this.num_id.TabIndex = 1;
            this.num_id.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 81);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Propriété dite (Ar) : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 54);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Propriété dite (Fr) :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Numéro de la propriété :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Fichier XLSX : ";
            // 
            // tx_file
            // 
            this.tx_file.Location = new System.Drawing.Point(9, 43);
            this.tx_file.Margin = new System.Windows.Forms.Padding(2);
            this.tx_file.Name = "tx_file";
            this.tx_file.Size = new System.Drawing.Size(276, 20);
            this.tx_file.TabIndex = 0;
            // 
            // lk_browse
            // 
            this.lk_browse.AutoSize = true;
            this.lk_browse.Location = new System.Drawing.Point(227, 26);
            this.lk_browse.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lk_browse.Name = "lk_browse";
            this.lk_browse.Size = new System.Drawing.Size(58, 13);
            this.lk_browse.TabIndex = 12;
            this.lk_browse.TabStop = true;
            this.lk_browse.Text = "Parcourir...";
            this.lk_browse.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lk_browse_LinkClicked);
            // 
            // bt_cancel
            // 
            this.bt_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bt_cancel.Location = new System.Drawing.Point(227, 341);
            this.bt_cancel.Margin = new System.Windows.Forms.Padding(2);
            this.bt_cancel.Name = "bt_cancel";
            this.bt_cancel.Size = new System.Drawing.Size(56, 32);
            this.bt_cancel.TabIndex = 11;
            this.bt_cancel.Text = "Annuler";
            this.bt_cancel.UseVisualStyleBackColor = true;
            // 
            // bt_ok
            // 
            this.bt_ok.Location = new System.Drawing.Point(157, 341);
            this.bt_ok.Margin = new System.Windows.Forms.Padding(2);
            this.bt_ok.Name = "bt_ok";
            this.bt_ok.Size = new System.Drawing.Size(56, 32);
            this.bt_ok.TabIndex = 10;
            this.bt_ok.Text = "Charger";
            this.bt_ok.UseVisualStyleBackColor = true;
            this.bt_ok.Click += new System.EventHandler(this.bt_ok_Click);
            // 
            // num_ctn
            // 
            this.num_ctn.Location = new System.Drawing.Point(190, 205);
            this.num_ctn.Margin = new System.Windows.Forms.Padding(2);
            this.num_ctn.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num_ctn.Name = "num_ctn";
            this.num_ctn.Size = new System.Drawing.Size(66, 20);
            this.num_ctn.TabIndex = 8;
            this.num_ctn.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 207);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Contenance : ";
            // 
            // LoadEtatParcelaireForm
            // 
            this.AcceptButton = this.bt_ok;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bt_cancel;
            this.ClientSize = new System.Drawing.Size(298, 384);
            this.Controls.Add(this.bt_ok);
            this.Controls.Add(this.bt_cancel);
            this.Controls.Add(this.lk_browse);
            this.Controls.Add(this.tx_file);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoadEtatParcelaireForm";
            this.Text = "Charger l\'état parcelaire ";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cst_num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.obs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adr_fr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adr_ar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_prop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_ar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_fr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_id)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_ctn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tx_file;
        private System.Windows.Forms.LinkLabel lk_browse;
        private System.Windows.Forms.NumericUpDown num_ar;
        private System.Windows.Forms.NumericUpDown num_fr;
        private System.Windows.Forms.NumericUpDown num_id;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bt_cancel;
        private System.Windows.Forms.Button bt_ok;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown num_prop;
        private System.Windows.Forms.NumericUpDown adr_fr;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown adr_ar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown obs;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown cst_num;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown num_ctn;
        private System.Windows.Forms.Label label10;
    }
}