﻿using System;
using System.Runtime.InteropServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.PlottingServices;

using App = Autodesk.AutoCAD.ApplicationServices.Application;

namespace Engine.Plotting
{
    using System.Collections.Generic;

    public class Plotter
    {
        [DllImport("acad.exe", CallingConvention = CallingConvention.Cdecl, EntryPoint = "acedTrans")]
        public static extern int AcEdTrans(double[] point, IntPtr fromRb, IntPtr toRb, int disp, double[] result);

        public static void PlotToPdf()
        {
            var document = App.DocumentManager.MdiActiveDocument;
            var ed = document.Editor;
            var db = document.Database;

            var filename = db.Filename;
            filename = filename.Contains("Photos A4")
                ? filename.Replace("DWG", "PDF").Replace(".dwg", ".pdf")
                : filename.Replace(".dwg", ".pdf");

            if (System.IO.File.Exists(filename))
                System.IO.File.Delete(filename);

            var rbFrom = new ResultBuffer(new TypedValue(5003, 1));
            var rbTo = new ResultBuffer(new TypedValue(5003, 2));

            var firres = new double[] { 0, 0, 0 };
            var secres = new double[] { 0, 0, 0 };

            var bgPlot = (short)App.GetSystemVariable("BACKGROUNDPLOT");

            App.SetSystemVariable("BACKGROUNDPLOT", 0);

            using (document.LockDocument())
            {
                var tr = db.TransactionManager.StartTransaction();
                using (tr)
                {
                    try
                    {
                        var bt = (BlockTable)tr.GetObject(db.BlockTableId, OpenMode.ForRead);

                        var btr = (BlockTableRecord)tr.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForRead);

                        var pi = new PlotInfo();
                        var piv = new PlotInfoValidator { MediaMatchingPolicy = MatchingPolicy.MatchEnabled };
                        pi.Layout = btr.LayoutId;

                        if (PlotFactory.ProcessPlotState == ProcessPlotState.NotPlotting)
                        {
                            var pe = PlotFactory.CreatePublishEngine();
                            using (pe)
                            {
                                var ppd = new PlotProgressDialog(false, 23, true);
                                using (ppd)
                                {
                                    var polygones = GetOuterRectangles(tr, btr);

                                    if (polygones.Count == 0)
                                    {
                                        App.SetSystemVariable("BACKGROUNDPLOT", bgPlot);
                                        System.Windows.Forms.MessageBox.Show("Rien à exporter.");
                                        return;
                                    }

                                    var numSheet = 1;

                                    foreach (var pl in polygones)
                                    {
                                        var lo = (Layout)tr.GetObject(btr.LayoutId, OpenMode.ForRead);

                                        double[] lower, upper;

                                        if (Math.Abs(pl.GetLineSegmentAt(0).Length - 420.0) < 1)
                                        {
                                            lower = pl.GetPoint3dAt(0).ToArray();
                                            upper = pl.GetPoint3dAt(2).ToArray();
                                        }
                                        else
                                        {
                                            lower = pl.GetPoint3dAt(1).ToArray();
                                            upper = pl.GetPoint3dAt(3).ToArray();
                                        }

                                        AcEdTrans(lower, rbFrom.UnmanagedObject, rbTo.UnmanagedObject, 0, firres);

                                        AcEdTrans(upper, rbFrom.UnmanagedObject, rbTo.UnmanagedObject, 0, secres);

                                        var window = new Extents2d(firres[0], firres[1], secres[0], secres[1]);

                                        var ps = new PlotSettings(lo.ModelType);
                                        ps.CopyFrom(lo);

                                        var psv = PlotSettingsValidator.Current;

                                        psv.SetPlotWindowArea(ps, window);

                                        psv.SetPlotType(ps, Autodesk.AutoCAD.DatabaseServices.PlotType.Window);
                                        psv.SetUseStandardScale(ps, false);
                                        psv.SetCustomPrintScale(ps, new CustomScale(1, 50.8));
                                        psv.SetPlotRotation(ps, PlotRotation.Degrees000);
                                        psv.SetPlotCentered(ps, true);

                                        psv.SetPlotConfigurationName(
                                                  ps,
                                                  "DWG To PDF.pc3",
                                                  "ISO_A4_(210.00_x_297.00_MM)");

                                        pi.OverrideSettings = ps;
                                        piv.MediaMatchingPolicy =
                                          MatchingPolicy.MatchDisabled;
                                        piv.Validate(pi);

                                        if (numSheet == 1)
                                        {
                                            ppd.set_PlotMsgString(
                                              PlotMessageIndex.DialogTitle,
                                              "Exportation des Photos vers PDF");
                                            ppd.set_PlotMsgString(
                                              PlotMessageIndex.CancelJobButtonMessage,
                                              "Annuler Tous");
                                            ppd.set_PlotMsgString(
                                              PlotMessageIndex.CancelSheetButtonMessage,
                                              "Annuler la feuille");
                                            ppd.set_PlotMsgString(
                                              PlotMessageIndex.SheetSetProgressCaption,
                                              "Avancement global de l'opération");
                                            ppd.set_PlotMsgString(
                                              PlotMessageIndex.SheetProgressCaption,
                                              "Avancement de la feuille");
                                            ppd.LowerPlotProgressRange = 0;
                                            ppd.UpperPlotProgressRange = 100;
                                            ppd.PlotProgressPos = 0;

                                            ppd.OnBeginPlot();
                                            ppd.IsVisible = true;
                                            pe.BeginPlot(ppd, null);

                                            pe.BeginDocument(pi, document.Name, null, 1, true, filename);
                                        }

                                        ppd.StatusMsgString = "Impression de " + document.Name.Substring(document.Name.LastIndexOf("\\", StringComparison.Ordinal) + 1) +
                                            " - ST " + numSheet +
                                          " de " + polygones.Count;

                                        ppd.OnBeginSheet();

                                        ppd.LowerSheetProgressRange = 0;
                                        ppd.UpperSheetProgressRange = 100;
                                        ppd.SheetProgressPos = 0;

                                        var ppi = new PlotPageInfo();
                                        pe.BeginPage(ppi, pi, numSheet == polygones.Count, null);
                                        pe.BeginGenerateGraphics(null);
                                        ppd.SheetProgressPos = 50;
                                        pe.EndGenerateGraphics(null);

                                        pe.EndPage(null);
                                        ppd.SheetProgressPos = 100;
                                        ppd.OnEndSheet();
                                        numSheet++;
                                    }

                                    pe.EndDocument(null);

                                    ppd.PlotProgressPos = 100;
                                    ppd.OnEndPlot();
                                    pe.EndPlot(null);
                                }
                            }
                        }
                        else
                        {
                            ed.WriteMessage("\nAnother plot is in progress.");

                            App.SetSystemVariable("BACKGROUNDPLOT", bgPlot);
                        }

                        App.SetSystemVariable("BACKGROUNDPLOT", bgPlot);
                    }
                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show(ex.Message + "\n" + ex.StackTrace);
                    }
                    tr.Commit();
                }
            }
        }

        private static List<Polyline> GetOuterRectangles(Transaction tr, BlockTableRecord btr)
        {
            var polygones = new List<Polyline>();
            foreach (var pId in btr)
            {
                if (pId.ObjectClass.DxfName != "LWPOLYLINE" && pId.ObjectClass.DxfName != "MTEXT") continue;
                if (pId.ObjectClass.DxfName == "LWPOLYLINE")
                {
                    var pl = (Polyline)tr.GetObject(pId, OpenMode.ForRead);
                    if (Math.Abs(pl.GetLineSegmentAt(0).Length - 420.0) < 1
                        || Math.Abs(pl.GetLineSegmentAt(1).Length - 420.0) < 1) polygones.Add(pl);
                }
                else
                {
                    var t = (MText)tr.GetObject(pId, OpenMode.ForWrite);
                    t.Contents = t.Contents.Replace("C7", "C255");
                }
            }

            return polygones;
        }
    }
}
