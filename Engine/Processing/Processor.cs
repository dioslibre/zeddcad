﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Engine.Application;
using Engine.Data.View;
using Engine.Documents;
using Engine.Properties;
using Engine.Utils;
using OpenXmlPowerTools;
using SQLite;

// ReSharper disable FunctionComplexityOverflow

namespace Engine.Processing
{
    using Autodesk.AutoCAD.Colors;
    using Novacode;

    public class Processor
    {
        public static void ProcessPlanIndividuels(List<Property> properties, ProjectInfo project,
            Database database, SQLiteConnection sqLiteConnection)
        {
            var document = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            var editor = document.Editor;

            var calques = new List<PlanIndividuel>();
            try
            {
                using (document.LockDocument())
                {
                    using (var transaction = database.TransactionManager.StartTransaction())
                    {
                        using (var lom = new LongOperationManager("Génération des calques individuels : ", 200))
                        {
                            lom.SetTotalOperations(properties.Count);
                            foreach (var property in properties)
                            {
                                if (!lom.Tick()) throw new Exception("\nOpération annulée.");
                                try
                                {
                                    var calque = new PlanIndividuel(property, project, sqLiteConnection, database, transaction);
                                    calques.Add(calque);
                                }
                                catch (Exception ex)
                                {
                                    var log = "\nImpossible de traiter la propriété " + property.PropertyId;
                                    log += property.Log();
                                    log += "\nErreur : " + ex;
                                    editor.WriteMessage(log);
                                }
                            }
                        }
                    }

                    var separates = Enumerable.Range(0, 7)
                        .Select(i => calques
                            .Where(c => c.Format.Name == CalqueFormatTypes.GetCalqueFormat(i).Name)
                            .OrderBy(c => c.Property.PropertyId.Trim('b'))
                            .ToArray())
                        .Where(cs => cs.Any())
                        .ToArray();

                    for (var i = 0; i < separates.Length; i++)
                    {
                        var sCalques = separates[i];
                        if (sCalques.Length == 0) continue;

                        var st = sCalques[0].Format;

                        var numberOfDbs = sCalques.Length % Settings.Default.CqNumberOfPlans == 0
                            ? sCalques.Length / Settings.Default.CqNumberOfPlans
                            : 1 + sCalques.Length / Settings.Default.CqNumberOfPlans;

                        for (var j = 0; j < numberOfDbs; j++)
                        {
                            var db = new Database();

                            Functions.CopyTextStyles(database, db, "CQ_");
                            Functions.AddSquareBlock(db);

                            var subCalques = Enumerable.Range(j * Settings.Default.CqNumberOfPlans,
                                Settings.Default.CqNumberOfPlans)
                                .Select(index => index < sCalques.Length
                                    ? sCalques[index]
                                    : null)
                                .TakeWhile(c => c != null)
                                .ToArray();

                            using (var transaction = db.TransactionManager.StartTransaction())
                            {
                                db.CreateCalqueLayers(transaction);
                                db.CopyLineTypes(database);

                                var stId = db.TextStyleTableId;
                                var styleTable = (TextStyleTable)transaction.GetObject(stId, OpenMode.ForRead);
                                var ltId = db.LayerTableId;
                                var layerTable = (LayerTable)transaction.GetObject(ltId, OpenMode.ForRead);
                                var btId = db.BlockTableId;
                                var blockTable = (BlockTable)transaction.GetObject(btId, OpenMode.ForWrite);
                                var msId = SymbolUtilityServices.GetBlockModelSpaceId(db);
                                var modelSpace = (BlockTableRecord)transaction.GetObject(msId, OpenMode.ForWrite);
                                var ltpId = db.LinetypeTableId;
                                var linetypeTable = (LinetypeTable)transaction.GetObject(ltpId, OpenMode.ForRead);
                                var ratId = db.RegAppTableId;
                                var regAppTable = (RegAppTable)transaction.GetObject(ratId, OpenMode.ForWrite);

                                using (var lom = new LongOperationManager("Enregistrement des calques individuels : ", 200))
                                {
                                    lom.SetTotalOperations(subCalques.Length * 2);
                                    Array.ForEach(subCalques, c =>
                                    {
                                        if (!lom.Tick())
                                        {
                                            throw new Exception("\nOpération annulée.");
                                        }
                                        c.SetBlocks(blockTable);
                                        c.SetTextStyles(styleTable);
                                        c.SetLayers(layerTable);
                                        c.ToBlockTableRecord(transaction, blockTable, modelSpace, linetypeTable, regAppTable, db);
                                        c.Dispose();
                                    });

                                    const double width = 0.5 * 2000;
                                    var height = (st.Height + 0.06) * 2000;
                                    var w = 0.0;
                                    var position = new Point3d(w, height * (2 + subCalques.Length / 4), 0.0);

                                    if (subCalques.Length == 1)
                                    {
                                        var c = subCalques[0];
                                        var propId = string.Copy(c.Property.PropertyId);
                                        SymbolUtilityServices.ValidateSymbolName(propId, false);
                                        var id = blockTable[propId];

                                        if (!lom.Tick())
                                        {
                                            editor.WriteMessage("\nOpération interrompue. Annulation ...");
                                            throw new Exception("\nOpération annulée.");
                                        }
                                        var block = new BlockReference(c.Grip, id)
                                        {
                                            ScaleFactors = new Scale3d(1)
                                        };
                                        modelSpace.AppendEntity(block);
                                        transaction.AddNewlyCreatedDBObject(block, true);

                                        var entities = new DBObjectCollection();
                                        block.Explode(entities);
                                        block.Erase();
                                        block.Dispose();

                                        foreach (DBObject entity in entities)
                                        {
                                            // ReSharper disable once PossibleInvalidCastException
                                            modelSpace.AppendEntity((Entity)entity);
                                            transaction.AddNewlyCreatedDBObject(entity, true);
                                            entity.Dispose();
                                        }
                                    }
                                    else
                                        Array.ForEach(subCalques, c =>
                                        {
                                            var propId = c.Property.PropertyId.Replace("/", "_");
                                            SymbolUtilityServices.ValidateSymbolName(propId, false);
                                            var id = blockTable[propId];

                                            if (!lom.Tick())
                                            {
                                                editor.WriteMessage("\nOpération interrompue. Annulation ...");
                                                throw new Exception("\nOpération annulée.");
                                            }
                                            var block = new BlockReference(position, id)
                                            {
                                                ScaleFactors = new Scale3d(2000.0 / c.ScaleNumber)
                                            };
                                            modelSpace.AppendEntity(block);
                                            transaction.AddNewlyCreatedDBObject(block, true);

                                            var entities = new DBObjectCollection();
                                            block.Explode(entities);
                                            block.Erase();
                                            block.Dispose();

                                            foreach (DBObject entity in entities)
                                            {
                                                // ReSharper disable once PossibleInvalidCastException
                                                modelSpace.AppendEntity((Entity)entity);
                                                transaction.AddNewlyCreatedDBObject(entity, true);
                                                entity.Dispose();
                                            }

                                            if (w < width)
                                            {
                                                w += (c.Format.Width + .06) * 2000;
                                                position = new Point3d(w, position.Y, 0.0);
                                            }
                                            else
                                            {
                                                w = 0.0;
                                                position = new Point3d(w, position.Y - height, 0.0);
                                            }
                                        });
                                }

                                db.ZoomExtents(transaction);
                                transaction.Commit();
                            }

                            var dwgName = subCalques.Length == 1
                                ? string.Format("{0}_{1}", st.Name, subCalques[0].Property.PropertyId.Replace("/", "_"))
                                : string.Format("{0}_{1}_{2}", st.Name, subCalques[0].Property.PropertyId.Replace("/", "_"),
                                    subCalques[subCalques.Length - 1].Property.PropertyId.Replace("/", "_"));

                            editor.WriteMessage("\nEnregistrement du fichier " + dwgName + ".");

                            var path = Path.Combine(project.LivrablesPath, "Plans Individuels DWG", dwgName + ".dwg");

                            db.SaveAs(path, DwgVersion.AC1800);
                            db.Dispose();
                        }
                    }
                    sqLiteConnection.Close();
                    editor.WriteMessage("\nOpération términée avec succées.");
                }
            }
            catch (Exception e)
            {
                calques.ForEach(c => c.Dispose());
                sqLiteConnection.Close();
                if (e.Message.Contains("annulée"))
                    editor.WriteMessage("\n" + e.Message);
                else
                {
                    editor.WriteMessage(Environment.NewLine + "Erreur : " + e);
                    editor.WriteMessage("\nOpération échouée. Voir ErrorLog.txt.");
                }
            }
        }

        public static void ProcessPhotos(List<Property> properties,ProjectInfo project, Database database, 
            SQLiteConnection sqLiteConnection, string scale, string orientation)
        {
            var document = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            var editor = document.Editor;

            var photos = new List<PhotoA4>();
            try
            {
                using (document.LockDocument())
                {
                    using (var tx = database.TransactionManager.StartTransaction())
                    {
                        IntPtr pointer;
                        RasterImage raster;
                        Functions.SetRaster(database, tx, out raster, out pointer);

                        using (var lom = new LongOperationManager("Génération des photos A4 : ", 100))
                        {
                            lom.SetTotalOperations(properties.Count);
                            foreach (var property in properties)
                            {
                                if (!lom.Tick()) throw new Exception("\nOpération annulée.");
                                try
                                {
                                    photos.Add(new PhotoA4(property, tx, project, sqLiteConnection, pointer, raster, scale, orientation));
                                }
                                catch (Exception ex)
                                {
                                    var log = "\nImpossible de traiter la propriété " + property.PropertyId;
                                    log += "\n" + property.Log();
                                    log += "\nErreur : " + ex;
                                    editor.WriteMessage(log);
                                }
                            }
                        }
                    }

                    var numberOfDbs = photos.Count % Settings.Default.PhNumberOfPhotos == 0
                        ? photos.Count / Settings.Default.PhNumberOfPhotos
                        : 1 + photos.Count / Settings.Default.PhNumberOfPhotos;

                    for (var j = 0; j < numberOfDbs; j++)
                    {
                        var db = new Database();

                        Functions.CopyTextStyles(database, db, "PH_");
                        db.CreatePhotoA4Layers();

                        var subPhotos = Enumerable.Range(j * Settings.Default.CqNumberOfPlans,
                            Settings.Default.CqNumberOfPlans)
                            .Select(index => index < photos.Count
                                ? photos[index]
                                : null)
                            .TakeWhile(c => c != null)
                            .ToArray();

                        Transaction transaction;
                        using (transaction = db.TransactionManager.StartTransaction())
                        {
                            var stId = db.TextStyleTableId;
                            var styleTable = (TextStyleTable)transaction.GetObject(stId, OpenMode.ForRead);
                            var ltId = db.LayerTableId;
                            var layerTable = (LayerTable)transaction.GetObject(ltId, OpenMode.ForRead);
                            var btId = db.BlockTableId;
                            var blockTable = (BlockTable)transaction.GetObject(btId, OpenMode.ForWrite);

                            var acImgDctId = RasterImageDef.GetImageDictionary(db);

                            if (acImgDctId.IsNull)
                                acImgDctId = RasterImageDef.CreateImageDictionary(db);

                            var acImgDict = (DBDictionary)transaction.GetObject(acImgDctId, OpenMode.ForRead);

                            using (var lom = new LongOperationManager("Enregistrement des Photos A4 : "))
                            {
                                lom.SetTotalOperations(subPhotos.Length * 2);
                                foreach (var photo in subPhotos)
                                {
                                    if (!lom.Tick())
                                    {
                                        throw new Exception("\nOpération annulée.");
                                    }
                                    photo.SetTextStyles(styleTable);
                                    photo.SetLayers(layerTable);
                                    photo.ToBlockTableRecord(transaction, blockTable);
                                    photo.Dispose();
                                }

                                var msId = SymbolUtilityServices.GetBlockModelSpaceId(db);
                                var modelSpace = (BlockTableRecord)transaction.GetObject(msId, OpenMode.ForWrite);

                                var onTopText = new List<Entity>();
                                var onTopPoly = new List<Entity>();

                                const double width = 0.5 * 2000;
                                const double height = .31 * 2000;
                                var w = 0.0;
                                var position = new Point3d(w, height * (2 + subPhotos.Length / 4), 0.0);

                                Array.ForEach(subPhotos, ph =>
                                {
                                    var propId = ph.Property.PropertyId.Replace("/", "_");
                                    SymbolUtilityServices.ValidateSymbolName(propId, false);
                                    var id = blockTable[propId];

                                    if (!lom.Tick())
                                    {
                                        editor.WriteMessage("\nOpération interrompue. Annulation ...");
                                        throw new Exception("\nOpération annulée.");
                                    }
                                    var filename = project.LivrablesPath
                                        + @"\Orthos PNG\" + ph.Property.PropertyId.Replace("/", "_") + ".png";

                                    if (File.Exists(filename))
                                        Functions.AttachRasterImage(filename, position, db, transaction, acImgDict, ph.IsVertical);

                                    var block = new BlockReference(position, id)
                                    {
                                        ScaleFactors = new Scale3d(2000.0 / ph.ScaleNumber)
                                    };
                                    modelSpace.AppendEntity(block);
                                    transaction.AddNewlyCreatedDBObject(block, true);

                                    var entities = new DBObjectCollection();
                                    block.Explode(entities);
                                    block.Erase();
                                    block.Dispose();

                                    foreach (DBObject entity in entities)
                                    {
                                        if (entity.GetRXClass().DxfName == "MTEXT")
                                        {
                                            onTopText.Add((Entity)entity);
                                            continue;
                                        }
                                        if (entity.GetRXClass().DxfName == "LWPOLYLINE"
                                            && ((Polyline)entity).Color == Color.FromColor(System.Drawing.Color.Red))
                                        {
                                            onTopPoly.Add((Entity)entity);
                                            continue;
                                        }

                                        // ReSharper disable once PossibleInvalidCastException
                                        modelSpace.AppendEntity((Entity)entity);
                                        transaction.AddNewlyCreatedDBObject(entity, true);
                                        entity.Dispose();
                                    }
                                    foreach (var entity in onTopPoly)
                                    {
                                        modelSpace.AppendEntity(entity);
                                        transaction.AddNewlyCreatedDBObject(entity, true);
                                        entity.Dispose();
                                    }
                                    onTopPoly.Clear();
                                    foreach (var entity in onTopText)
                                    {
                                        modelSpace.AppendEntity(entity);
                                        transaction.AddNewlyCreatedDBObject(entity, true);
                                        entity.Dispose();
                                    }
                                    onTopText.Clear();

                                    if (w < width)
                                    {
                                        w += 0.23 * 2000;
                                        position = new Point3d(w, position.Y, 0.0);
                                    }
                                    else
                                    {
                                        w = 0.0;
                                        position = new Point3d(w, position.Y - height, 0.0);
                                    }
                                });
                            }

                            db.ZoomExtents(transaction);
                            transaction.Commit();
                        }

                        var dwgName = subPhotos.Length == 1
                            ? string.Format("PhotoA4_{0}", subPhotos[0].Property.PropertyId.Replace("/", "_"))
                            : string.Format("PhotoA4_{0}_{1}", subPhotos[0].Property.PropertyId.Replace("/", "_"),
                                subPhotos[subPhotos.Length - 1].Property.PropertyId.Replace("/", "_"));

                        editor.WriteMessage("\nEnregistrement du fichier " + dwgName + ".");
                        var path = Path.Combine(project.LivrablesPath, "Photos A4 DWG", dwgName + ".dwg");

                        db.SaveAs(path, DwgVersion.AC1800);
                        db.Dispose();
                    }
                    sqLiteConnection.Close();
                    editor.WriteMessage("\nOpération términée avec succées.");
                }
            }
            catch (Exception e)
            {
                photos.ForEach(p => p.Dispose());
                sqLiteConnection.Close();
                if (e.Message.Contains("annulée"))
                    editor.WriteMessage("\n" + e.Message);
                else
                {
                    editor.WriteMessage(Environment.NewLine + "Erreur : " + e);
                    editor.WriteMessage("\nOpération échouée. Voir ErrorLog.txt.");
                }
            }
        }

        public static void ProcessPVs(List<Property> properties, ProjectInfo project, Database database, 
            SQLiteConnection sqLiteConnection)
        {
            var document = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            var editor = document.Editor;

            try
            {
                using (document.LockDocument())
                {
                    using (var lom = new LongOperationManager("Génération des Procés Verbaux : ", 200))
                    {
                        lom.SetTotalOperations(properties.Count);
                        foreach (var property in properties)
                        {
                            if (!lom.Tick()) throw new Exception("\nOpération annulée.");

                            var pages = new List<Source>();
                            try
                            {
                                var pv = new ProcesVerbal(property, sqLiteConnection);

                                // replace text in first page
                                using (var firstPage = new MemoryStream())
                                {
                                    firstPage.Write(Resources.PV_Template_P1, 0, Resources.PV_Template_P1.Length);
                                    using (var docx = DocX.Load(firstPage))
                                    {
                                        docx.ReplaceText("//CADASTRE//", project.Cadastre);
                                        docx.ReplaceText("//REQUISITION//", property.RequisitionId);
                                        docx.ReplaceText("//PDITE//", property.NameFr);
                                        docx.ReplaceText("//PROVINCE//", project.ProvinceFr);
                                        docx.ReplaceText("//CERCLE//", project.CercleFr);
                                        docx.ReplaceText("//COMMUNE//", project.CommuneFr);
                                        docx.ReplaceText("//DOUAR//", property.AddressFr);
                                        docx.ReplaceText("//SECTEUR//", project.Secteur);
                                        docx.ReplaceText("//SSID//", project.SubSector.ToString());
                                        docx.ReplaceText("//OWNERSFR//", property.OwnersFr == null ? "" : property.OwnersFr.Replace("\n", " ").Trim());

                                        docx.Save();
                                    }
                                    pages.Add(new Source(new WmlDocument("1", firstPage.ToArray()), true));
                                }

                                const int rowCount = 20;
                                var totalSecondPageCount = pv.BorneTables.Sum(t => pv.GetSecondPageCount(t.Key, rowCount));

                                var nl = Environment.NewLine;

                                // fill tables in the second page
                                foreach (var parcel in property.Parcels)
                                {
                                    var secondPageCount = pv.GetSecondPageCount(parcel, rowCount);

                                    var bEnumerator = pv.BorneTables[parcel].GetEnumerator();
                                    var rEnumerator = pv.RiverainsTables[parcel].GetEnumerator();

                                    for (var i = 0; i < secondPageCount; i++)
                                    {
                                        using (var secondPage = new MemoryStream())
                                        {
                                            secondPage.Write(Resources.PV_Template_P2, 0, Resources.PV_Template_P2.Length);
                                            using (var docx = DocX.Load(secondPage))
                                            {
                                                docx.ReplaceText("//NUMREQ//", property.RequisitionId);
                                                docx.ReplaceText("//PAGE//", (pages.Count) + "/" + totalSecondPageCount);
                                                docx.ReplaceText("//MAPPE//", Functions.CalculateAllMappes(new List<Parcel> { parcel }, 2000)[0]);

                                                docx.ReplaceText("//NP//", parcel.Part == 0
                                                    ? "Unique"
                                                    : property.Parcels.Count.ToString());

                                                docx.ReplaceText("//PN//", parcel.Part == 0
                                                    ? parcel.PropertyId
                                                    : parcel.PropertyId + "-P" + parcel.Part);

                                                var geometry = parcel.GetPolygon();
                                                var centroid = geometry.Centroid;
                                                if (!Functions.PointInPolygon(geometry.Coordinates, centroid))
                                                {
                                                    geometry = Functions.GetBiggestConvexPolygon(geometry.Coordinates.ToList());
                                                    centroid = geometry.Centroid;
                                                }

                                                docx.ReplaceText("//X//", string.Format("{0:0.00}", centroid.X));
                                                docx.ReplaceText("//SS//", project.SubSector.ToString());
                                                docx.ReplaceText("//Y//", string.Format("{0:0.00}", centroid.Y));
                                                var secondTable = docx.Tables[1];
                                                var rowIndex = 1;
                                                var finished = false;
                                                while (rowIndex <= rowCount)
                                                {
                                                    if (rowIndex % 2 == 1)
                                                    {
                                                        if (!bEnumerator.MoveNext())
                                                        {
                                                            finished = true;
                                                            break;
                                                        }

                                                        var bEntry = bEnumerator.Current;
                                                        var descriptions = bEntry.Description.Distinct().ToList();

                                                        var description = "Plantée ";
                                                        if (descriptions.Contains("en Terrain Nu "))
                                                        {
                                                            description += "en Terrain Nu ";
                                                            descriptions.Remove("en Terrain Nu ");
                                                        }

                                                        descriptions.ForEach(d => description += "et " + d);
                                                        description = description.Trim() + ".";
                                                        description = description.Replace("Plantée et", "Plantée");

                                                        var row = secondTable.Rows[rowIndex];

                                                        row.Cells[1].Paragraphs.First().Append(bEntry.Borne.Name).Bold();
                                                        row.Cells[2].Paragraphs.First().Append(description).Bold();

                                                        rowIndex++;
                                                    }
                                                    else
                                                    {
                                                        if (!rEnumerator.MoveNext())
                                                        {
                                                            finished = true;
                                                            break;
                                                        }

                                                        var rEntry = rEnumerator.Current;

                                                        var row = secondTable.Rows[rowIndex];

                                                        rEntry.Riverain = string.IsNullOrEmpty(rEntry.Riverain)
                                                            ? "Inconnu"
                                                            : rEntry.Riverain;

                                                        rEntry.Description = string.IsNullOrEmpty(rEntry.Description)
                                                            ? "Limite Rectiligne en Terrain Nu."
                                                            : rEntry.Description;

                                                        row.Cells[0].Paragraphs.First().Append(rEntry.Riverain);
                                                        row.Cells[2].Paragraphs.First().Append(rEntry.Description);

                                                        rowIndex++;
                                                    }
                                                }

                                                if (finished)
                                                {
                                                    var row = secondTable.Rows[rowIndex];
                                                    row.Cells[1].Paragraphs.First().Append(parcel.Points[0].Name).Bold();
                                                    row.Cells[2].Paragraphs.First().Append("Point de départ").Bold();
                                                }

                                                for (var j = 1; j < secondTable.RowCount; j++)
                                                {
                                                    var r = secondTable.Rows[j];
                                                    if (r.Cells[2].Paragraphs.All(p => string.IsNullOrEmpty(p.Text)))
                                                    {
                                                        r.Remove();
                                                        j--;
                                                    }
                                                }
                                                docx.Save();
                                            }
                                            pages.Add(new Source(new WmlDocument("2", secondPage.ToArray()), true));
                                        }
                                    }
                                }

                                // replace CONSISTANCE AND area in third page
                                using (var thirdPage = new MemoryStream())
                                {
                                    thirdPage.Write(Resources.PV_Template_P3, 0, Resources.PV_Template_P3.Length);
                                    using (var docx = DocX.Load(thirdPage))
                                    {
                                        var consistance = string.Empty;
                                        var superficie = string.Empty;
                                        var charges = string.Empty;

                                        property.Parcels.ForEach(p =>
                                        {
                                            var area = p.Contenance / 10000.0;
                                            var ha = (int)Math.Floor(area);
                                            var a = (int)Math.Floor((area - ha) * 100);
                                            var ca = (int)Math.Round(((area - ha) * 100 - a) * 100);

                                            superficie += string
                                                .Format("Parcelle n° : {0}     Contenance :  {1:D2} Ha   {2:D2} A    {3:D2} Ca",
                                                    p.Part == 0
                                                    ? p.PropertyId
                                                    : p.PropertyId + "-P" + p.Part, ha, a, ca) + nl;

                                            consistance += String.Format("P{0} : {1}{2}", p.Part, p.Consistance ?? "T.C", Environment.NewLine);
                                            charges += String.Format("P{0} : {1}{2}", p.Part, "Néant", Environment.NewLine);
                                        });
                                        consistance = consistance.Replace("P0 : ", "");
                                        charges = charges.Replace("P0 : ", "");

                                        docx.ReplaceText("//CHARGES//", charges);
                                        docx.ReplaceText("//AREA//", superficie);
                                        docx.ReplaceText("//CONSISTANCE//", consistance);
                                        docx.Save();
                                    }
                                    pages.Add(new Source(new WmlDocument("3", thirdPage.ToArray()), true));
                                }

                                // fill opposition table in last page
                                if (!string.IsNullOrEmpty(property.Observations) && (property.Observations.Contains("OPP") || property.Observations.Contains("REV")))
                                {
                                    using (var lastPage = new MemoryStream())
                                    {
                                        lastPage.Write(Resources.PV_Template_P4, 0, Resources.PV_Template_P4.Length);
                                        using (var docx = DocX.Load(lastPage))
                                        {
                                            try
                                            {
                                                var lines = property.Observations.Split("\n;".ToCharArray(),
                                                    StringSplitOptions.RemoveEmptyEntries);
                                                for (var i = 0; i < lines.Length; i++)
                                                {
                                                    var line = lines[i];
                                                    var oppIndex = property.Observations.IndexOf("OPP",
                                                        StringComparison.InvariantCulture);

                                                    string type = "";
                                                    if (line.Contains("OPP"))
                                                    {
                                                        var opp = property.Observations.Substring(oppIndex);
                                                        type = "Opposition ";
                                                        if (opp.Contains("TOTAL"))
                                                            type += "totale";
                                                        else if (opp.Contains("OFFICE"))
                                                            type += "d'office";
                                                        else if (opp.Contains("PARTIEL"))
                                                            type += "partielle";
                                                        else
                                                            type += "totale";
                                                    }

                                                    else if (line.Contains("REV"))
                                                        type = "revendication de droits" + Environment.NewLine +
                                                               "indivus";

                                                    var nameIndex =
                                                        line.LastIndexOf("DE M", StringComparison.InvariantCulture);

                                                    var name = line.Substring(nameIndex + 3)
                                                        .Split(" ,".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                                                        .Where(s => s.All(char.IsLetter))
                                                        .ToArray();

                                                    var row = docx.Tables[0].Rows[i + 1];
                                                    row.Cells[0].Paragraphs.First().Append(string.Join(" ", name));
                                                    row.Cells[1].Paragraphs.First().Append(type);
                                                }

                                                docx.Save();
                                            }
                                            catch (Exception)
                                            {
                                                // ignored
                                            }
                                        }
                                        pages.Add(new Source(new WmlDocument("4", lastPage.ToArray()), true));
                                    }
                                }
                                else pages.Add(new Source(new WmlDocument("4", Resources.PV_Template_P4), true));

                                // assemble pages and write to file
                                var id = string.IsNullOrEmpty(property.RequisitionId)
                                    ? property.PropertyId
                                    : property.RequisitionId.Replace("/", "_");

                                try
                                {
                                    var path = project.LivrablesPath + @"\Procés Verbaux DOCX\" + id + @".docx";
                                    if (File.Exists(path)) File.Delete(path);
                                    File.WriteAllBytes(path,
                                        DocumentBuilder.BuildDocument(pages).DocumentByteArray);
                                }
                                catch (Exception)
                                {
                                    throw new Exception("Impossible d'enregistrer le fichier.");
                                }
                                // 
                            }
                            catch (Exception ex)
                            {
                                var log = "\nImpossible de traiter la propriété " + property.PropertyId;
                                log += "\nErreur : " + ex.Message;
                                editor.WriteMessage(log);
                            }
                        }
                        editor.WriteMessage("\nOpération términée.");
                    }
                }
            }
            catch (Exception e)
            {
                sqLiteConnection.Close();
                if (e.Message.Contains("annulée"))
                    editor.WriteMessage("\n" + e.Message);
                else
                {
                    editor.WriteMessage(Environment.NewLine + "Erreur : " + e.Message);
                    editor.WriteMessage("\nOpération échouée.");
                }
            }
        }
    }
}
