﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Ribbon;
using Autodesk.AutoCAD.Runtime;
using Autodesk.Windows;
using Engine;
using Portable.Licensing;
using Portable.Licensing.Validation;
using Exception = System.Exception;
using Orientation = System.Windows.Controls.Orientation;
using _app = Autodesk.AutoCAD.ApplicationServices.Application;

[assembly: ExtensionApplication(typeof(Untitled))]

namespace Engine
{
    public class Untitled : IExtensionApplication
    {
        void IExtensionApplication.Initialize()
        {
            var collection = _app.DocumentManager;
            collection.DocumentBecameCurrent += CollectionDocumentBecameCurrent;
            

            var dwg = collection.MdiActiveDocument;
            var ed = dwg.Editor;

            try
            {
                ed.WriteMessage("\nInitialisation du {0}... ", GetType().Name);
                
                ed.WriteMessage("\nVerification de la license...");
                //VerifyLicense();
                ed.WriteMessage("terminée.\n");

                ed.WriteMessage("Chargememt du ribbon...");
                if (ComponentManager.Ribbon == null)
                    ComponentManager.ItemInitialized += ComponentManagerItemInitialized;
                else
                    LoadRibbon();

                Utils.PolylineContextMenu.Attach();

                ed.WriteMessage("terminée.\n");
                ed.WriteMessage("Initialisation terminée.\n");
            }
            catch (Exception)
            {
                ed.WriteMessage("échouée. AutoCAD vas sortir maintenant.\n");
                Thread.Sleep(1000);
                _app.Quit();
            }
        }

        private static void VerifyLicense()
        {
            var publicKey = Properties.Settings.Default.PublicKey;
            if (string.IsNullOrEmpty(publicKey))
            {
                var openf = new OpenFileDialog()
                {
                    Title = "",
                    Filter = @"Fichier key | *.key",
                    Multiselect = false
                };

                var result = openf.ShowDialog(new AcadMainWindow(){Handle = _app.MainWindow.Handle});
                if (result == DialogResult.OK)
                    publicKey = File.ReadAllText(openf.FileName);
            }
            var license = License.Load(new MemoryStream(Properties.Resources.License));
            var validationFailures = license.Validate()
                                .Signature(publicKey)
                                .AssertValidLicense();

            if(validationFailures.Any()) throw new Exception();

            Properties.Settings.Default.PublicKey = publicKey;
            Properties.Settings.Default.Save();
        }

        private static void ComponentManagerItemInitialized(object sender, RibbonItemEventArgs e)
        {
            if (ComponentManager.Ribbon == null) return;
            LoadRibbon();
            ComponentManager.ItemInitialized -= ComponentManagerItemInitialized;
        }

        private static void LoadRibbon()
        {
            try
            {
                var handler = new Handler();

                var projet = new RibbonRowPanel();
                projet.Items.Add(CreateButton(" Créer ", "CreateProject", handler, Properties.Resources._0));
                projet.Items.Add(CreateButton(" Ouvrir ", "OpenDwg", handler, Properties.Resources._1));

                var entrees = new RibbonRowPanel();
                //données.Items.Add(CreateButton(" Rapport \n GPS ", "ParseGpsFile", handler, Properties.Resources._0));
                entrees.Items.Add(CreateButton(" Fichier \n DAT ", "ParseDatFile", handler, Properties.Resources._4));
                entrees.Items.Add(CreateButton(" Etat \n Parcelaire ", "LoadEtatParcelaire", handler, Properties.Resources._5));
                entrees.Items.Add(CreateButton(" Etat \n Juridique ", "LoadEtatJuridique", handler, Properties.Resources._6));

                var livrables = new RibbonRowPanel();
                livrables.Items.Add(CreateButton(" Plan \n Individuel ", "GenerateCalque", handler, Properties.Resources._8));
                livrables.Items.Add(CreateButton(" PhotoA4 ", "GeneratePhoto", handler, Properties.Resources._9));
                livrables.Items.Add(CreateButton(" Procés \n Verbal ", "GeneratePVs", handler, Properties.Resources._10));

                var impression = new RibbonRowPanel();
                impression.Items.Add(CreateButton(" Fichier Actif ", "PlotToPDFSingle", handler, Properties.Resources._11));
                impression.Items.Add(CreateButton(" Autres ", "PlotToPDFMultiple", handler, Properties.Resources._12));

                var outils = new RibbonRowPanel();
                outils.Items.Add(CreateButton(" Lier les N° \n de Propriétés ", "MatchGoldenIds", handler, Properties.Resources._13));
                //outils.Items.Add(CreateButton(" Reporter les \n Mapppes ", "CalculateMappes", handler, Properties.Resources._14));
                //outils.Items.Add(CreateButton(" Reporter les \n Consistances ", "CalculateConsistance", handler, Properties.Resources._15));
                outils.Items.Add(CreateButton(" Reporter les N° \n de Réquisitions ", "MatchRequisitionIds", handler, Properties.Resources._17));
                //outils.Items.Add(CreateButton(" Générer le DAT des \n N° de Réquisitions  ", "GenerateDatFiles", handler, Properties.Resources._15));

                var prjPanelSource = new RibbonPanelSource { Title = "Projet" };
                prjPanelSource.Items.Add(projet);
                var dataPanelSource = new RibbonPanelSource { Title = "Entrées" };
                dataPanelSource.Items.Add(entrees);
                var livrPanelSource = new RibbonPanelSource { Title = "Livrables" };
                livrPanelSource.Items.Add(livrables);
                var impPanelSource = new RibbonPanelSource { Title = "PDF" };
                impPanelSource.Items.Add(impression);
                var otlPanelSource = new RibbonPanelSource { Title = "Outils" };
                otlPanelSource.Items.Add(outils);

                var prjPanel = new RibbonPanel { Source = prjPanelSource };
                var dataPanel = new RibbonPanel { Source = dataPanelSource, IsEnabled = false };
                var livrPanel = new RibbonPanel { Source = livrPanelSource, IsEnabled = false };
                var impPanel = new RibbonPanel { Source = impPanelSource, IsEnabled = true };
                var otlPanel = new RibbonPanel { Source = otlPanelSource, IsEnabled = false };

                var tab = new RibbonTab { Title = "ZeddCAD", Id = "Untitled", IsContextualTab = false };
                tab.Panels.Add(prjPanel);
                tab.Panels.Add(dataPanel);
                tab.Panels.Add(livrPanel);
                tab.Panels.Add(impPanel);
                tab.Panels.Add(otlPanel);

                var ribbonControl = RibbonServices.RibbonPaletteSet.RibbonControl;
                ribbonControl.Tabs.Add(tab);
                ribbonControl.ActiveTab = tab;
            }
            catch (Exception ex)
            {
                _app.ShowAlertDialog(ex.ToString());
            }
        }

        private static RibbonButton CreateButton(string name, string id, ICommand handler, Bitmap image, bool enabled = true)
        {
            return new RibbonButton
            {
                Text = name,
                Orientation = Orientation.Vertical,
                Size = RibbonItemSize.Large,
                ShowText = true,
                ShowImage = true,
                Id = id,
                CommandHandler = handler,
                IsEnabled = enabled,
                LargeImage = GetBitmap(image)
            };
        }

        public static void CollectionDocumentBecameCurrent(object sender, DocumentCollectionEventArgs e)
        {
            using (e.Document.LockDocument())
            {
                ToggleRibbon(e.Document.Database.IsProjectDatabase());
            }
        }

        void IExtensionApplication.Terminate()
        {
            ComponentManager.ItemInitialized -= ComponentManagerItemInitialized;
            Utils.PolylineContextMenu.Detach();
        }

        static BitmapImage GetBitmap(Bitmap image)
        {
            image.MakeTransparent(Color.White);
            var stream = new MemoryStream();
            image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);

            var bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = stream;
            bmp.DecodePixelHeight = 32;
            bmp.DecodePixelWidth = 32;
            bmp.EndInit();

            return bmp;
        }

        static void ToggleRibbon(bool enable)
        {
            if (RibbonServices.RibbonPaletteSet == null)
                return;
            var ribbonControl = RibbonServices.RibbonPaletteSet.RibbonControl;
            var tab = ribbonControl.Tabs.First(t => t.Id == "Untitled");

            tab.Panels.Where(p => p.Source.Title != "Projet" && p.Source.Title != "PDF")
                .ToList()
                .ForEach(p => p.IsEnabled = enable);
        }
    }
}
