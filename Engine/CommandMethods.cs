﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Engine.Application;
using Engine.Data.Model;
using Engine.Forms;
using Engine.Utils;
using SQLite;
using App = Autodesk.AutoCAD.ApplicationServices.Application;
using Autodesk.AutoCAD.Runtime;
using Engine;
using Engine.Data;
using Exception = System.Exception;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;
// ReSharper disable InconsistentNaming

// ReSharper disable FunctionComplexityOverflow

#pragma warning disable 618

[assembly: CommandClass(typeof(CommandMethods))]

namespace Engine
{
    public class CommandMethods
    {
        [CommandMethod("CreateProject")]
        public void CreateProject()
        {
            var document = App.DocumentManager.MdiActiveDocument;
            var form = new CreateProjectForm();

            var result = App.ShowModalDialog(form);
            if (result != DialogResult.OK)
            {
                form.Dispose();
                return;
            }

            App.DocumentManager.MdiActiveDocument = App.DocumentManager.Open(form.DwgPath, false);
            form.Dispose();

            document.SendStringToExecute("_zoom _e\n", true, false, false);
        }

        [CommandMethod("ParseDatFile")]
        public void ParseDatFile()
        {
            var document = App.DocumentManager.MdiActiveDocument;

            var fileDialog = new OpenFileDialog
            {
                Title = @"Ouvrir un fichier DAT",
                CheckFileExists = true,
                Filter = @"Fichier TXT | *.txt",
                Multiselect = false,
                InitialDirectory = Environment.CurrentDirectory
            };

            var result = fileDialog.ShowDialog(new AcadMainWindow { Handle = App.MainWindow.Handle });

            if (result != DialogResult.OK) return;

            if (!Controller.LoadDatProperties(fileDialog.FileName, ProjectInfo.LoadFrom(document.Database)))
                return;

            document.SendStringToExecute("_zoom _e\n", true, false, false);
            document.SendStringToExecute("_qsave\n", true, false, false);
        }

        [CommandMethod("ParseGpsFile")]
        public void ParseGpsFile()
        {
            var document = App.DocumentManager.MdiActiveDocument;

            var form = new LoadGpsPoinsForm();

            var result = App.ShowModalDialog(form);
            if (result != DialogResult.OK)
            {
                form.Dispose();
                return;
            }

            if (!form.IsDisposed)
                form.Dispose();

            document.SendStringToExecute("_zoom _e\n", true, false, false);
            document.SendStringToExecute("_qsave\n", true, false, false);
        }

        [CommandMethod("CreateParcel")]
        public void CreateParcel()
        {
            var document = App.DocumentManager.MdiActiveDocument;
            var db = document.Database;
            var editor = document.Editor;
            var project = ProjectInfo.LoadFrom(db);
            var sql = new SQLiteConnection(project.DbPath);
            sql.BeginTransaction();
            var msId = SymbolUtilityServices.GetBlockModelSpaceId(db);
            PLineJig jig = null;
            using (var transaction = db.TransactionManager.StartOpenCloseTransaction())
            {
                try
                {
                    PromptResult proceedToNext;
                    var ms = (BlockTableRecord)transaction.GetObject(msId, OpenMode.ForWrite);

                    do
                    {
                        jig = new PLineJig(editor.CurrentUserCoordinateSystem);
                        string id;
                        var sprompt = editor.GetString(new PromptStringOptions("Identifiant parcelle : ")
                        {
                            AllowSpaces = false,
                            DefaultValue = "",
                            UseDefaultValue = true,
                        });
                        if (sprompt.Status == PromptStatus.OK && !string.IsNullOrEmpty(sprompt.StringResult))
                            id = sprompt.StringResult.Trim();
                        else
                            throw new Exception("Términé.");

                        var part = 0;
                        var iprompt = editor.GetInteger(new PromptIntegerOptions("P (0 = Unique): ")
                        {
                            DefaultValue = 0,
                            UseDefaultValue = true,
                            LowerLimit = 0,
                            AllowNegative = false,
                            AllowNone = true
                        });
                        if (iprompt.Status == PromptStatus.OK)
                            part = iprompt.Value;

                        bool success;
                        bool complete;

                        do
                        {
                            var result = editor.Drag(jig);

                            success = result.Status == PromptStatus.OK;
                            if (success)
                                jig.AddLatestVertex();

                            complete = result.Status == PromptStatus.None;
                            if (complete)
                                jig.RemoveLastVertex();

                        } while (success && !complete);

                        var polyline = (Polyline)jig.GetEntity();
                        polyline.Closed = true;
                        var points = new List<PointInfo>();
                        for (var i = 0; i < polyline.NumberOfVertices; i++)
                        {
                            var p = polyline.GetPoint2dAt(i);
                            var lId = long.Parse(string.Format("{0:F2}{1:F2}", p.X, p.Y).Replace(".", ""));
                            var pInfo = sql.Get<PointInfo>(lId);
                            if (pInfo == null || string.IsNullOrEmpty(pInfo.Name))
                            {

                                throw new Exception("\nBorne/Point de détail inconnu.");
                            }
                            points.Add(pInfo);
                        }

                        var parcelCount = sql.Table<ParcelInfo>().Count();

                        var plId = ms.AppendEntity(polyline);

                        var parcel = new ParcelInfo
                        {
                            ParcelId = ++parcelCount,
                            Part = part,
                            Handle = plId.Handle.Value
                        };

                        if (char.IsDigit(id[0])) parcel.PropertyId = id;
                        else if (id.StartsWith("R")) parcel.RequisitionId = id;
                        else if (id.StartsWith("T")) parcel.TitreId = id;
                        else parcel.GoldenId = id;

                        var links = points.Select(p => new LinkInfo
                        {
                            ParcelId = parcel.ParcelId,
                            PointId = p.PointId
                        });

                        sql.Insert(parcel, typeof(ParcelInfo));
                        sql.InsertAll(links, typeof(LinkInfo));

                        polyline.Dispose();

                        proceedToNext = editor.GetString(new PromptStringOptions("Continuer (O/N): ")
                            {
                                AllowSpaces = false,
                                DefaultValue = "O",
                                UseDefaultValue = true
                            });

                    } while (proceedToNext.Status == PromptStatus.OK && proceedToNext.StringResult == "O");
                }
                catch (Exception e)
                {
                    if (jig != null && !jig.GetEntity().IsDisposed)
                        jig.GetEntity().Dispose();
                    editor.WriteMessage(e.Message);

                    sql.Rollback();
                    sql.Close();
                    transaction.Abort();
                    return;
                }
                sql.Commit();
                sql.Close();
                transaction.Commit();
                document.SendStringToExecute("_qsave\n", true, false, false);
            }
        }

        [CommandMethod("ModifyParcel")]
        public void ModifyParcel()
        {
            var document = App.DocumentManager.MdiActiveDocument;
            var db = document.Database;
            var editor = document.Editor;
            var msId = SymbolUtilityServices.GetBlockModelSpaceId(db);
            var project = ProjectInfo.LoadFrom(db);
            var sql = new SQLiteConnection(project.DbPath);
            sql.BeginTransaction();
            Polyline oldPolyline = null;
            PLineJig jig = null;

            using (var transaction = db.TransactionManager.StartOpenCloseTransaction())
            {
                try
                {
                    PromptResult proceedToNext;
                    var ms = (BlockTableRecord)transaction.GetObject(msId, OpenMode.ForWrite);

                    do
                    {
                        var result = editor.GetEntity(new PromptEntityOptions("Sélectionnez un polyline : ")
                        {
                            AllowNone = true,
                            AllowObjectOnLockedLayer = false
                        });
                        ParcelInfo oldParcel;
                        if (result.Status == PromptStatus.OK && result.ObjectId.ObjectClass.DxfName == "LWPOLYLINE")
                        {
                            var oId = result.ObjectId;
                            oldParcel = sql.Table<ParcelInfo>().FirstOrDefault(p => p.Handle == oId.Handle.Value);
                            if (oldParcel == null)
                                throw new Exception("\nLe polyline sélectionné n'est pas associé à une parcelle.");

                            oldPolyline = (Polyline)transaction.GetObject(oId, OpenMode.ForWrite);
                        }
                        else
                            throw new Exception("\nSélection invalide.");

                        jig = new PLineJig(editor.CurrentUserCoordinateSystem);

                        bool success;
                        bool complete;

                        do
                        {
                            var dragResult = editor.Drag(jig);

                            success = dragResult.Status == PromptStatus.OK;
                            if (success)
                                jig.AddLatestVertex();

                            complete = dragResult.Status == PromptStatus.None;
                            if (complete)
                                jig.RemoveLastVertex();

                        } while (success && !complete);

                        var polyline = (Polyline)jig.GetEntity();
                        polyline.Closed = true;
                        var plId = ms.AppendEntity(polyline);
                        var points = new List<PointInfo>();
                        for (var i = 0; i < polyline.NumberOfVertices; i++)
                        {
                            var p = polyline.GetPoint2dAt(i);
                            var lId = long.Parse(string.Format("{0:F2}{1:F2}", p.X, p.Y).Replace(".", ""));
                            var pInfo = sql.Get<PointInfo>(lId);
                            if (pInfo == null || string.IsNullOrEmpty(pInfo.Name))
                            {
                                throw new Exception("\nBorne/Point de détail inconnu.");
                            }
                            points.Add(pInfo);
                        }
                        polyline.Dispose();

                        oldParcel.Handle = plId.Handle.Value;

                        var parcelId = oldParcel.ParcelId;
                        var links = points.Select(p => new LinkInfo
                        {
                            ParcelId = parcelId,
                            PointId = p.PointId
                        });

                        sql.Update(oldParcel, typeof(ParcelInfo));
                        sql.Table<LinkInfo>().Where(p => p.ParcelId == parcelId)
                            .ToList()
                            .ForEach(l => sql.Delete<LinkInfo>(l.LinkId));

                        sql.InsertAll(links, typeof(LinkInfo));

                        oldPolyline.Erase();
                        oldPolyline.Close();

                        proceedToNext = editor.GetString(new PromptStringOptions("Continuer (O/N): ")
                        {
                            AllowSpaces = false,
                            DefaultValue = "O",
                            UseDefaultValue = true
                        });
                    } while (proceedToNext.Status == PromptStatus.OK && proceedToNext.StringResult == "O");

                    sql.Commit();
                    sql.Close();
                    transaction.Commit();
                    document.SendStringToExecute("_qsave\n", true, false, false);
                }
                catch (Exception e)
                {
                    if (jig != null && !jig.GetEntity().IsDisposed)
                        jig.GetEntity().Dispose();

                    if (oldPolyline != null)
                        oldPolyline.Close();

                    sql.Rollback();
                    sql.Close();
                    transaction.Abort();
                    editor.WriteMessage("\n" + e.Message + "\n" + e.StackTrace);
                }
            }
        }

        [CommandMethod("GenerateCalque")]
        public void GenerateCalque()
        {
            var form = new GenerateCalqueForm();

            var result = App.ShowModalDialog(form);
            if (result != DialogResult.OK)
            {
                form.Dispose();
                return;
            }

            if (!form.IsDisposed)
                form.Dispose();
        }

        [CommandMethod("GeneratePhoto")]
        public void GeneratePhotos()
        {
            var form = new GeneratePhotoForm();

            var result = App.ShowModalDialog(form);
            if (result != DialogResult.OK)
            {
                form.Dispose();
                return;
            }

            if (!form.IsDisposed)
                form.Dispose();
        }

        [CommandMethod("GeneratePVs")]
        public void GeneratePVs()
        {
            var form = new GeneratePvForm();

            var result = App.ShowModalDialog(form);
            if (result != DialogResult.OK)
            {
                form.Dispose();
                return;
            }

            if (!form.IsDisposed)
                form.Dispose();
        }

        [CommandMethod("OpenDwg")]
        public void OpenDwg()
        {
            var document = App.DocumentManager.MdiActiveDocument;
            document.SendStringToExecute("ouvrir\n", true, false, false);
        }

        [CommandMethod("LoadEtatParcelaire")]
        public void LoadEtatParcelaire()
        {
            var document = App.DocumentManager.MdiActiveDocument;

            var form = new LoadEtatParcelaireForm();

            var result = App.ShowModalDialog(form);
            if (result != DialogResult.OK)
            {
                form.Dispose();
                return;
            }

            if (!form.IsDisposed)
                form.Dispose();

            document.SendStringToExecute("_zoom _e\n", true, false, false);
            document.SendStringToExecute("_qsave\n", true, false, false);
        }

        [CommandMethod("LoadEtatJuridique")]
        public void LoadEtatJuridique()
        {
            var document = App.DocumentManager.MdiActiveDocument;

            var form = new LoadEtatJuridiqueForm();

            var result = App.ShowModalDialog(form);
            if (result != DialogResult.OK)
            {
                form.Dispose();
                return;
            }

            if (!form.IsDisposed)
                form.Dispose();

            document.SendStringToExecute("_zoom _e\n", true, false, false);
            document.SendStringToExecute("_qsave\n", true, false, false);
        }

        [CommandMethod("MatchGoldenIds")]
        public void MatchGoldenIds()
        {
            var document = App.DocumentManager.MdiActiveDocument;
            document.SendStringToExecute("_zoom _e\n", true, false, false);

            var form = new CopyIdsForm();

            var result = App.ShowModalDialog(form);
            if (result != DialogResult.OK)
            {
                form.Dispose();
                return;
            }

            if (!form.IsDisposed)
                form.Dispose();
        }

        [CommandMethod("PlotToPDFSingle")]
        public void PlotToPDFSingle()
        {
            var document = App.DocumentManager.MdiActiveDocument;
            document.SendStringToExecute("_zoom _e\n", true, false, false);
            Plotting.Plotter.PlotToPdf();
        }

        [CommandMethod("PlotToPDFMultiple", CommandFlags.Session)]
        public void PlotToPDFMultiple()
        {
            var fileDialog = new OpenFileDialog
            {
                CheckFileExists = true,
                Filter = @"Fichier DWG | *.dwg",
                Multiselect = true,
                InitialDirectory = Environment.CurrentDirectory
            };

            var result = fileDialog.ShowDialog(new AcadMainWindow { Handle = App.MainWindow.Handle });

            if (result != DialogResult.OK) return;

            foreach (var fileName in fileDialog.FileNames)
            {
                var document = App.DocumentManager.Open(fileName, true);
                App.DocumentManager.MdiActiveDocument = document;
                Plotting.Plotter.PlotToPdf();
                document.CloseAndDiscard();
            }
            fileDialog.Dispose();
        }

        [CommandMethod("MatchRequisitionIds", CommandFlags.Session)]
        public void MatchRequisitionIds()
        {
            var document = App.DocumentManager.MdiActiveDocument;
            var project = ProjectInfo.LoadFrom(document.Database);
            var suffix = "-";
            var sql = new SQLiteConnection(project.DbPath);
            var fileDialog = new OpenFileDialog
            {
                Title = @"Ouvrir un fichier DWG",
                CheckFileExists = true,
                Filter = @"Fichier DWG | *.dwg",
                Multiselect = true,
                InitialDirectory = Environment.CurrentDirectory
            };

            var result = fileDialog.ShowDialog(new AcadMainWindow { Handle = App.MainWindow.Handle });

            if (result != DialogResult.OK) return;

            foreach (var fileName in fileDialog.FileNames)
            {
                var doc = App.DocumentManager.Open(fileName, false);
                App.DocumentManager.MdiActiveDocument = doc;
                Controller.ReplaceIds(suffix, sql);
            }
            fileDialog.Dispose();
            sql.Close();
        }

        [CommandMethod("RemoveParcel", CommandFlags.UsePickSet)]
        public void RemoveParcel()
        {
            var doc = App.DocumentManager.MdiActiveDocument;
            var db = doc.Database;
            var ed = doc.Editor;

            using (doc.LockDocument())
            {
                try
                {
                    var psr = ed.GetSelection();
                    if (psr.Status == PromptStatus.OK)
                    {
                        var project = ProjectInfo.LoadFrom(db);
                        var sql = new SQLiteConnection(project.DbPath);
                        var table = sql.Table<ParcelInfo>();
                        var count = 0;
                        using (var tx = db.TransactionManager.StartTransaction())
                        {
                            using (var lom = new LongOperationManager("Suppression des parcelles : ", 100))
                            {
                                var ids = psr.Value.GetObjectIds();
                                lom.SetTotalOperations(ids.Length);
                                foreach (var id in ids)
                                {
                                    var handle = id.Handle.Value;
                                    var parcel = table.FirstOrDefault(p => p.Handle == handle);
                                    if (parcel == null)
                                    {
                                        count++;
                                        continue;
                                    }
                                    var entity = tx.GetObject(id, OpenMode.ForWrite);
                                    entity.Erase();
                                    entity.Dispose();
                                    
                                    sql.Delete(parcel);
                                }
                            }
                            tx.Commit();
                        }
                        if (count > 0)
                            ed.WriteMessage("\nErreur Lors de la suppression de {0} objet(s).", count);

                        doc.SendStringToExecute("_QSAVE\n", true, false, false);
                    }
                    else
                        throw new Exception();
                }
                catch (Exception)
                {
                    Console.WriteLine(@"Erreur Lors de la suppression des parcelles.");
                }
            }
        }

        [CommandMethod("-PYLOAD")]
        public static void PythonLoadCmdLine()
        {
            PythonLoad(true);
        }

        [CommandMethod("PYLOAD")]
        public static void PythonLoadUI()
        {
            PythonLoad(false);
        }

        public static void PythonLoad(bool useCmdLine)
        {
            var doc = App.DocumentManager.MdiActiveDocument;
            var ed = doc.Editor;

            var fd = (short)App.GetSystemVariable("FILEDIA");

            // As the user to select a .py file

            var pfo = new PromptOpenFileOptions("Select Python script to load")
            {
                Filter = "Python script (*.py)|*.py",
                PreferCommandLine = (useCmdLine || fd == 0)
            };

            var pr =
              ed.GetFileNameForOpen(pfo);

            // And then try to load and execute it

            if (pr.Status == PromptStatus.OK)
                ExecutePythonScript(pr.StringResult);
        }

        [LispFunction("PYLOAD")]
        public ResultBuffer PythonLoadLISP(ResultBuffer rb)
        {
            const int RTSTR = 5005;

            var doc = App.DocumentManager.MdiActiveDocument;
            var ed = doc.Editor;

            if (rb == null)
            {
                ed.WriteMessage("\nError: too few arguments\n");
            }
            else
            {
                // We're only really interested in the first argument

                Array args = rb.AsArray();
                var tv = (TypedValue)args.GetValue(0);

                // Which should be the filename of our script

                if (tv.TypeCode == RTSTR)
                {
                    // If we manage to execute it, let's return the
                    // filename as the result of the function
                    // (just as (arxload) does)

                    var success =
                      ExecutePythonScript(Convert.ToString(tv.Value));
                    return
                      (success ?
                        new ResultBuffer(
                          new TypedValue(RTSTR, tv.Value)
                        )
                        : null);
                }
            }
            return null;
        }

        private static bool ExecutePythonScript(string file)
        {
            // If the file exists, let's load and execute it
            // (we could/should probably add some more robust
            // exception handling here)

            var ret = System.IO.File.Exists(file);
            if (ret)
            {
                ScriptEngine engine = Python.CreateEngine();
                engine.ExecuteFile(file);
            }
            return ret;
        }

        [CommandMethod("ListTextStyles")]
        public void ListTextStyles()
        {
            var document = App.DocumentManager.MdiActiveDocument;
            var editor = document.Editor;
            var db = document.Database;

            using (var transaction = db.TransactionManager.StartTransaction())
            {
                var ts = (TextStyleTable) transaction.GetObject(db.TextStyleTableId, OpenMode.ForRead);

                foreach (var id in ts)
                {
                    var style = (TextStyleTableRecord) transaction.GetObject(id, OpenMode.ForRead);
                    editor.WriteMessage("\n" + style.Name);
                    editor.WriteMessage(", " + style.FileName);
                    editor.WriteMessage(", Angle = " + style.ObliquingAngle);
                    editor.WriteMessage(", XScale = " + style.XScale);
                    editor.WriteMessage(", Font = " + style.Font.TypeFace);
                    editor.WriteMessage("\n");
                }
            }
        }
    }
}
