﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Engine.Application;
using Engine.Data.View;
using Engine.Properties;
using Engine.Utils;
using GeoAPI.Geometries;
using SQLite;
using Point = Engine.Data.View.Point;

// ReSharper disable AccessToForEachVariableInClosure

namespace Engine.Documents
{
    public struct CalqueFormat
    {
        public string Name { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
    }

    public static class CalqueFormatTypes
    {
        public static CalqueFormat St25
        {
            get
            {
                return new CalqueFormat { Name = "ST25", Height = .26, Width = .16 };
            }
        }
        public static CalqueFormat St26
        {
            get
            {
                return new CalqueFormat { Name = "ST26", Height = .26, Width = .26 };
            }
        }
        public static CalqueFormat St27
        {
            get
            {
                return new CalqueFormat { Name = "ST27", Height = .42, Width = .37 };
            }
        }
        public static CalqueFormat St28
        {
            get
            {
                return new CalqueFormat { Name = "ST28", Height = .42, Width = .47 };
            }
        }
        public static CalqueFormat St29
        {
            get
            {
                return new CalqueFormat { Name = "ST29", Height = .42, Width = .58 };
            }
        }
        public static CalqueFormat St30
        {
            get
            {
                return new CalqueFormat { Name = "ST30", Height = .53, Width = .53 };
            }
        }
        public static CalqueFormat Aigle
        {
            get
            {
                return new CalqueFormat { Name = "Aigle", Height = .60, Width = .90 };
            }
        }

        public static CalqueFormat GetCalqueFormat(int i)
        {
            switch (i)
            {
                case 0:
                    return St25;
                case 1:
                    return St26;
                case 2:
                    return St27;
                case 3:
                    return St28;
                case 4:
                    return St29;
                case 5:
                    return St30;
                case 6:
                    return Aigle;
                default:
                    throw new Exception();
            }
        }
    }

    public class VersBorne
    {
        public bool Equals(VersBorne other)
        {
            return Name == other.Name && Start.DistanceTo(other.Start) < 0.2;
        }

        public override int GetHashCode()
        {
            return (Name != null ? Name.GetHashCode() * Start.GetHashCode() : 0);
        }

        public Point3d Start { get; set; }
        public Vector3d Vector { get; set; }
        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is VersBorne && Equals((VersBorne)obj);
        }
    }

    public class PlanIndividuel
    {
        public Property Property { get; set; }
        public ProjectInfo ProjectInfo { get; set; }
        public SQLiteConnection SqLite { get; set; }
        public Database Database { get; set; }
        private Dictionary<Parcel, List<Parcel>> RiverainsParcels { get; set; }
        private Transaction Transaction { get; set; }
        public CalqueFormat Format { get; set; }
        public int ScaleNumber { get; set; }
        private Dictionary<Parcel, Point3d[]> Piste { get; set; }
        private List<BlockReference> Blocks { get; set; }
        private Dictionary<MText, string> Mtexts { get; set; }
        private Polyline InnerRectangle { get; set; }
        private Polyline OuterRectangle { get; set; }
        private List<Line> Limits { get; set; }
        private List<MText> Riverains { get; set; }
        private List<MText> Bornes { get; set; }
        private List<Entity> AreaTable { get; set; }
        private List<Entity> Coordinates { get; set; }
        private List<Line> Croisions { get; set; }
        private List<Entity> VersBornes { get; set; }
        private Dictionary<Entity, string> EntityDetails { get; set; }
        private ObjectIdCollection ObjectIdDetails { get; set; }
        private IGeometry ConvexHull { get; set; }
        public Point3d Grip { get; set; }
        private MText PditeFr { get; set; }
        private MText PditeAr { get; set; }

        public PlanIndividuel(Property property, ProjectInfo projectInfo, SQLiteConnection sqLite, Database db, Transaction transaction)
        {
            Property = property;
            Database = db;
            SqLite = sqLite;
            ProjectInfo = projectInfo;
            Transaction = transaction;

            RiverainsParcels = Functions.GetRiverainsParcels(Property, Functions.GetConvexHull(Property), SqLite);
            Piste = Functions.GetPiste(Property, RiverainsParcels, SqLite);
            ConvexHull = Functions.SetConvexHull(Property, Piste);
            SetSizeAndScale(Settings.Default.CqFormatMode, Settings.Default.MaxST);
            SetRectangles();
            SetCroisions();
            SetCoordinates();
            SetStaticTexts();
            SetLimites();
            SetBornes();
            SetVersBornes();
            SetRiverains();
            SetPiste();
            AddTextForPiste();
            SetAreaTable();
            GetDetail();
        }

        private void SetPiste()
        {
            foreach (var entry in Piste)
            {
                var parcel = entry.Key;
                var points = entry.Value;

                var text = CreateTextForRiverains(parcel, points.Select(p =>
                    parcel.Points.First(pt => pt.P3D.DistanceTo(p) < 0.02)).ToList());
                if (text != null)
                    Riverains.Add(text);

                if (points.Count() == 1)
                    continue;

                for (var i = 0; i < points.Length - 1; i++)
                {
                    var line = new Line(points[i], points[i + 1]) { ColorIndex = 7 };
                    Limits.Add(line);
                }

            }
        }

        private void GetDetail()
        {
            EntityDetails = new Dictionary<Entity, string>();
            ObjectIdDetails = new ObjectIdCollection();

            var offset = Functions.OffsetParcel(ConvexHull, .01 * ScaleNumber);
            var oIds = Functions.GetEntitiesFromLayersCrossing(offset, Settings.Default.Dtl);

            if (oIds == null) return;

            foreach (var objectId in oIds)
            {
                LinetypeTableRecord linetype;
                Entity e;
                switch (objectId.ObjectClass.DxfName)
                {
                    case "LWPOLYLINE":
                        e = Transaction.GetObject(objectId, OpenMode.ForRead) as Polyline;
                        if (e == null) break;
                        var lw = Functions.SplitPolyline(offset, (Polyline)e);
                        if (lw == null) break;
                        EntityDetails.Add(lw, "");
                        if (e.LinetypeId == ObjectId.Null) break;
                        linetype = (LinetypeTableRecord)Transaction.GetObject(e.LinetypeId, OpenMode.ForRead);
                        EntityDetails[lw] = linetype.Name;
                        break;
                    case "LINE":
                        e = Transaction.GetObject(objectId, OpenMode.ForRead) as Line;
                        if (e == null) break;
                        var l = Functions.SplitPolyline(offset, (Line)e);
                        EntityDetails.Add(l, "");
                        if (e.LinetypeId == ObjectId.Null) break;
                        linetype = (LinetypeTableRecord)Transaction.GetObject(e.LinetypeId, OpenMode.ForRead);
                        EntityDetails[l] = linetype.Name;
                        break;
                    case "POLYLINE":
                        e = Transaction.GetObject(objectId, OpenMode.ForRead) as Polyline2d;
                        if (e == null) break;
                        var poly = Functions.SplitPolyline(offset, (Polyline2d)e);
                        EntityDetails.Add(poly, "");
                        if (e.LinetypeId == ObjectId.Null) break;
                        linetype = (LinetypeTableRecord)Transaction.GetObject(e.LinetypeId, OpenMode.ForRead);
                        EntityDetails[poly] = linetype.Name;
                        break;
                    default:
                        ObjectIdDetails.Add(objectId);
                        break;
                }
            }
        }

        // ReSharper disable once InconsistentNaming
        private void SetSizeAndScale(int formatModeIndex, int maxST)
        {
            var width = ConvexHull.Coordinates.Max(c => c.X) - ConvexHull.Coordinates.Min(c => c.X);
            var height = ConvexHull.Coordinates.Max(c => c.Y) - ConvexHull.Coordinates.Min(c => c.Y);

            var min = Settings.Default.CqMinScaleNumber;

            switch (formatModeIndex)
            {
                case 0:
                    var zero = new[] { 500, 1000 };
                    var first = new[] { 500, 1000, 2000 };
                    var second = new[] { 500, 1000, 2000, 5000 };
                    var third = new[] { 500, 1000, 2000, 5000, 10000 };
                    var fourth = new[] { 500, 1000, 2000, 5000, 10000, 7500 };
                    var fifth = new[] { 500, 1000, 2000, 5000, 10000, 7500, 20000 };

                    var scales = new[] { zero, first, second, third, fourth, fifth };

                    for (var j = 0; j < scales.Length; j++)
                    {
                        var s = j == maxST + 1 ? fifth : scales[j];
                        for (var i = 0; i < s.Length; i++)
                        {
                            var index = j == maxST + 1 ? j - 1 : j;
                            var current = s[i];
                            if (current < min) continue;
                            CalqueFormat format;
                            if (i >= 2 && i == s.Length - 1)
                            {
                                format = CalqueFormatTypes.GetCalqueFormat(index - 1);
                                if (!((format.Height < height / current + .1) ||
                                      (format.Width < width / current + .06)))
                                {
                                    ScaleNumber = current;
                                    Format = format;
                                    return;
                                }
                            }

                            format = CalqueFormatTypes.GetCalqueFormat(index);
                            if ((format.Height < height / current + .1) ||
                                (format.Width < width / current + .06)) continue;
                            ScaleNumber = current;
                            Format = format;
                            return;
                        }
                    }

                    break;
                case 1:
                    ScaleNumber = GetStandardScale();
                    for (var i = 0; i < 6; i++)
                    {
                        var format = CalqueFormatTypes.GetCalqueFormat(i);
                        if ((format.Height < height / ScaleNumber + .1) ||
                            (format.Width < width / ScaleNumber + .06)) continue;
                        Format = format;
                        return;
                    }
                    break;
                case 2:
                    ScaleNumber = Settings.Default.CqFixedScaleNumber;
                    for (var i = 0; i < 6; i++)
                    {
                        var format = CalqueFormatTypes.GetCalqueFormat(i);
                        if ((format.Height < height / ScaleNumber + .1) ||
                            (format.Width < width / ScaleNumber + .06)) continue;
                        Format = format;
                        return;
                    }
                    break;
                case 3:
                    Format = CalqueFormatTypes.GetCalqueFormat(Settings.Default.CqFixedFormat);
                    var all = new[] { 500, 1000, 2000, 5000, 7500, 10000, 20000, 50000 };
                    foreach (var t in all)
                    {
                        if ((Format.Height < height / ScaleNumber + .1) ||
                            (Format.Width < width / ScaleNumber + .06)) continue;
                        ScaleNumber = t;
                        return;
                    }
                    break;
                default:
                    throw new Exception("Should never happen.");
            }

        }

        private int GetStandardScale()
        {
            var area = Property.Parcels.Sum(p => p.Contenance);
            var min = Settings.Default.CqMinScaleNumber;

            if (area < 2000)
                return 500 >= min ? 500 : min;
            if (area >= 2000 && area < 20000)
                return 1000 >= min ? 1000 : min;
            if (area >= 20000 && area < 200000)
                return 2000 >= min ? 2000 : min;
            if (area >= 200000 && area < 2000000)
                return 5000 >= min ? 5000 : min;
            return area >= 2000000 ? 10000 : 0;
        }

        // ReSharper disable once FunctionComplexityOverflow
        private void SetStaticTexts()
        {
            Mtexts = new Dictionary<MText, string>();

            if (Property.Parcels.Count > 1)
            {
                foreach (var parcel in Property.Parcels)
                {
                    var cons = new MText { Contents = "T.C", Attachment = AttachmentPoint.TopCenter };
                    var p3D = new Point3d(parcel.GetPolygon().Centroid.X, parcel.GetPolygon().Centroid.Y, 0);
                    if (!Functions.PointInPolygon(parcel.GetP3DCollection(), p3D))
                    {
                        var polygon =
                            Functions.GetBiggestConvexPolygon(parcel.Points.Select(p => p.Coordinate).ToList());
                        p3D = new Point3d(polygon.Centroid.X, polygon.Centroid.Y, 0);
                    }
                    var x = p3D.X;
                    var y = p3D.Y;
                    cons.Location = new Point3d(x, y - 2 * Settings.Default.PeTextHeightCst * ScaleNumber, 0);
                    cons.TextHeight = Settings.Default.PeTextHeightCst * ScaleNumber;
                    Mtexts.Add(cons, Settings.Default.Cst);

                    var part = new MText
                    {
                        Contents = "P." + parcel.Part,
                        Attachment = AttachmentPoint.TopCenter,
                        Location = new Point3d(x, y, 0),
                        TextHeight = Settings.Default.PeTextHeightCst * ScaleNumber
                    };
                    Mtexts.Add(part, Settings.Default.Cst);
                }
            }
            else
            {
                var parcel = Property.Parcels[0];
                var cons = new MText { Contents = "T.C", Attachment = AttachmentPoint.TopCenter };
                var p3D = new Point3d(parcel.GetPolygon().Centroid.X, parcel.GetPolygon().Centroid.Y, 0);
                if (!Functions.PointInPolygon(parcel.GetP3DCollection(), p3D))
                {
                    var polygon =
                        Functions.GetBiggestConvexPolygon(parcel.Points.Select(p => p.Coordinate).ToList());
                    p3D = new Point3d(polygon.Centroid.X, polygon.Centroid.Y, 0);
                }
                var x = p3D.X;
                var y = p3D.Y;
                cons.Location = new Point3d(x, y, 0);
                cons.TextHeight = .002 * ScaleNumber;
                Mtexts.Add(cons, Settings.Default.Cst);
            }

            var p1 = InnerRectangle.GetPoint3dAt(3);
            var propdite = new MText
            {
                Contents = "Propriété dite : ",
                Attachment = AttachmentPoint.BottomLeft,
                Location = new Point3d(p1.X, p1.Y + .012 * ScaleNumber, 0),
                TextHeight = .005 * ScaleNumber
            };
            Mtexts.Add(propdite, Settings.Default.Pdite);

            PditeFr = new MText
            {
                Contents = Property.NameFr,
                Attachment = AttachmentPoint.BottomLeft,
                Location = new Point3d(p1.X + 0.035 * ScaleNumber, p1.Y + .0136 * ScaleNumber, 0),
                TextHeight = .005 * ScaleNumber
            };
            Mtexts.Add(PditeFr, "ROMANT");

            var situee = new MText
            {
                Contents =
                    String.Format("Située à : Pce de {0}, Cle de {1}, C.R. {2} Douar {3}.", ProjectInfo.ProvinceFr, ProjectInfo.CercleFr,
                        ProjectInfo.CommuneFr, Property.AddressFr),
                Attachment = AttachmentPoint.BottomLeft,
                Location =
                    new Point3d(p1.X, p1.Y + .007 * ScaleNumber, 0),
                TextHeight = .0025 * ScaleNumber
            };
            Mtexts.Add(situee, "ROMANTS");

            var titre = new MText
            {
                Contents = "Titre : ",
                Attachment = AttachmentPoint.BottomLeft,
                Location =
                    new Point3d(p1.X, p1.Y + 0.002 * ScaleNumber, 0),
                TextHeight = .003 * ScaleNumber
            };
            Mtexts.Add(titre, "ROMANT");

            var p2 = InnerRectangle.GetPoint3dAt(2);

            var echelle = new MText
            {
                Contents = "Echelle : 1/" + ScaleNumber,
                Attachment = AttachmentPoint.BottomRight,
                Location = new Point3d(p2.X, p2.Y + .012 * ScaleNumber, 0),
                TextHeight = .003 * ScaleNumber
            };
            Mtexts.Add(echelle, Settings.Default.Ech);

            var pleve = new MText
            {
                Contents = "Plan levé en : " + ProjectInfo.Date,
                Attachment = AttachmentPoint.BottomRight,
                Location = new Point3d(p2.X, p2.Y + .008 * ScaleNumber, 0),
                TextHeight = .003 * ScaleNumber
            };
            Mtexts.Add(pleve, "ROMANT");

            var lim = new MText
            {
                Contents = "____ Limites de la propriété",
                Attachment = AttachmentPoint.BottomRight,
                Location = new Point3d(p2.X, p2.Y + .002 * ScaleNumber, 0),
                TextHeight = .003 * ScaleNumber
            };
            Mtexts.Add(lim, "ROMANT");

            var p3 = InnerRectangle.GetPoint3dAt(0);
            var dircad = new MText
            {
                Contents = "DIRECTION\nDU CADASTRE",
                Attachment = AttachmentPoint.MiddleCenter,
                Location = new Point3d(p3.X + 0.015 * ScaleNumber, p3.Y - 0.006 * ScaleNumber, 0),
                TextHeight = .003 * ScaleNumber
            };
            Mtexts.Add(dircad, Settings.Default.Cdtr);

            var pletpar = new MText
            {
                Contents = "Plan établit par : " + ProjectInfo.EtablitPar,
                Attachment = Format.Name == "ST25" ? AttachmentPoint.TopLeft : AttachmentPoint.TopCenter,
                Location =
                    new Point3d(Format.Name == "ST25" ? p3.X + .035 * ScaleNumber : p3.X / 2 + p2.X / 2,
                        p3.Y - 0.002 * ScaleNumber, 0),
                TextHeight = .002 * ScaleNumber
            };
            Mtexts.Add(pletpar, "ROMANT");

            var geom = new MText
            {
                Contents = "Géometre : " + ProjectInfo.Geometre,
                Attachment = Format.Name == "ST25" ? AttachmentPoint.TopLeft : AttachmentPoint.TopCenter,
                Location =
                    new Point3d(Format.Name == "ST25" ? p3.X + .035 * ScaleNumber : p3.X / 2 + p2.X / 2,
                        p3.Y - 0.006 * ScaleNumber, 0),
                TextHeight = .002 * ScaleNumber
            };
            Mtexts.Add(geom, "ROMANT");

            var dess = new MText
            {
                Contents = "Dessinateur : " + ProjectInfo.Dessinateur,
                Attachment = Format.Name == "ST25" ? AttachmentPoint.TopLeft : AttachmentPoint.TopCenter,
                Location =
                    new Point3d(Format.Name == "ST25" ? p3.X + .035 * ScaleNumber : p3.X / 2 + p2.X / 2,
                        p3.Y - 0.01 * ScaleNumber, 0),
                TextHeight = .002 * ScaleNumber
            };
            Mtexts.Add(dess, "ROMANT");

            var mappes = Functions.CalculateAllMappes(Property.Parcels, ScaleNumber);

            for (var i = 0; i < mappes.Length; i++)
            {
                var map = new MText
                {
                    Contents = mappes[i],
                    Attachment = AttachmentPoint.BottomRight,
                    Location = new Point3d(p2.X - 0.002 * ScaleNumber, p3.Y + (0.002 + i * 0.003) * ScaleNumber, 0),
                    TextHeight = .002 * ScaleNumber
                };
                Mtexts.Add(map, Settings.Default.Map);
            }

            var map2K = new MText
            {
                Contents = "Mappe de Repérage au 1/2000 : ".ToUpper() + Functions.CalculateAllMappes(Property.Parcels, 2000)[0],
                Attachment = AttachmentPoint.TopRight,
                Location = new Point3d(p2.X, p3.Y - 0.002 * ScaleNumber, 0),
                TextHeight = .002 * ScaleNumber
            };
            Mtexts.Add(map2K, "ROMANT");

            var cart = new MText
            {
                Contents = "Carte Topographique Au ".ToUpper() + ProjectInfo.Carte,
                Attachment = AttachmentPoint.TopRight,
                Location = new Point3d(p2.X, p3.Y - 0.006 * ScaleNumber, 0),
                TextHeight = .002 * ScaleNumber
            };
            Mtexts.Add(cart, "ROMANT");

            ReqFr = new MText
            {
                Contents = "Réquisition N°: ",
                Attachment = AttachmentPoint.BottomLeft,
                Location = new Point3d(p1.X - 0.005 * ScaleNumber, p1.Y, 0),
                TextHeight = .002 * ScaleNumber,
                Rotation = -Math.PI / 2
            };
            Mtexts.Add(ReqFr, "ROMANT");

            var sect = new MText
            {
                Contents = "Secteur IFE " + ProjectInfo.Secteur + " S.S. " + ProjectInfo.SubSector,
                Attachment = AttachmentPoint.TopCenter,
                Location = new Point3d(p1.X + 0.03 * ScaleNumber, p2.Y - 0.01 * ScaleNumber, 0),
                TextHeight = .002 * ScaleNumber
            };
            Mtexts.Add(sect, "ROMANT");

            var st = new MText
            {
                Contents = Format.Name,
                Attachment = AttachmentPoint.TopLeft,
                Location = new Point3d(p2.X + 0.005 * ScaleNumber, p2.Y, 0),
                TextHeight = .002 * ScaleNumber,
                Rotation = -Math.PI / 2
            };
            Mtexts.Add(st, "ROMANT");

            var id = new MText
            {
                Contents = "Plle " + Property.PropertyId,
                Attachment = AttachmentPoint.TopCenter,
                Location = sect.Location.Add(new Vector3d(0, -0.006 * ScaleNumber, 0)),
                TextHeight = .002 * ScaleNumber
            };
            Mtexts.Add(id, Settings.Default.PrId);

            var pt1 = InnerRectangle.GetPoint3dAt(1);

            PditeAr = new MText();
            if (!string.IsNullOrEmpty(Property.NameAr) && Property.NameAr.Any(Char.IsDigit))
            {
                var ns = Property.NameAr;
                var ss = ns.Substring(0, ns.Length - 2);
                var nb = ns.Substring(ns.Length - 2);
                propdite.Contents = String.Format(" {1} {0} : الملك المسمى", ss, nb);
            }
            else
                PditeAr.Contents = String.Format(" {0} : الملك المسمى", Property.NameAr);
            PditeAr.Attachment = AttachmentPoint.BottomRight;
            PditeAr.Location = new Point3d(pt1.X + .013 * ScaleNumber, pt1.Y, 0);
            PditeAr.TextHeight = .005 * ScaleNumber;
            PditeAr.Rotation = -Math.PI / 2;

            Mtexts.Add(PditeAr, Settings.Default.Txa);

            var sitAr = String.Format("اقليم {1} دائرة {2} الجماعة القروية {0} دوار", ProjectInfo.CommuneAr, ProjectInfo.ProvinceAr, ProjectInfo.CercleAr);

            var situeeAr = new MText
            {
                Contents = String.Format(" {1} {0} : الكائن", Property.AddressAr, sitAr),
                Attachment = AttachmentPoint.BottomRight,
                Location = new Point3d(pt1.X + .007 * ScaleNumber, pt1.Y, 0),
                TextHeight = .003 * ScaleNumber,
                Rotation = -Math.PI / 2
            };
            Mtexts.Add(situeeAr, Settings.Default.Txa);

            var titreAr = new MText
            {
                Contents = ": رسم",
                Attachment = AttachmentPoint.BottomRight,
                Location = new Point3d(pt1.X + .002 * ScaleNumber, pt1.Y, 0),
                TextHeight = .004 * ScaleNumber,
                Rotation = -Math.PI / 2
            };
            Mtexts.Add(titreAr, Settings.Default.Txa);

            var pt2 = InnerRectangle.GetPoint3dAt(3).Add((InnerRectangle.GetPoint3dAt(2) - InnerRectangle.GetPoint3dAt(3)) / 2);
            var nord = new MText
            {
                Contents = "الشمال    ",
                Attachment = AttachmentPoint.TopLeft,
                Location = new Point3d(pt2.X, pt2.Y - .004 * ScaleNumber, 0),
                TextHeight = .004 * ScaleNumber
            };
            Mtexts.Add(nord, Settings.Default.Txa);

            var nordar = new MText
            {
                Contents = "NORD      ",
                Attachment = AttachmentPoint.TopRight,
                Location = new Point3d(pt2.X, pt2.Y - .005 * ScaleNumber, 0),
                TextHeight = .003 * ScaleNumber
            };
            Mtexts.Add(nordar, Settings.Default.Txa);

            var pt3 = InnerRectangle.GetPoint3dAt(0);
            ReqAr = new MText
            {
                Contents = " : مطلب  عدد",
                Attachment = AttachmentPoint.BottomRight,
                Location = new Point3d(pt3.X - 0.006 * ScaleNumber, pt3.Y, 0),
                TextHeight = .003 * ScaleNumber,
                Rotation = -Math.PI / 2
            };
            Mtexts.Add(ReqAr, Settings.Default.Txa);
        }

        private void SetRiverains()
        {
            Riverains = new List<MText>();
            foreach (var entry in RiverainsParcels)
            {
                var parcel = entry.Key;
                var riverains = entry.Value;

                riverains.ForEach(riverain =>
                {
                    var shared = Functions.OrderPoints(riverain.Points.Intersect(parcel.Points).ToList(), riverain);
                    var text = CreateTextForRiverains(riverain, shared);
                    if (text != null)
                        Riverains.Add(text);
                });
            }
        }

        private void SetLimites()
        {
            Limits = new List<Line>();
            Property.Parcels.ForEach(p =>
            {
                for (var i = 0; i < p.Points.Count; i++)
                {
                    var line = new Line(p.Points[i].P3D, p.Points[(i + 1) % p.Points.Count].P3D);
                    line.Color = Color.FromColor(System.Drawing.Color.Red);
                    Limits.Add(line);
                }

            });
        }

        public void SetBlocks(BlockTable blockTable)
        {
            var brId = blockTable[Settings.Default.BlkRect];
            Blocks = new List<BlockReference>();
            Property.Parcels.ForEach(p => p.Points.ForEach(t =>
                {
                    if (!t.Name.StartsWith("B")) return;
                    Blocks.Add(new BlockReference(t.P3D, brId)
                    {
                        ScaleFactors = new Scale3d(ScaleNumber / 2000.0)
                    });
                }));

            var added = new HashSet<Point>();
            foreach (var entry in Piste)
            {
                var parcel = entry.Key;
                var points = entry.Value;

                Array.ForEach(points, p =>
                {
                    var point = parcel.Points.FirstOrDefault(pt => pt.P3D.DistanceTo(p) < 0.001);
                    if (point == null || added.Contains(point) || !point.Name.StartsWith("B")) return;

                    Blocks.Add(new BlockReference(p, brId)
                    {
                        ScaleFactors = new Scale3d(ScaleNumber / 2000.0)
                    });
                    added.Add(point);
                });
            }
        }

        private void SetAreaTable()
        {
            AreaTable = new List<Entity>();

            if (Property.Parcels.Count == 1)
            {
                var area = Property.Parcels.Sum(p => p.Contenance) / 10000.0;
                var ha = (int)Math.Floor(area);
                var a = (int)Math.Floor((area - ha) * 100);
                var ca = (int)Math.Round(((area - ha) * 100 - a) * 100);

                var pt1 = InnerRectangle.GetPoint3dAt(0).Add((InnerRectangle.GetPoint3dAt(1) - InnerRectangle.GetPoint3dAt(0)) / 2);

                var super = new MText
                {
                    Contents = ha == 0
                        ? String.Format("{0:D2}  {1:D2}", a, ca)
                        : String.Format("{0:D2}  {1:D2}  {2:D2}", ha, a, ca),
                    Attachment = AttachmentPoint.BottomCenter,
                    Location = new Point3d(pt1.X, pt1.Y + .02 * ScaleNumber, 0),
                    TextHeight = .003 * ScaleNumber
                };
                Mtexts.Add(super, Settings.Default.Ctnc);

                var super1 = new MText
                {
                    Contents = ha == 0 ? "    A   Ca" : "   Ha  A   Ca",
                    Attachment = AttachmentPoint.BottomCenter,
                    Location = new Point3d(pt1.X, pt1.Y + .022 * ScaleNumber, 0),
                    TextHeight = .003 * ScaleNumber
                };
                Mtexts.Add(super1, Settings.Default.Ctnc);

                var cont = new MText
                {
                    Contents = "Contenance",
                    Attachment = AttachmentPoint.BottomLeft,
                    Location = new Point3d(pt1.X - 0.05 * ScaleNumber, pt1.Y + .02 * ScaleNumber, 0),
                    TextHeight = .003 * ScaleNumber
                };
                Mtexts.Add(cont, Settings.Default.Ctnc);

                var cont1 = new MText
                {
                    Contents = "مساحة",
                    Attachment = AttachmentPoint.BottomRight,
                    Location = new Point3d(pt1.X + 0.04 * ScaleNumber, pt1.Y + .019 * ScaleNumber, 0),
                    TextHeight = .004 * ScaleNumber
                };
                Mtexts.Add(cont1, Settings.Default.Txa);
            }
            else
            {
                var atotal = Property.Parcels.Sum(p => p.Contenance) / 10000.0;

                var tHa = (int)Math.Floor(atotal);
                var tA = (int)Math.Floor((atotal - tHa) * 100);
                var tCa = (int)Math.Round(((atotal - tHa) * 100 - tA) * 100);

                var width = tHa == 0 ? 0.025 * ScaleNumber : 0.035 * ScaleNumber;
                var lenth1 = 0.005 * ScaleNumber;
                var lenth2 = 0.007 * ScaleNumber;

                var grip = InnerRectangle.GetPoint3dAt(0).Add(new Vector3d(0.03 * ScaleNumber, 0.03 * ScaleNumber, 0));

                var l = new Line(grip.Add(new Vector3d(0, 0, 0)),
                    grip.Add(new Vector3d(0, -lenth1 - lenth2, 0)));
                AreaTable.Add(l);

                var tt = new MText
                {
                    Contents = "Total",
                    Attachment = AttachmentPoint.BottomCenter,
                    Location = grip.Add(new Vector3d(width / 2, -.004 * ScaleNumber, 0)),
                    TextHeight = .003 * ScaleNumber
                };
                Mtexts.Add(tt, Settings.Default.Ctnc);

                var tot = new MText
                {
                    Contents = tHa == 0
                        ? String.Format("{0:D2}  {1:D2}", tA, tCa)
                        : String.Format("{0:D2}  {1:D2}  {2:D2}", tHa, tA, tCa),
                    Attachment = AttachmentPoint.BottomCenter,
                    Location =
                        grip.Add(new Vector3d(width / 2 - .003 * ScaleNumber, -lenth1 - lenth2 + .001 * ScaleNumber, 0)),
                    TextHeight = .0025 * ScaleNumber
                };
                Mtexts.Add(tot, Settings.Default.Ctnc);

                var tot1 = new MText
                {
                    Contents = tHa == 0 ? "    A   Ca" : "   Ha  A   Ca",
                    Attachment = AttachmentPoint.BottomCenter,
                    Location =
                        grip.Add(new Vector3d(width / 2 - .003 * ScaleNumber, -lenth1 - lenth2 + .002 * ScaleNumber, 0)),
                    TextHeight = .0025 * ScaleNumber
                };
                Mtexts.Add(tot1, Settings.Default.Ctnc);

                foreach (var plle in Property.Parcels)
                {
                    var area = plle.Contenance / 10000.0;
                    var ha = (int)Math.Floor(area);
                    var a = (int)Math.Floor((area - ha) * 100);
                    var ca = (int)Math.Round(((area - ha) * 100 - a) * 100);

                    var w = ha == 0 ? 0.03 * ScaleNumber : 0.04 * ScaleNumber;

                    var contt = new MText
                    {
                        Contents = "P." + plle.Part,
                        Attachment = AttachmentPoint.BottomCenter,
                        Location = grip.Add(new Vector3d(width + w / 2, -.004 * ScaleNumber, 0)),
                        TextHeight = .0025 * ScaleNumber
                    };
                    Mtexts.Add(contt, Settings.Default.Ctnc);

                    var l1 = new Line(grip.Add(new Vector3d(width, 0, 0)),
                        grip.Add(new Vector3d(width, -lenth1 - lenth2, 0)));
                    AreaTable.Add(l1);

                    var super = new MText
                    {
                        Contents = ha == 0
                            ? String.Format("{0:D2}  {1:D2}", a, ca)
                            : String.Format("{0:D2}  {1:D2}  {2:D2}", ha, a, ca),
                        Attachment = AttachmentPoint.BottomCenter,
                        Location =
                            grip.Add(new Vector3d(width + w / 2.0 - .003 * ScaleNumber, -lenth1 - lenth2 + .001 * ScaleNumber,
                                0)),
                        TextHeight = .0025 * ScaleNumber
                    };
                    Mtexts.Add(super, Settings.Default.Ctnc);

                    var super1 = new MText
                    {
                        Contents = ha == 0 ? "    A   Ca" : "   Ha  A   Ca",
                        Attachment = AttachmentPoint.BottomCenter,
                        Location =
                            grip.Add(new Vector3d(width + w / 2.0 - .003 * ScaleNumber, -lenth1 - lenth2 + .002 * ScaleNumber,
                                0)),
                        TextHeight = .0025 * ScaleNumber
                    };
                    Mtexts.Add(super1, Settings.Default.Ctnc);

                    width += w;
                }

                var l2 = new Line(grip.Add(new Vector3d(width, 0, 0)),
                    grip.Add(new Vector3d(width, -lenth1 - lenth2, 0)));
                AreaTable.Add(l2);

                var up = new Line(grip, grip.Add(new Vector3d(width, 0, 0)));
                var mid = new Line(grip.Add(new Vector3d(0, -lenth1, 0)), grip.Add(new Vector3d(width, -lenth1, 0)));
                var down = new Line(grip.Add(new Vector3d(0, -lenth1 - lenth2, 0)), grip.Add(new Vector3d(width, -lenth1 - lenth2, 0)));

                AreaTable.Add(up);
                AreaTable.Add(mid);
                AreaTable.Add(down);

                var cont = new MText
                {
                    Contents = "TABLEAU DES CONTENANCES",
                    Attachment = AttachmentPoint.BottomRight,
                    Location = grip.Add(new Vector3d(width / 2 + .01 * ScaleNumber, .005 * ScaleNumber, 0)),
                    TextHeight = .003 * ScaleNumber
                };
                Mtexts.Add(cont, Settings.Default.Ctnc);

                var cont1 = new MText
                {
                    Contents = "جدول المساحات",
                    Attachment = AttachmentPoint.BottomLeft,
                    Location =
                        grip.Add(
                            new Vector3d(
                        width / 2 + .015 * ScaleNumber, .00375 * ScaleNumber, 0)),
                    TextHeight = .003 * ScaleNumber
                };
                Mtexts.Add(cont1, Settings.Default.Txa);

            }
        }

        private IEnumerable<VersBorne> GetVersBornes()
        {
            var versbornes = new List<VersBorne>();
            foreach (var entry in RiverainsParcels)
            {
                var parcel = entry.Key;
                var points = parcel.Points;
                var riverains = entry.Value;

                riverains.ForEach(riverain =>
                {
                    var shared = Functions.OrderPoints(riverain.Points.Intersect(parcel.Points).ToList(), parcel);

                    var first = shared.First();
                    var last = shared.Last();

                    var prev = points.Contains(riverain.GetNext(first)) ? riverain.GetPrevious(first) : riverain.GetNext(first);
                    var next = points.Contains(riverain.GetPrevious(last)) ? riverain.GetNext(last) : riverain.GetPrevious(last);

                    var prevBorne = prev.Equals(riverain.GetNext(first))
                        ? riverain.GetNextBorne(first)
                        : riverain.GetPreviousBorne(first);

                    var nextBorne = next.Equals(riverain.GetNext(last))
                        ? riverain.GetNextBorne(last)
                        : riverain.GetPreviousBorne(last);

                    if (!parcel.Points.Contains(prev))
                        versbornes.Add(new VersBorne
                        {
                            Name = !"TR".Contains(parcel.PropertyId[0])
                            ? prevBorne.Name
                            : "(" + prevBorne.Name + ")",
                            Start = first.P3D,
                            Vector = prev.P3D - first.P3D
                        });

                    if (!parcel.Points.Contains(next))
                        versbornes.Add(new VersBorne
                        {
                            Name = !"TR".Contains(parcel.PropertyId[0])
                            ? nextBorne.Name
                            : "(" + nextBorne.Name + ")",
                            Start = last.P3D,
                            Vector = next.P3D - last.P3D
                        });
                });
            }

            foreach (var entry in Piste)
            {
                var parcel = entry.Key;
                var points = entry.Value;

                var firstP3D = points.First();
                var lastP3D = points.Last();

                var first = parcel.Points.First(p => p.P3D == firstP3D);
                var last = parcel.Points.First(p => p.P3D == lastP3D);

                var prev = points.Contains(parcel.GetNext(first).P3D)
                    ? parcel.GetPrevious(first)
                    : parcel.GetNext(first);

                var next = points.Contains(parcel.GetPrevious(last).P3D)
                    ? parcel.GetNext(last)
                    : parcel.GetPrevious(last);

                var prevBorne = prev.Equals(parcel.GetNext(first))
                    ? parcel.GetNextBorne(first)
                    : parcel.GetPreviousBorne(first);

                var nextBorne = next.Equals(parcel.GetNext(last))
                    ? parcel.GetNextBorne(last)
                    : parcel.GetPreviousBorne(last);

                if (!Property.Parcels.Any(p => p.Points.Any(pt => pt.Equals(prevBorne))))
                    versbornes.Add(new VersBorne
                    {
                        Name = !"TR".Contains(parcel.PropertyId[0])
                        ? prevBorne.Name
                        : "(" + prevBorne.Name + ")",
                        Start = first.P3D,
                        Vector = prev.P3D - first.P3D
                    });
                if (!Property.Parcels.Any(p => p.Points.Any(pt => pt.Equals(nextBorne))))
                    versbornes.Add(new VersBorne
                    {
                        Name = !"TR".Contains(parcel.PropertyId[0])
                        ? nextBorne.Name
                        : "(" + nextBorne.Name + ")",
                        Start = last.P3D,
                        Vector = next.P3D - last.P3D
                    });
            }

            return versbornes;
        }

        private void SetVersBornes()
        {
            VersBornes = new List<Entity>();
            var versbornes = GetVersBornes().Distinct().ToList();

            var points = new List<Point3d>();
            versbornes.ForEach(v => { if (!points.Contains(v.Start)) points.Add(v.Start); });

            foreach (var p in points)
            {
                var candidates = versbornes.Where(v => v.Start.DistanceTo(p) < 0.2).ToList();
                if (candidates.Count == 0) continue;

                for (var i = 0; i < candidates.Count; i++)
                {
                    var candidate1 = candidates[i];
                    var rotation = Vector3d.XAxis.GetAngleTo(candidate1.Vector, Vector3d.ZAxis);
                    candidates.Remove(candidate1);
                    i--;
                    var candidate2 = candidates.FirstOrDefault(c =>
                        c.Vector.GetAngleTo(candidate1.Vector, Vector3d.ZAxis) < Math.PI / 90);
                    if (candidate2 != null)
                    {
                        candidates.Remove(candidate2);
                        i++;
                        VersBornes.Add(new Line(p, p + 0.013 * ScaleNumber * candidate1.Vector /
                            candidate1.Vector.Length));
                        VersBornes.Add(new MText
                        {
                            Contents = "v." + candidate1.Name + "\nv." + candidate2.Name,
                            Rotation = Math.Cos(rotation) >= 0 ? rotation : rotation - Math.PI,
                            Attachment = AttachmentPoint.MiddleCenter,
                            Location = p + (.013 + .6 * Settings.Default.CqTextHeightVbrn * candidate1.Name.Length)
                                * ScaleNumber * candidate1.Vector / candidate1.Vector.Length,
                            TextHeight = Settings.Default.CqTextHeightVbrn * ScaleNumber,
                        });
                    }
                    else
                    {
                        VersBornes.Add(new Line(p, p + 0.013 * ScaleNumber * candidate1.Vector /
                            candidate1.Vector.Length));
                        VersBornes.Add(new MText
                        {
                            Contents = "v." + candidate1.Name,
                            Rotation = Math.Cos(rotation) >= 0 ? rotation : rotation - Math.PI,
                            Attachment = AttachmentPoint.MiddleCenter,
                            Location = p + (.013 + .6 * Settings.Default.CqTextHeightVbrn * candidate1.Name.Length)
                                * ScaleNumber * candidate1.Vector / candidate1.Vector.Length,
                            TextHeight = Settings.Default.CqTextHeightVbrn * ScaleNumber,
                        });
                    }
                }
            }
        }

        private void SetBornes()
        {
            Bornes = new List<MText>();
            Property.Parcels.ForEach(parcel =>
            {
                parcel.Points.ForEach(point =>
                {
                    if (!point.Name.StartsWith("B")) return;
                    Bornes.Add(Functions.CreateTextForBornes(point, parcel, Settings.Default.CqTextHeightBrn, ScaleNumber));
                });

                RiverainsParcels[parcel]
                    .Where(p => "TR".Contains(p.PropertyId[0]))
                    .ToList()
                    .ForEach(riverain => riverain.Points
                             .Where(p1 => parcel.Points.Any(p2 => p2.P2D.GetDistanceTo(p1.P2D) <= .2))
                             .ToList()
                             .ForEach(point => Bornes.
                                      Add(Functions.CreateTextForBornes(point, riverain, Settings.Default.CqTextHeightBrn, ScaleNumber))));
            });

            var added = new HashSet<Point>();
            foreach (var entry in Piste)
            {
                var parcel = entry.Key;
                var points = entry.Value;

                Array.ForEach(points, p =>
                {
                    var point = parcel.Points.FirstOrDefault(pt => pt.P3D.DistanceTo(p) < 0.001);
                    if (point == null || added.Contains(point) || !point.Name.StartsWith("B")) return;

                    Bornes.Add(Functions.CreateTextForBornes(point, parcel, Settings.Default.CqTextHeightBrn, ScaleNumber));
                    added.Add(point);
                });
            }
        }

        private void SetCroisions()
        {
            Croisions = new List<Line>();

            var dix = (int)(0.10 * ScaleNumber);

            var ymin = InnerRectangle.GetPoint2dAt(0).Y;
            var ymax = InnerRectangle.GetPoint2dAt(2).Y;
            var xmin = InnerRectangle.GetPoint2dAt(0).X;
            var xmax = InnerRectangle.GetPoint2dAt(2).X;

            var starty = (int)Math.Floor(ymin / 100);

            starty *= 100;
            while (starty < ymin)
            {
                starty += dix;
            }

            var startx = (int)Math.Floor(xmin / 100);
            startx *= 100;
            while (startx < xmin)
            {
                startx += dix;
            }

            for (var i = startx; i < xmax; i += dix)
            {
                double y = starty;
                while (y < ymax)
                {
                    if (Property.Parcels.Any(p => Functions.PointInPolygon(p.GetP3DCollection(), new Point3d(i, y, 0))))
                    {
                        y += dix;
                        continue;
                    }
                    Croisions.Add(new Line(new Point3d(i, y - .001 * ScaleNumber, 0),
                        new Point3d(i, y + .001 * ScaleNumber, 0)));
                    Croisions.Add(new Line(new Point3d(i - .001 * ScaleNumber, y, 0),
                        new Point3d(i + .001 * ScaleNumber, y, 0)));

                    y += dix;
                }
            }
        }

        private void SetCoordinates()
        {
            Coordinates = new List<Entity>();
            var dix = (int)(0.10 * ScaleNumber);

            var ymin = InnerRectangle.GetPoint2dAt(0).Y;
            var ymax = InnerRectangle.GetPoint2dAt(2).Y;
            var xmin = InnerRectangle.GetPoint2dAt(0).X;
            var xmax = InnerRectangle.GetPoint2dAt(2).X;

            var starty = (int)Math.Floor(ymin / 100);

            starty *= 100;
            while (starty < ymin)
            {
                starty += dix;
            }

            var startx = (int)Math.Floor(xmin / 100);

            startx *= 100;
            while (startx < xmin)
            {
                startx += dix;
            }

            var vec1 = new Vector3d(0, 0.0025 * ScaleNumber, 0);
            var vec2 = new Vector3d(0, -0.0025 * ScaleNumber, 0);

            for (var i = startx; i < xmax; i += dix)
            {
                Coordinates.Add(new Line(new Point3d(i, ymin, 0), new Point3d(i, ymin, 0).Add(vec1)));
                Coordinates.Add(new Line(new Point3d(i, ymax, 0), new Point3d(i, ymax, 0).Add(vec2)));

                Coordinates.Add(new MText
                {
                    Contents = "" + i,
                    Attachment = AttachmentPoint.MiddleLeft,
                    Location = new Point3d(i, ymin + 0.0035 * ScaleNumber, 0),
                    Rotation = Math.PI / 2,
                    TextHeight = Settings.Default.CqTextHeightCoord * ScaleNumber
                });
            }

            var vec3 = new Vector3d(0.0025 * ScaleNumber, 0, 0);
            var vec4 = new Vector3d(-0.0025 * ScaleNumber, 0, 0);

            for (var i = starty; i < ymax; i += dix)
            {
                var l1 = new Line(new Point3d(xmin, i, 0), new Point3d(xmin, i, 0).Add(vec3));
                var l2 = new Line(new Point3d(xmax, i, 0), new Point3d(xmax, i, 0).Add(vec4));

                Coordinates.Add(l1);
                Coordinates.Add(l2);

                Coordinates.Add(new MText
                {
                    Contents = "" + i,
                    Attachment = AttachmentPoint.MiddleRight,
                    Location = new Point3d(xmax - 0.0035 * ScaleNumber, i, 0),
                    TextHeight = Settings.Default.CqTextHeightCoord * ScaleNumber
                });
            }
        }

        private void SetRectangles()
        {
            InnerRectangle = new Polyline();

            InnerRectangle.AddVertexAt(0, Point2d.Origin, 0, 0, 0);
            InnerRectangle.AddVertexAt(1, new Point2d(Format.Width * ScaleNumber, 0), 0, 0, 0);
            InnerRectangle.AddVertexAt(2, new Point2d(Format.Width * ScaleNumber, Format.Height * ScaleNumber), 0, 0, 0);
            InnerRectangle.AddVertexAt(3, new Point2d(0, Format.Height * ScaleNumber), 0, 0, 0);
            InnerRectangle.Closed = true;

            var ymax = InnerRectangle.GetPoint2dAt(2).Y;
            var xmax = InnerRectangle.GetPoint2dAt(2).X;

            var center = new Point3d(xmax / 2, ymax / 2, 0);
            var centeroid = new Point3d(ConvexHull.Centroid.X, ConvexHull.Centroid.Y, 0);
            var vec = centeroid - center;

            InnerRectangle.TransformBy(Matrix3d.Displacement(vec));

            OuterRectangle = new Polyline();

            OuterRectangle.AddVertexAt(0, new Point2d(-.015 * ScaleNumber, -.025 * ScaleNumber), 0, 0, 0);
            OuterRectangle.AddVertexAt(1, new Point2d((Format.Width + .025) * ScaleNumber, -.025 * ScaleNumber), 0, 0, 0);
            OuterRectangle.AddVertexAt(2, new Point2d((Format.Width + .025) * ScaleNumber, (Format.Height + .025) * ScaleNumber), 0, 0, 0);
            OuterRectangle.AddVertexAt(3, new Point2d(-.015 * ScaleNumber, (Format.Height + .025) * ScaleNumber), 0, 0, 0);
            OuterRectangle.Closed = true;

            OuterRectangle.TransformBy(Matrix3d.Displacement(vec));

            Grip = OuterRectangle.GetPoint3dAt(0);
        }

        public void SetTextStyles(TextStyleTable styleTable)
        {
            Bornes.ForEach(e => e.TextStyleId = styleTable["CQ_" + Settings.Default.BrnId]);
            Coordinates.ForEach(e =>
            {
                if (e.GetRXClass().DxfName != "MTEXT") return;
                var text = (MText)e;
                text.TextStyleId = styleTable["CQ_" + Settings.Default.Coord];
            });
            Riverains.ForEach(e => e.TextStyleId = styleTable["CQ_" + Settings.Default.Riv]);
            foreach (var entry in Mtexts)
            {
                var text = entry.Key;
                var value = entry.Value;

                text.TextStyleId = styleTable["CQ_" + value];
            }

            VersBornes.ForEach(e =>
            {
                if (e.GetRXClass().DxfName != "MTEXT") return;
                var text = (MText)e;
                text.TextStyleId = styleTable["CQ_" + Settings.Default.Vbr];
            });
        }

        public void ToBlockTableRecord(Transaction transaction, BlockTable blockTable, BlockTableRecord modelSpace,
            LinetypeTable linetypeTable, RegAppTable regAppTable, Database database)
        {
            OuterRectangle.SetPropertyId(Property.PropertyId, regAppTable, transaction);
            PditeFr.SetString("PditeFr", regAppTable, transaction);
            PditeAr.SetString("PditeAr", regAppTable, transaction);
            ReqFr.SetString("ReqFr", regAppTable, transaction);
            ReqAr.SetString("ReqAr", regAppTable, transaction);

            var id = Property.PropertyId.Replace("/", "_");
            SymbolUtilityServices.ValidateSymbolName(id, false);

            var btRecord = new BlockTableRecord
            {
                Name = id,
                Explodable = true,
                BlockScaling = BlockScaling.Uniform,
                Origin = Grip
            };
            blockTable.Add(btRecord);
            transaction.AddNewlyCreatedDBObject(btRecord, true);

            Database.Wblock(database, ObjectIdDetails, Point3d.Origin, DuplicateRecordCloning.Ignore);
            ObjectIdDetails.Clear();
            foreach (var oId in modelSpace)
            {
                ObjectIdDetails.Add(oId);
            }
            database.DeepCloneObjects(ObjectIdDetails, btRecord.ObjectId, new IdMapping(), false);
            foreach (ObjectId oId in ObjectIdDetails)
            {
                var e = (Entity)transaction.GetObject(oId, OpenMode.ForWrite);
                e.Erase();
                e.Dispose();
            }

            EntityDetails.Keys.ToList().ForEach(e =>
                {
                    btRecord.AppendEntity(e);
                    transaction.AddNewlyCreatedDBObject(e, true);
                    var name = EntityDetails[e];
                    if (!string.IsNullOrEmpty(name)) e.LinetypeId = linetypeTable[name];
                });
            Bornes.ForEach(e => { btRecord.AppendEntity(e); transaction.AddNewlyCreatedDBObject(e, true); });
            Mtexts.Keys.ToList().ForEach(e => { btRecord.AppendEntity(e); transaction.AddNewlyCreatedDBObject(e, true); });
            Limits.ForEach(e => { btRecord.AppendEntity(e); transaction.AddNewlyCreatedDBObject(e, true); });
            VersBornes.ForEach(e => { btRecord.AppendEntity(e); transaction.AddNewlyCreatedDBObject(e, true); });
            Riverains.ForEach(e => { btRecord.AppendEntity(e); transaction.AddNewlyCreatedDBObject(e, true); });
            Coordinates.ForEach(e => { btRecord.AppendEntity(e); transaction.AddNewlyCreatedDBObject(e, true); });
            Croisions.ForEach(e => { btRecord.AppendEntity(e); transaction.AddNewlyCreatedDBObject(e, true); });
            AreaTable.ForEach(e => { btRecord.AppendEntity(e); transaction.AddNewlyCreatedDBObject(e, true); });
            Blocks.ForEach(e => { btRecord.AppendEntity(e); transaction.AddNewlyCreatedDBObject(e, true); });
            Array.ForEach(new[] { InnerRectangle, OuterRectangle },
                e => { btRecord.AppendEntity(e); transaction.AddNewlyCreatedDBObject(e, true); });

        }

        public void SetLayers(LayerTable layerTable)
        {
            Limits.ForEach(e => e.LayerId = layerTable[Settings.Default.Lim]);
            Blocks.ForEach(e => e.LayerId = layerTable[Settings.Default.Blk]);
            Bornes.ForEach(e => e.LayerId = layerTable[Settings.Default.BrnId]);
            EntityDetails.Keys.ToList().ForEach(e => e.LayerId = layerTable[Settings.Default.Dtl]);
            Coordinates.ForEach(e => e.LayerId = layerTable[Settings.Default.Frm]);
            Croisions.ForEach(e => e.LayerId = layerTable[Settings.Default.Frm]);
            Riverains.ForEach(e => e.LayerId = layerTable[Settings.Default.Riv]);
            Mtexts.Keys.ToList().ForEach(e => e.LayerId = layerTable[Settings.Default.Txt]);
            AreaTable.ForEach(e => e.LayerId = layerTable[Settings.Default.Txt]);
            InnerRectangle.LayerId = layerTable[Settings.Default.Frm];
            OuterRectangle.LayerId = layerTable[Settings.Default.Frm];
            PditeFr.LayerId = layerTable[Settings.Default.Frm];
            PditeAr.LayerId = layerTable[Settings.Default.Frm];
            ReqFr.LayerId = layerTable[Settings.Default.Frm];
            ReqAr.LayerId = layerTable[Settings.Default.Frm];
            VersBornes.ForEach(e => e.LayerId = layerTable[Settings.Default.Vbr]);
        }

        private MText CreateTextForRiverains(Parcel riverain, List<Point> shared)
        {
            const char suffix = '-';

            var contents = riverain.PropertyId + suffix + "P" + riverain.Part;
            contents = char.IsDigit(contents[0]) ? "Plle " + contents : contents;

            double rotation;
            Point3d location;
            const AttachmentPoint attachment = AttachmentPoint.MiddleCenter;
            var width = .9 * contents.Length * Settings.Default.CqTextHeightRiv * ScaleNumber;
            var height = 2.5 * Settings.Default.CqTextHeightRiv * ScaleNumber;

            Point first;
            Point last;

            switch (shared.Count)
            {
                case 0:
                    return null;
                case 1:
                    first = shared.First();
                    last = shared.Last();
                    var back = riverain.GetPrevious(first);
                    var forward = riverain.GetNext(last);
                    var tanback = Math.Atan((back.P3D.Y - first.P3D.Y) / (back.P3D.X - first.P3D.X));
                    var tanforward = Math.Atan((forward.P3D.Y - last.P3D.Y) / (forward.P3D.X - last.P3D.X));

                    var chosen = tanback > tanforward ? back.P3D - first.P3D : forward.P3D - last.P3D;
                    chosen /= chosen.Length;

                    var sign = tanback > tanforward ? 1 : -1;

                    location = first.P3D
                        + (.005 * ScaleNumber + width / 2) * chosen
                        + (.005 * ScaleNumber + height / 2) * chosen.RotateBy(sign * Math.PI / 2.0, Vector3d.ZAxis);
                    rotation = Vector3d.XAxis.GetAngleTo(chosen, Vector3d.ZAxis);

                    break;
                default:
                    first = shared.First();
                    last = shared.Last();
                    var line1 = Functions.GetLonguestLine(shared, height / 2.0);

                    var delta = line1.Delta;

                    var length = delta.Length;
                    location = line1.EndPoint - .5 * delta;
                    Vector3d vector;

                    if (length < width)
                    {
                        var prev = riverain.GetPrevious(first).P3D - first.P3D;
                        var next = riverain.GetNext(last).P3D - last.P3D;
                        vector = prev.Length > next.Length ? prev / prev.Length :
                            next / next.Length;
                        rotation = Vector3d.XAxis.GetAngleTo(vector, Vector3d.ZAxis);

                        var s = prev.Length > next.Length ? 1 : -1;

                        location += .005 * ScaleNumber * vector;
                        location += width / 2.0 * vector;

                        var d = location.DistanceTo(line1.GetClosestPointTo(location, true));
                        location = d < height
                            ? location.Add(0.01 * ScaleNumber * vector.RotateBy(s * Math.PI / 2.0, Vector3d.ZAxis))
                            : location;
                    }
                    else
                    {
                        vector = delta.RotateBy(-Math.PI / 2.0, Vector3d.ZAxis) / length;
                        rotation = Vector3d.XAxis.GetAngleTo(delta, Vector3d.ZAxis);

                        location += .005 * ScaleNumber * vector;

                        var tan = Math.Tan(rotation);
                        if (Math.Pow(tan, 2.0) >= 1.0)
                        {
                            location += width / 2.0 * vector;
                            rotation += Math.PI / 2.0;
                        }
                        else
                        {
                            location += height * 1.5 * vector;
                        }
                    }

                    line1.Dispose();
                    break;
            }

            return new MText
            {
                Attachment = attachment,
                Location = location,
                Rotation = Math.Cos(rotation) <= 0 ? rotation + Math.PI : rotation,
                Contents = contents.EndsWith("P0")
                    ? contents.Remove(contents.IndexOf(suffix)) + "\nT.C"
                    : contents + "\nT.C",
                TextHeight = Settings.Default.CqTextHeightRiv * ScaleNumber,
            };
        }

        public void AddTextForPiste()
        {
            if (Piste.Count == 0) return;
            var dict = Property.Parcels.ToDictionary(p => p,
                p => Functions.GetFreePoints(p, RiverainsParcels[p]))
                .OrderByDescending(e => e.Value.Count)
                .ToList();

            var parcel = dict[0].Key;
            var freePoints = dict[0].Value;

            var limites = Functions.GetPisteWidth(parcel, Property, Piste, RiverainsParcels, freePoints);

            foreach (var limite in limites)
            {
                var contents = string.Empty;
                if (limite.Points.Any(p => p.Name.StartsWith("P")))
                {
                    contents = string.Format("Piste Publique de {0:F2}m", limite.Width);
                }
                else if (limite.Points.Any(p => p.Name.StartsWith("O")))
                {
                    contents = "Oued";
                }
                else if (limite.Points.Any(p => p.Name.StartsWith("C")))
                {
                    contents = "Chaàbat";
                }
                else if (limite.Points.Any(p => p.Name.StartsWith("R")))
                {
                    contents = string.Format("Route de {0:F2}m", limite.Width);
                }

                var n = limite.Points.Length;
                var p1 = limite.Points[n / 2].P3D;
                var p2 = limite.Points[n / 2 - 1].P3D;
                var vector = p2 - p1;
                var mid = n % 2 == 1
                    ? p1
                    : p1.Add(vector / 2);

                var height = .002 * ScaleNumber;

                var half = limite.Width == 0
                    ? 1.5 * height
                    : limite.Width / 2.0;

                var rotation = Vector3d.XAxis.GetAngleTo(p2 - p1, Vector3d.ZAxis);

                var location = mid.Add(half * vector.RotateBy(-Math.PI / 2, Vector3d.ZAxis) / vector.Length);

                Mtexts.Add(new MText
                {
                    Contents = contents,
                    Attachment = AttachmentPoint.MiddleCenter,
                    Rotation = Math.Cos(rotation) <= 0 ? rotation + Math.PI : rotation,
                    Location = location,
                    TextHeight = .002 * ScaleNumber
                }, "ROMANT");
            }
        }

        public void Dispose()
        {
            try
            {
                Bornes.ForEach(e => { if (e != null) e.Dispose(); });
                Mtexts.Keys.ToList().ForEach(e => { if (e != null) e.Dispose(); });
                Limits.ForEach(e => { if (e != null) e.Dispose(); });
                VersBornes.ForEach(e => { if (e != null) e.Dispose(); });
                Riverains.ForEach(e => { if (e != null) e.Dispose(); });
                Coordinates.ForEach(e => { if (e != null) e.Dispose(); });
                Croisions.ForEach(e => { if (e != null) e.Dispose(); });
                AreaTable.ForEach(e => { if (e != null) e.Dispose(); });
                Blocks.ForEach(e => { if (e != null) e.Dispose(); });
                Array.ForEach(new[] { InnerRectangle, OuterRectangle },
                    e => { if (e != null) e.Dispose(); });
            }
            catch
            {
                // ignored
            }
        }

        public MText ReqFr { get; set; }

        public MText ReqAr { get; set; }
    }
}
