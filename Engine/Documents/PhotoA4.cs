﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

using Engine.Application;
using Engine.Data;
using Engine.Data.View;
using Engine.Properties;
using Engine.Utils;
using GeoAPI.Geometries;
using SQLite;
// ReSharper disable PossibleMultipleEnumeration

namespace Engine.Documents
{
    using System.IO;

    class PhotoA4
    {
        public Property Property { get; set; }
        public List<MText> Texts { get; set; }
        public List<Parcel> Parcels { get; set; }
        public Polyline InnerRectangle { get; set; }
        public Polyline MiddleRectangle { get; set; }
        public Polyline OuterRectangle { get; set; }
        public List<Polyline> Limites { get; set; }
        public Transaction Transaction { get; set; }
        public int ScaleNumber { get; set; }
        public ProjectInfo Project { get; set; }
        public bool IsVertical { get; set; }
        public Dictionary<Parcel, MText> PropTexts { get; set; }
        private IGeometry ConvexHull { get; set; }
        private Dictionary<Parcel, Point3d[]> Piste { get; set; }
        public IntPtr RasterPointer { get; set; }
        public RasterImage RasterImage { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public SQLiteConnection SqLite { get; set; }
        public Dictionary<Parcel, List<Parcel>> RiverainsParcels { get; set; }
        public List<Entity> Entities { get; set; }
        public Point3d Grip { get; set; }

        public PhotoA4(Property property, Transaction transaction, ProjectInfo project, SQLiteConnection sqLite,
            IntPtr pointer, RasterImage raster, string scale, string orientation)
        {
            Property = property;
            Transaction = transaction;
            Project = project;
            SqLite = sqLite;
            RasterPointer = pointer;
            RasterImage = raster;

            InnerRectangle = new Polyline();
            MiddleRectangle = new Polyline();
            OuterRectangle = new Polyline();
            Entities = new List<Entity>();

            RiverainsParcels = Functions.GetRiverainsParcels(Property, Functions.GetConvexHull(property), SqLite);
            Piste = Functions.GetPiste(Property, RiverainsParcels, SqLite);
            ConvexHull = Functions.SetConvexHull(Property, Piste);
            SetOrientation(orientation);
            SetScale(scale);
            SetRectangles();

            CropRaster();

            GetPropTexts();
            SetLimitesAndPropTexts();
            SetCoordinates();
            SetCroisions();
            SetText();
        }

        private void SetRectangles()
        {
            InnerRectangle.AddVertexAt(0, Point2d.Origin, 0.0, 0.0, 0.0);
            InnerRectangle.AddVertexAt(1, new Point2d(Width * ScaleNumber, 0.0), 0.0, 0.0, 0.0);
            InnerRectangle.AddVertexAt(2, new Point2d(Width * ScaleNumber, Height * ScaleNumber), 0.0, 0.0, 0.0);
            InnerRectangle.AddVertexAt(3, new Point2d(0.0, Height * ScaleNumber), 0.0, 0.0, 0.0);
            InnerRectangle.Closed = true;

            MiddleRectangle.AddVertexAt(0, new Point2d(-.01 * ScaleNumber, -.01 * ScaleNumber), 0.0, 0.0, 0.0);
            MiddleRectangle.AddVertexAt(1, new Point2d((Width + .01) * ScaleNumber, -.01 * ScaleNumber), 0.0, 0.0, 0.0);
            MiddleRectangle.AddVertexAt(2, new Point2d((Width + .01) * ScaleNumber, (Height + .01) * ScaleNumber), 0.0, 0.0, 0.0);
            MiddleRectangle.AddVertexAt(3, new Point2d(-.01 * ScaleNumber, (Height + .01) * ScaleNumber), 0.0, 0.0, 0.0);
            MiddleRectangle.Closed = true;

            var oWidth = IsVertical ? .21 : .297;
            var oHeight = IsVertical ? .297 : 0.21;

            OuterRectangle.AddVertexAt(0, Point2d.Origin, 0, 0, 0);
            OuterRectangle.AddVertexAt(1, new Point2d(oWidth * ScaleNumber, 0), 0, 0, 0);
            OuterRectangle.AddVertexAt(2, new Point2d(oWidth * ScaleNumber, oHeight * ScaleNumber), 0, 0, 0);
            OuterRectangle.AddVertexAt(3, new Point2d(0, oHeight * ScaleNumber), 0, 0, 0);
            OuterRectangle.Closed = true;

            var center = new Point3d(Width * ScaleNumber, Height * ScaleNumber, 0.0) / 2.0;
            var vector = new Point3d(ConvexHull.Centroid.X, ConvexHull.Centroid.Y, 0.0) - center;

            InnerRectangle.TransformBy(Matrix3d.Displacement(vector));
            MiddleRectangle.TransformBy(Matrix3d.Displacement(vector));

            center = new Point3d(oWidth * ScaleNumber, oHeight * ScaleNumber, 0.0) / 2.0;
            vector = new Point3d(ConvexHull.Centroid.X, ConvexHull.Centroid.Y, 0.0) - center;

            OuterRectangle.TransformBy(Matrix3d.Displacement(vector));
        }

        private void SetText()
        {
            Texts = new List<MText>();
            var max = MiddleRectangle.GetPoint3dAt(2);
            var min = MiddleRectangle.GetPoint3dAt(0);

            var d = IsVertical ? 0.008 : 0.005;

            var propId = new MText
            {
                Contents = "Plle : " + Property.PropertyId,
                Attachment = AttachmentPoint.BottomLeft,
                Location = new Point3d(min.X, max.Y + d * ScaleNumber, 0),
                TextHeight = .004 * ScaleNumber
            };
            Texts.Add(propId);

            var echelle = new MText
            {
                Contents = "Echelle : 1/" + ScaleNumber,
                Attachment = AttachmentPoint.BottomRight,
                Location = new Point3d(max.X, max.Y + d * ScaleNumber, 0),
                TextHeight = .004 * ScaleNumber
            };
            Texts.Add(echelle);

            var map = new MText
            {
                Contents =
                    "Mappe au 1/2000" + " : " + Functions.CalculateAllMappes(Property.Parcels, 2000)[0],
                Attachment = AttachmentPoint.TopRight,
                Location = new Point3d(max.X, min.Y - d * ScaleNumber, 0),
                TextHeight = .004 * ScaleNumber
            };
            Texts.Add(map);

            var ss = new MText
            {
                Contents = "Secteur \"Jabria\" S.S." + Project.SubSector,
                Attachment = AttachmentPoint.BottomCenter,
                Location = new Point3d(min.X + (max.X - min.X) / 2, max.Y + d * ScaleNumber, 0),
                TextHeight = .004 * ScaleNumber
            };
            Texts.Add(ss);
        }

        private void GetPropTexts()
        {
            var oIds = Functions.GetEntitiesFromLayersCrossing(InnerRectangle.GetPoint3DCollection(), Settings.Default.Lim, Settings.Default.Lima);
            var parcels = Controller.GetParcels(oIds, SqLite);

            var txIds = Functions.GetEntitiesFromLayersWindow(OuterRectangle.GetPoint3DCollection(), Settings.Default.Prps);
            var txts = new List<MText>();
            if (txIds != null && txIds.Length > 0)
                Array.ForEach(txIds, id => txts.Add((MText)Transaction.GetObject(id, OpenMode.ForRead)));

            PropTexts = parcels.ToDictionary(p => p, p =>
            {
                var txt = txts.FirstOrDefault(t => t != null && Functions.PointInPolygon(p.GetP3DCollection(), t.Location));
                if (txt != null) txts.Remove(txt);
                return txt;
            });

            txts.ForEach(t => { if (t != null) t.Dispose(); });
        }

        private void SetScale(string scale)
        {
            var width = ConvexHull.Coordinates.Max(c => c.X) - ConvexHull.Coordinates.Min(c => c.X);
            var height = ConvexHull.Coordinates.Max(c => c.Y) - ConvexHull.Coordinates.Min(c => c.Y);

            if (scale != "Automatique")
            {
                ScaleNumber = int.Parse(scale);
                return;
            }

            var scales = new[] { 1000, 1500, 2000, 3000, 4000, 5000, 7500, 10000 };

            foreach (var item in scales)
            {
                ScaleNumber = item;
                if (width <= (Width - .03) * ScaleNumber && height < (Height - .04) * ScaleNumber)
                    return;
            }
        }

        private void SetOrientation(string orientation)
        {
            var width = ConvexHull.Coordinates.Max(c => c.X) - ConvexHull.Coordinates.Min(c => c.X);
            var height = ConvexHull.Coordinates.Max(c => c.Y) - ConvexHull.Coordinates.Min(c => c.Y);

            switch (orientation)
            {
                case "Automatique":
                    IsVertical = height > .45 * width;
                    break;
                case "Portrait":
                    IsVertical = true;
                    break;
                case "Paysage":
                    IsVertical = false;
                    break;
            }

            Width = IsVertical ? 0.18 : 0.26;
            Height = IsVertical ? 0.24 : 0.16;
        }

        private void CropRaster()
        {
            if (RasterImage == null || RasterPointer == IntPtr.Zero)
                return;

            var filename = Project.LivrablesPath + "\\Orthos PNG\\" + Property.PropertyId.Replace("/", "_") + ".png";
            try
            {
                if (File.Exists(filename)) File.Delete(filename);

                var values = Functions.GetCropArea(RasterImage, InnerRectangle);
                Functions.Crop(RasterPointer, filename, values);
            }
            catch (Exception)
            {
                // ignored
            }
        }

        // ReSharper disable once FunctionComplexityOverflow
        private void SetLimitesAndPropTexts()
        {
            Limites = new List<Polyline>();
            var suffix = '-';

            var rectangle = InnerRectangle.GetGeometry();

            var subject = new List<List<IntPoint>> { new List<IntPoint>(4) };
            subject[0].Add(new IntPoint((long)(InnerRectangle.GetPoint2dAt(0).X * 100), (long)(InnerRectangle.GetPoint2dAt(0).Y * 100)));
            subject[0].Add(new IntPoint((long)(InnerRectangle.GetPoint2dAt(1).X * 100), (long)(InnerRectangle.GetPoint2dAt(1).Y * 100)));
            subject[0].Add(new IntPoint((long)(InnerRectangle.GetPoint2dAt(2).X * 100), (long)(InnerRectangle.GetPoint2dAt(2).Y * 100)));
            subject[0].Add(new IntPoint((long)(InnerRectangle.GetPoint2dAt(3).X * 100), (long)(InnerRectangle.GetPoint2dAt(3).Y * 100)));

            foreach (var parcel in PropTexts.Keys.ToList())
            {
                var text = PropTexts[parcel];
                var property = Controller.GetProperty(parcel.PropertyId, SqLite);

                if (!rectangle.Intersects(parcel.GetPolygon()))
                {
                    var limits = parcel.GetPolyline();

                    limits.LineWeight = Property.Parcels.Contains(parcel)
                        ? LineWeight.LineWeight080
                        : LineWeight.LineWeight040;

                    limits.Color = Property.Parcels.Contains(parcel)
                        ? Color.FromColor(System.Drawing.Color.Red)
                        : Color.FromColorIndex(ColorMethod.ByAci, 7);
                    Limites.Add(limits);

                    if (text != null)
                        PropTexts[parcel] = Functions.CopyMtext(text);
                    else
                    {
                        if (string.IsNullOrEmpty(property.OwnersFr))
                            property.OwnersFr = "\n";

                        text = Functions.CreateTextForPropriétaires(parcel.GetPolygon(), property.OwnersFr);

                        var id = parcel.Part == 0 ? parcel.PropertyId : parcel.PropertyId + suffix + "P" + parcel.Part;

                        text.Contents = "{\\C1;" + (id + "}\\P"
                            + "{\\C255;" + text.Contents.Trim("\n".ToCharArray())).Replace("\n\n", "\n") + "}\\P";
                        PropTexts[parcel] = text;
                    }
                    continue;
                }

                var clip = new List<List<IntPoint>>(1) { new List<IntPoint>() };

                for (var i = 0; i < parcel.Points.Count; i++)
                {
                    var p = parcel.Points[i];
                    clip[0].Add(new IntPoint((long)(p.P2D.X * 100.0), (long)(p.P2D.Y * 100.0)));
                }

                var solution = new List<List<IntPoint>>();

                var clipper = new Clipper();
                clipper.AddPaths(subject, PolyType.ptSubject, true);
                clipper.AddPaths(clip, PolyType.ptClip, true);
                clipper.Execute(ClipType.ctIntersection, solution, PolyFillType.pftEvenOdd, PolyFillType.pftEvenOdd);

                if (!solution.Any())
                {
                    PropTexts[parcel] = null;
                    continue;
                }
                
                var n = solution[0].Count;

                var polyline = new Polyline();
                for (var i = 0; i < n; i++)
                {
                    polyline.AddVertexAt(i, new Point2d(solution[0][i].X / 100.0, solution[0][i].Y / 100.0), 0, 0, 0);
                }

                polyline.Closed = true;
                polyline.LineWeight = LineWeight.LineWeight040;
                polyline.Color = Color.FromColorIndex(ColorMethod.ByAci, 7);
                Limites.Add(polyline);

                var polygon = polyline.GetGeometry();

                if (text == null && polyline.Area > 0.0004 * Math.Pow(ScaleNumber, 2.0))
                {
                    if (string.IsNullOrEmpty(property.OwnersFr))
                        property.OwnersFr = "\n";
                    text = Functions.CreateTextForPropriétaires(polygon, property.OwnersFr);
                    var id = parcel.Part == 0 ? parcel.PropertyId : parcel.PropertyId + suffix + "P" + parcel.Part;

                    text.Contents = "{\\C1;" + (id + "}\\P"
                        + "{\\C255;" + text.Contents.Trim("\n".ToCharArray())).Replace("\n\n", "\n") + "}\\P";
                    text.Contents = text.Contents.Replace(" BEN ", "\nBEN ").Replace(" BENT ", "\nBENT ");
                    PropTexts[parcel] = text;
                }

                else if (text != null
                         && (Piste.ContainsKey(parcel) || RiverainsParcels.ContainsKey(parcel)
                             || polyline.Area > 0.001 * Math.Pow(ScaleNumber, 2.0)))
                {
                    var contents = text.Contents.Replace(" BEN ", "\nBEN ").Replace(" BENT ", "\nBENT ");
                    PropTexts[parcel] = Functions.CreateTextForPropriétaires(polygon, contents);
                }

                else if (polyline.Area > 0.0004 * Math.Pow(ScaleNumber, 2.0))
                {
                    var contents = parcel.PropertyId + suffix + "P" + parcel.Part;
                    contents = contents.EndsWith("0") ? contents.Remove(contents.IndexOf(suffix)) : contents;
                    contents = "{\\C1;" + contents + "}\\P";

                    PropTexts[parcel] = Functions.CreateTextForPropriétaires(polygon, contents);
                }
                else
                    PropTexts[parcel] = null;
            }
        }

        public void Dispose()
        {
            Limites.ForEach(e => { if (e != null) e.Dispose(); });
            PropTexts.Values.ToList().ForEach(e => { if (e != null) e.Dispose(); });
            Texts.ForEach(e => { if (e != null) e.Dispose(); });
            Entities.ForEach(e => { if (e != null) e.Dispose(); });
            Array.ForEach(new[] { InnerRectangle, MiddleRectangle, OuterRectangle },
                e => { if (e != null) e.Dispose(); });
        }

        private void SetCoordinates()
        {
            var odd = ScaleNumber % 2 == 0 ? 2 : 1;

            var dix = (int)(0.10 * ScaleNumber);

            var ymin = InnerRectangle.GetPoint2dAt(0).Y;
            var ymax = InnerRectangle.GetPoint2dAt(2).Y;
            var min = InnerRectangle.GetPoint2dAt(0).X;
            var max = InnerRectangle.GetPoint2dAt(2).X;

            var starty = (int)Math.Floor(ymin / 100) % odd == 0 ? (int)Math.Floor(ymin / 100) : (int)Math.Floor(ymin / 100) - 1;

            starty *= 100;
            while (starty < ymin)
            {
                starty += dix;
            }

            var start = (int)Math.Floor(min / 100) % odd == 0 ? (int)Math.Floor(min / 100) : (int)Math.Floor(min / 100) - 1;

            start *= 100;
            while (start < min)
            {
                start += dix;
            }

            var vec1 = new Vector3d(0, 0.002 * ScaleNumber, 0);
            var vec2 = new Vector3d(0, -0.002 * ScaleNumber, 0);

            for (var i = start; i < max; i += dix)
            {
                var l1 = new Line(new Point3d(i, ymin, 0), new Point3d(i, ymin, 0).Add(vec1));
                var l2 = new Line(new Point3d(i, ymax, 0), new Point3d(i, ymax, 0).Add(vec2));
                Entities.Add(l1);
                Entities.Add(l2);

                var text1 = new MText
                {
                    Contents = "" + i,
                    Attachment = AttachmentPoint.MiddleRight,
                    Location = new Point3d(i, ymin - 0.00050 * ScaleNumber, 0),
                    Rotation = Math.PI / 2.0,
                    TextHeight = 0.002 * ScaleNumber
                };
                Entities.Add(text1);

                var text2 = new MText
                {
                    Contents = "" + i,
                    Attachment = AttachmentPoint.MiddleLeft,
                    Location = new Point3d(i, ymax + 0.00050 * ScaleNumber, 0),
                    Rotation = Math.PI / 2.0,
                    TextHeight = 0.002 * ScaleNumber
                };
                Entities.Add(text2);
            }

            var vec3 = new Vector3d(0.002 * ScaleNumber, 0, 0);
            var vec4 = new Vector3d(-0.002 * ScaleNumber, 0, 0);

            for (var i = starty; i < ymax; i += dix)
            {
                var l1 = new Line(new Point3d(min, i, 0), new Point3d(min, i, 0).Add(vec3));
                var l2 = new Line(new Point3d(max, i, 0), new Point3d(max, i, 0).Add(vec4));
                Entities.Add(l1);
                Entities.Add(l2);

                var text1 = new MText
                {
                    Contents = "" + i,
                    Attachment = AttachmentPoint.MiddleLeft,
                    Location = new Point3d(max + 0.00050 * ScaleNumber, i, 0),
                    TextHeight = 0.002 * ScaleNumber
                };
                Entities.Add(text1);

                var text2 = new MText
                {
                    Contents = "" + i,
                    Attachment = AttachmentPoint.MiddleRight,
                    Location = new Point3d(min - 0.00050 * ScaleNumber, i, 0),
                    TextHeight = 0.002 * ScaleNumber
                };
                Entities.Add(text2);
            }
        }

        private void SetCroisions()
        {
            var odd = ScaleNumber % 2 == 0 ? 2 : 1;

            var dix = (int)(0.10 * ScaleNumber);

            var ymin = InnerRectangle.GetPoint2dAt(0).Y;
            var ymax = InnerRectangle.GetPoint2dAt(2).Y;
            var min = InnerRectangle.GetPoint2dAt(0).X;
            var max = InnerRectangle.GetPoint2dAt(2).X;

            var starty = (int)Math.Floor(ymin / 100) % odd == 0 ? (int)Math.Floor(ymin / 100) : (int)Math.Floor(ymin / 100) - 1;

            starty *= 100;
            while (starty < ymin)
            {
                starty += dix;
            }

            var start = (int)Math.Floor(min / 100) % odd == 0 ? (int)Math.Floor(min / 100) : (int)Math.Floor(min / 100) - 1;

            start *= 100;
            while (start < min)
            {
                start += dix;
            }

            for (var i = start; i < max; i += dix)
            {
                double y = starty;
                while (y < ymax)
                {
                    if (Property.Parcels.Any(p => Functions.PointInPolygon(p.GetP3DCollection(), new Point3d(i, y, 0))))
                    {
                        y += dix;
                        continue;
                    }
                    Entities.Add(new Line(new Point3d(i, y - .001 * ScaleNumber, 0),
                        new Point3d(i, y + .001 * ScaleNumber, 0)));
                    Entities.Add(new Line(new Point3d(i - .001 * ScaleNumber, y, 0),
                        new Point3d(i + .001 * ScaleNumber, y, 0)));

                    y += dix;
                }
            }
        }

        public void SetTextStyles(TextStyleTable styleTable)
        {
            PropTexts.Values.ToList().ForEach(e =>
            {
                if (e != null) e.TextStyleId = styleTable["PH_" + Settings.Default.Prps];
            });
            Texts.ForEach(e => e.TextStyleId = styleTable["PH_" + Settings.Default.Txt]);
            Entities.ForEach(e =>
            {
                if (e.GetRXClass().DxfName == "MTEXT") ((MText)e).TextStyleId = styleTable["PH_" + Settings.Default.Txt];
            });
        }

        public void SetLayers(LayerTable layerTable)
        {
            Limites.ForEach(e => e.LayerId = layerTable[Settings.Default.Lim]);
            PropTexts.Values.ToList().ForEach(e => { if (e != null) e.LayerId = layerTable[Settings.Default.Prps]; });
            Texts.ForEach(e => e.LayerId = layerTable[Settings.Default.Txt]);
            Entities.ForEach(e => e.LayerId = layerTable[Settings.Default.Frm]);
            Array.ForEach(new[] { InnerRectangle, OuterRectangle }, e => e.LayerId = layerTable[Settings.Default.Frm]);
        }

        public void ToBlockTableRecord(Transaction transaction, BlockTable blockTable)
        {
            var id = Property.PropertyId.Replace("/","_");
            SymbolUtilityServices.ValidateSymbolName(id, false);
            if (blockTable.Has(id)) return;

            var rotation = IsVertical ? 0.0 : -Math.PI / 2;

            var center = new Point3d(ConvexHull.Centroid.X, ConvexHull.Centroid.Y, 0);
            OuterRectangle.TransformBy(Matrix3d.Rotation(rotation, Vector3d.ZAxis, center));
            Grip = OuterRectangle.GeometricExtents.MinPoint;

            var btRecord = new BlockTableRecord
            {
                Name = id,
                Explodable = true,
                BlockScaling = BlockScaling.Uniform,
                Origin = Grip
            };
            
            blockTable.Add(btRecord);
            transaction.AddNewlyCreatedDBObject(btRecord, true);

            btRecord.AppendEntity(OuterRectangle);
            transaction.AddNewlyCreatedDBObject(OuterRectangle, true);
            Limites.ForEach(e =>
            {
                e.TransformBy(Matrix3d.Rotation(rotation, Vector3d.ZAxis, center));
                btRecord.AppendEntity(e);
                transaction.AddNewlyCreatedDBObject(e, true);
            });
            PropTexts.Values.ToList().ForEach(e =>
            {
                if (e == null) return;
                e.TransformBy(Matrix3d.Rotation(rotation, Vector3d.ZAxis, center));
                btRecord.AppendEntity(e);
                transaction.AddNewlyCreatedDBObject(e, true);
            });
            Texts.ForEach(e =>
            {
                e.TransformBy(Matrix3d.Rotation(rotation, Vector3d.ZAxis, center));
                btRecord.AppendEntity(e);
                transaction.AddNewlyCreatedDBObject(e, true);
            });
            Entities.ForEach(e =>
            {
                e.TransformBy(Matrix3d.Rotation(rotation, Vector3d.ZAxis, center));
                btRecord.AppendEntity(e);
                transaction.AddNewlyCreatedDBObject(e, true);
            });
            Array.ForEach(new[] { MiddleRectangle, InnerRectangle }, e =>
                {
                    e.TransformBy(Matrix3d.Rotation(rotation, Vector3d.ZAxis, center));
                    btRecord.AppendEntity(e);
                    transaction.AddNewlyCreatedDBObject(e, true);
                });
        }
    }
}