﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Engine.Data.View;
using Engine.Properties;
using Engine.Utils;

#pragma warning disable 618

namespace Engine.Documents
{
    public class ParcelScore
    {
        public Parcel Parcel { get; set; }
        public Point Point { get; set; }

        private double _lengthProduct;
        private double _angle;
        public double Score { get; set; }

        public ParcelScore(Parcel parcel, Point point)
        {
            Parcel = parcel;
            Point = point;
            CalculateScore();
        }

        private void CalculateScore()
        {
            var next = Parcel.GetNext(Point);
            var prev = Parcel.GetPrevious(Point);

            var vnext = next.P3D - Point.P3D;
            var vprev = prev.P3D - Point.P3D;

            _lengthProduct = vnext.Length * vprev.Length;
            _angle = vnext.GetAngleTo(vprev, Vector3d.XAxis);

            Score = .7 * _lengthProduct + .3 * _angle;
        }
    }
    public class PlanDeMasse
    {
        public BlockTableRecord ModelSpace { get; set; }
        public Transaction Transaction { get; set; }
        public LayerTable LayerTable { get; set; }
        public TextStyleTable StyleTable { get; set; }

        public PlanDeMasse(Transaction transaction, BlockTableRecord modelSpace, LayerTable layerTable, TextStyleTable styleTable)
        {
            ModelSpace = modelSpace;
            Transaction = transaction;
            LayerTable = layerTable;
            StyleTable = styleTable;
        }

        public void CreatePropriétaires(Property property, char suffix)
        {
            var contents = property.OwnersFr;
            property.Parcels.ForEach(p =>
                {
                    var poly = p.GetPolygon();
                    if (string.IsNullOrEmpty(contents)) contents = "\n";

                    var t = Functions.CreateTextForPropriétaires(poly, contents);
                    var id = p.Part == 0 ? p.PropertyId : p.PropertyId + suffix + "P" + p.Part;

                    if (p.Contenance < 400 && property.Parcels.Count > 1)
                        t.Contents = "{\\C1;" + id + "}\\P";
                    else
                        t.Contents = "{\\C1;" + (id + "}\\P" + "{\\C255;" + t.Contents).Trim("\n".ToCharArray()) + "}\\P";
                    t.Contents = t.Contents.Trim("\n".ToCharArray());
                    t.LayerId = LayerTable[Settings.Default.Prps];
                    t.TextStyleId = StyleTable["PE_" + Settings.Default.Prps];

                    ModelSpace.AppendEntity(t);
                    Transaction.AddNewlyCreatedDBObject(t, true);
                    t.Dispose();
                });
        }

        public void CreatePropertyIds(Parcel parcel, string suffix)
        {
            suffix = suffix.Trim();
            var centroid = parcel.GetPolygon().Centroid;
            var point = new Point3d(centroid.X, centroid.Y, centroid.Z);
            if (!Functions.PointInPolygon(parcel.GetP3DCollection(), point))
                centroid = Functions.GetBiggestConvexPolygon(parcel.Points.Select(p => p.Coordinate).ToList()).Centroid;

            var location = new Point3d(centroid.X, centroid.Y, 0);

            var contents = parcel.PropertyId + suffix + "P" + parcel.Part;
            contents = char.IsDigit(contents[0]) ? "Plle " + contents : contents;

            var id = new MText
            {
                Location = location,
                Attachment = AttachmentPoint.MiddleCenter,
                Contents = contents.EndsWith("P0")
                    ? contents.Remove(contents.LastIndexOf(suffix, StringComparison.CurrentCulture))
                    : contents,
                TextHeight = Settings.Default.PeTextHeightParId * 2000,
                LayerId = "TR".Contains(contents[0]) ? LayerTable[Settings.Default.Rqtr] : LayerTable[Settings.Default.PclId],
                TextStyleId = StyleTable["PE_" + Settings.Default.PclId]
            };

            ModelSpace.AppendEntity(id);
            Transaction.AddNewlyCreatedDBObject(id, true);
        }

        public void CreateParcelLimites(Parcel parcel)
        {
            var limites = parcel.GetPolyline();

            if ("TR".Contains(parcel.PropertyId[0]))
                limites.LayerId = LayerTable[Settings.Default.Lima];
            else
                limites.LayerId = LayerTable[Settings.Default.Lim];

            var oId = ModelSpace.AppendEntity(limites);
            Transaction.AddNewlyCreatedDBObject(limites, true);
            parcel.Handle = oId.Handle.Value;
            limites.Dispose();
        }

        public void CreateLengths(List<Parcel> parcels, LongOperationManager lom)
        {
            var lengths = new List<Int64[]>();
            parcels.ForEach(l => l.Points.ForEach(t =>
                {
                    var t1 = t;
                    var t2 = l.GetNext(t);

                    if (!t1.Name.StartsWith("B") || !t2.Name.StartsWith("B")) return;

                    if (lengths.Any(g => g.Contains(t1.PointId) && g.Contains(t2.PointId)))
                        return;

                    var vector = (t2.P3D - t1.P3D) / 2.0;
                    var rotation = Vector3d.XAxis.GetAngleTo(vector, Vector3d.ZAxis);

                    var text = new MText
                        {
                            Location =
                                t1.P3D + vector +
                                vector.RotateBy(-Math.PI / 2, Vector3d.ZAxis).MultiplyBy(.0015 * 2000 / vector.Length),
                            Contents = string.Format("({0:F2})", t1.P3D.DistanceTo(t2.P3D)),
                            Rotation = Math.Cos(rotation) >= 0.0 ? rotation : rotation + Math.PI,
                            TextHeight = .0015 * 2000,
                            Attachment = AttachmentPoint.MiddleCenter,
                            LayerId = LayerTable[Settings.Default.Lgth],
                            TextStyleId = StyleTable["PE_" + Settings.Default.Lgth],
                        };

                    ModelSpace.AppendEntity(text);
                    Transaction.AddNewlyCreatedDBObject(text, true);
                    if (!lom.Tick()) throw new Exception("Opération interrompu. Annulation ...");

                    lengths.Add(new[] { t1.PointId, t2.PointId });
                }));
        }

        internal void CreateRquisitionIds(Property property, char suffix)
        {
            foreach (var parcel in property.Parcels)
            {
                var centroid = parcel.GetPolygon().Centroid;
                var point = new Point3d(centroid.X, centroid.Y, centroid.Z);
                if (!Functions.PointInPolygon(parcel.GetP3DCollection(), point))
                    centroid = Functions.GetBiggestConvexPolygon(parcel.Points.Select(p => p.Coordinate).ToList()).Centroid;

                var location = new Point3d(centroid.X, centroid.Y + 1.5 * Settings.Default.PeTextHeightParId * 2000, 0);

                var contents = "R" + property.RequisitionId + suffix + "P" + parcel.Part;

                var id = new MText
                {
                    Location = location,
                    Attachment = AttachmentPoint.MiddleCenter,
                    Contents = contents.EndsWith("P0")
                        ? contents.Remove(contents.LastIndexOf("" + suffix, StringComparison.CurrentCulture))
                        : contents,
                    TextHeight = Settings.Default.PeTextHeightParId * 2000,
                    Color = Color.FromColor(System.Drawing.Color.Magenta),
                    LayerId = LayerTable[Settings.Default.Rqtr],
                    TextStyleId = StyleTable["PE_" + Settings.Default.PclId]
                };

                ModelSpace.AppendEntity(id);
                Transaction.AddNewlyCreatedDBObject(id, true);
            }
        }
    }
}
