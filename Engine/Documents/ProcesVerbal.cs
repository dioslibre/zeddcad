﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.DatabaseServices;
using Engine.Data.View;
using Engine.Utils;
using GeoAPI.Geometries;
using SQLite;

namespace Engine.Documents
{
    public class BorneTableEntry
    {
        public Point Borne { get; set; }
        public List<string> Description { get; set; }
    }

    public class RiverainsTableEntry
    {
        public string Riverain { get; set; }
        public string Description { get; set; }
    }

    public class ProcesVerbal
    {
        public Property Property { get; set; }
        public SQLiteConnection SqLite { get; set; }
        private Dictionary<Parcel, List<Parcel>> RiverainsParcels { get; set; }
        public Dictionary<Parcel, List<BorneTableEntry>> BorneTables { get; set; }
        public Dictionary<Parcel, List<RiverainsTableEntry>> RiverainsTables { get; set; }
        public IGeometry Hull { get; set; }
        public DateTime DateTime { get; set; }


        public ProcesVerbal(Property property, SQLiteConnection sqLite)
        {
            Property = property;
            SqLite = sqLite;

            Hull = Functions.GetConvexHull(property);
            RiverainsParcels = Functions.GetRiverainsParcels(property, Hull, sqLite);
            BorneTables = property.Parcels.ToDictionary(p => p,
                p => p.Points.Where(pt => pt.Name.StartsWith("B"))
                .Select(pt => new BorneTableEntry { Borne = pt, Description = new List<string>() })
                .ToList());
            RiverainsTables = property.Parcels.ToDictionary(p => p,
                p => new List<RiverainsTableEntry>());
            SetRiverainsAndDescriptions();
        }

        private void SetRiverainsAndDescriptions()
        {
            foreach (var parcel in Property.Parcels)
            {
                var parcelEntry = BorneTables[parcel];
                var riverains = RiverainsParcels[parcel];
                var freePoints = Functions.GetFreePoints(parcel, riverains);
                var piste = Functions.GetPisteForParcel(Property, parcel, RiverainsParcels, SqLite, freePoints);
                var limites = freePoints.Count == 0
                    ? new List<PassageInfo>()
                    : Functions.GetPisteWidth(parcel, Property, piste, RiverainsParcels, freePoints);

                var parcelId = 1000000;
                foreach (var limite in limites)
                {
                    Parcel pcl;
                    if (limite.Points.Any(p => p.Name.StartsWith("P")))
                    {
                        pcl = new Parcel
                        {
                            ParcelId = parcelId*limites.IndexOf(limite),
                            PropertyId = string.Format("Piste Publique de {0}m", limite.Width),
                            Points = new List<Point>(limite.Points)
                        };
                        riverains.Add(pcl);
                        limite.Parcel = pcl;
                    }
                    else if (limite.Points.Any(p => p.Name.StartsWith("O")))
                    {
                        pcl = new Parcel
                        {
                            ParcelId = parcelId * limites.IndexOf(limite),
                            PropertyId = "Oued",
                            Points = new List<Point>(limite.Points)
                        };
                        riverains.Add(pcl);
                        limite.Parcel = pcl;
                    }
                    else if (limite.Points.Any(p => p.Name.StartsWith("C")))
                    {
                        pcl = new Parcel
                        {
                            ParcelId = parcelId * limites.IndexOf(limite),
                            PropertyId = "Chaàbat",
                            Points = new List<Point>(limite.Points)
                        };
                        riverains.Add(pcl);
                        limite.Parcel = pcl;
                    }
                    else if (limite.Points.Any(p => p.Name.StartsWith("R")))
                    {
                        pcl = new Parcel
                        {
                            ParcelId = parcelId * limites.IndexOf(limite),
                            PropertyId = "Route N°  de 20m",
                            Points = new List<Point>(limite.Points)
                        };
                        riverains.Add(pcl);
                        limite.Parcel = pcl;
                    }
                    else
                    {
                        pcl = new Parcel
                        {
                            ParcelId = parcelId * limites.IndexOf(limite),
                            PropertyId = "Inconnu",
                            Points = new List<Point>(limite.Points)
                        };
                        riverains.Add(pcl);
                        limite.Parcel = pcl;
                    }
                }

                var dictionary = parcel.Points
                    .ToDictionary(p => p, p => riverains.Where(r => r.Points.Contains(p)).ToList());

                foreach (var point in dictionary.Keys)
                {
                    if (!point.Name.StartsWith("B")) continue;
                    var rivs = dictionary[point];

                    var nextb = parcel.GetNextBorne(point);
                    var next = parcel.GetNext(point);
                    var nrivs = dictionary[nextb];
                    var prevb = parcel.GetPreviousBorne(point);
                    var privs = dictionary[prevb];

                    var pointEntry = parcelEntry.First(e => e.Borne.Equals(point));

                    foreach (var riv in rivs)
                    {
                        var passage = limites.FirstOrDefault(l => l.Parcel.Equals(riv)) ?? new PassageInfo();
                        if (riv.PropertyId.StartsWith("P"))
                        {
                            pointEntry.Description.Insert(0, string.Format("sur le bord d'emprise {1} de la {0} ", riv.PropertyId.ToLower(), 
                                passage.Side));
                            pointEntry.Description.RemoveAll(p => p == "en Terrain Nu ");
                        }

                        else if (riv.PropertyId.StartsWith("O"))
                        {
                            if(pointEntry.Description.All(d => !d.Contains("oued")))
                            pointEntry.Description.Insert(0, string.Format("à 2m du front bord {0} de l'oued ", passage.Side));
                            pointEntry.Description.RemoveAll(p => p == "en Terrain Nu ");
                        }

                        else if (riv.PropertyId.StartsWith("C"))
                        {
                            if(pointEntry.Description.All(d => !d.Contains("chaàbat")))
                            pointEntry.Description.Insert(0, string.Format("sur le bord {0} d'une chaàbat ", passage.Side));
                            pointEntry.Description.RemoveAll(p => p == "en Terrain Nu ");
                        }

                        else if (riv.PropertyId.StartsWith("Rou"))
                        {
                            pointEntry.Description.Insert(0, string.Format("sur le bord d'emprise {1} de la {0} ", riv.PropertyId.ToLower(), 
                                passage.Side));
                            pointEntry.Description.RemoveAll(p => p == "en Terrain Nu ");
                        }

                        else if ("TR".Contains(riv.PropertyId[0]))
                        {
                            pointEntry.Description.Add("en Terrain Nu ");
                            pointEntry.Description.Add(string.Format("commune avec la ({0}) de {1} "
                                , riv.Points.First(p => p.Equals(point)).Name, riv.PropertyId));
                        }
                        else if (pointEntry.Description.All(d => d.IndexOfAny("sà".ToCharArray()) != 0))
                            pointEntry.Description.Add("en Terrain Nu ");
                    }

                    var nold = nrivs.FirstOrDefault(r => "TR".Contains(r.PropertyId[0]) && !r.Points.Contains(point));
                    if (nold != null)
                    {
                        var npt = nold.GetNext(nextb);
                        var line = new Line(npt.P3D, nextb.P3D);
                        var d = point.P3D.DistanceTo(line.GetClosestPointTo(point.P3D, false));
                        line.Dispose();
                        if (d <= 0.2)
                        {
                            var name = nold.Points.First(p => p.Equals(nextb)).Name;
                            pointEntry.Description.Add(string.Format("sur alignement entre ({0}) et ({1}) de {2}.",
                                    npt.Name, name, nold.PropertyId));
                        }
                    }

                    var pold = privs.FirstOrDefault(r => "TR".Contains(r.PropertyId[0]) && !r.Points.Contains(point));
                    if (pold != null)
                    {
                        var ppt = pold.GetNext(prevb);
                        var line = new Line(ppt.P3D, prevb.P3D);
                        var d = point.P3D.DistanceTo(line.GetClosestPointTo(point.P3D, false));
                        line.Dispose();
                        if (d <= 0.2)
                        {
                            var name = pold.Points.First(p => p.Equals(nextb));
                            pointEntry.Description.Add(string.Format("sur alignement entre ({0}) et ({1}) de {2}.",
                                    ppt.Name, name, pold.PropertyId));
                        }
                    }

                    var rivEntry = new RiverainsTableEntry();
                    var nshared = rivs.Intersect(nrivs).FirstOrDefault();
                    if (nshared != null)
                    {
                        var passage = limites.FirstOrDefault(l => l.Parcel.Equals(nshared)) ?? new PassageInfo();
                        if (char.IsDigit(nshared.PropertyId[0]) || nshared.PropertyId.Contains("/"))
                        {
                            rivEntry.Description = next.Name.StartsWith("D") 
                                ? "Limite Naturelle suivant une depression." 
                                : "Limite Rectiligne en Terrain Nu.";
                            rivEntry.Riverain = string.IsNullOrEmpty(nshared.RequisitionId)
                                ? nshared.PropertyId
                                : "R" + nshared.RequisitionId;
                            rivEntry.Riverain = nshared.Part == 0
                                ? rivEntry.Riverain
                                : rivEntry.Riverain + "-P" + nshared.Part;
                            rivEntry.Riverain = char.IsDigit(rivEntry.Riverain[0])
                                ? "Plle " + rivEntry.Riverain
                                : rivEntry.Riverain;
                        }
                        if (nshared.PropertyId.StartsWith("P"))
                        {
                            rivEntry.Description = string.Format("Limite Naturelle suivant le bord d'emprise {1} de la {0}.",
                                nshared.PropertyId, passage.Side);
                            rivEntry.Riverain = nshared.PropertyId;
                        }
                        else if (nshared.PropertyId.StartsWith("C"))
                        {
                            rivEntry.Description = rivEntry.Description = string.Format("Limite Naturelle suivant le bord {0} d'une chaàbat.",
                                passage.Side);
                            rivEntry.Riverain = nshared.PropertyId;
                        }
                        else if (nshared.PropertyId.StartsWith("O"))
                        {
                            rivEntry.Description = string.Format("Limite Naturelle suivant le front bord {0} de l'oued.", passage.Side);
                            rivEntry.Riverain = nshared.PropertyId;
                        }
                        else if (nshared.PropertyId.StartsWith("Rou"))
                        {
                            rivEntry.Description = string.Format("Limite Naturelle suivant le bord d'emprise {1} de la {0}.",
                                nshared.PropertyId, passage.Side);
                            rivEntry.Riverain = nshared.PropertyId;
                        }
                    }
                    else if(rivs.Any())
                    {
                        nshared = rivs[0];
                        var passage = limites.FirstOrDefault(l => l.Parcel.Equals(nshared)) ?? new PassageInfo();
                        if (nshared.PropertyId.StartsWith("P"))
                        {
                            rivEntry.Description = string.Format("Limite Naturelle suivant le bord d'emprise {1} de la {0}.",
                                nshared.PropertyId, passage.Side);
                            rivEntry.Riverain = nshared.PropertyId;
                        }
                        else if (nshared.PropertyId.StartsWith("C"))
                        {
                            rivEntry.Description = rivEntry.Description = "Limite Naturelle suivant une depression.";
                            rivEntry.Riverain = nshared.PropertyId;
                        }
                        else if (nshared.PropertyId.StartsWith("O"))
                        {
                            rivEntry.Description = string.Format("Limite Naturelle suivant le front bord {0} de l'oued.", passage.Side);
                            rivEntry.Riverain = nshared.PropertyId;
                        }
                        else if (nshared.PropertyId.StartsWith("Rou"))
                        {
                            rivEntry.Description = string.Format("Limite Naturelle suivant le bord d'emprise {0} de la {0}.", passage.Side);
                            rivEntry.Riverain = nshared.PropertyId;
                        }
                    }
                    RiverainsTables[parcel].Add(rivEntry);
                }
            }
        }

        public int GetSecondPageCount(Parcel parcel, int rowCount)
        {

            var numberOfBornes = BorneTables[parcel].Count * 2;
            return numberOfBornes % rowCount == 0
                ? numberOfBornes / rowCount
                : numberOfBornes / rowCount + 1;
        }
    }
}
