// stdafx.h�: fichier Include pour les fichiers Include syst�me standard,
// ou les fichiers Include sp�cifiques aux projets qui sont utilis�s fr�quemment,
// et sont rarement modifi�s
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclure les en-t�tes Windows rarement utilis�s
// Fichiers d'en-t�te Windows�:
#include <windows.h>

// TODO: faites r�f�rence ici aux en-t�tes suppl�mentaires n�cessaires au programme
#include "ImageContext.h"
#include "RowProviderInterface.h"
#include "FileReadDescriptor.h"
#include "FileWriteDescriptor.h"
#include "FileWriteDescriptorRep.h"
#include "FileSpecifier.h"
#include "StringBuffer.h"
#include "PngFormatCodec.h"
#include "Image.h"
#include "FileWriteDescriptor.h"
#include "RowProviderInterface.h"
#include "FileSpecifier.h"
#include "DataModelAttributes.h"
#include "DataModel.h"
#include "Image.h"

#include <JfifFormatCodec.h>
#include <PngFormatCodec.h>
#include <TiffFormatCodec.h>
#include <BmpFormatCodec.h>
#include <TiffCustomProperties.h>
#include <PngCustomProperties.h>
#include <FileReadDescriptor.h>
#include <RgbPaletteModel.h>
#include <BitonalModel.h>
#include <RgbGrayModel.h>