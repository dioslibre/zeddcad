#include "stdafx.h"
#include "CropLib.h"
#include <atlstr.h>
#include <string>
#include <iosfwd>

bool ATILSaveImage(Atil::RowProviderInterface *pPipe, CString &fileName, const CString fileTypeUpper);

CROPLIB_API int __cdecl fnCropLib(void* img, char* filename, int rectangle[])
{
	try
	{
		Atil::Image* pImage = (Atil::Image*)img;

		Atil::Offset upperLeft(rectangle[0], rectangle[1]);

		Atil::Size size(rectangle[2], rectangle[3]);

		Atil::RowProviderInterface* pRpi = pImage->read(size, upperLeft);

		CString s(filename);

		ATILSaveImage(pRpi, s, "PNG");
	}
	catch (Atil::ATILException *pExp)
	{
		delete pExp;
		return 1;
	}

	return 0;
}

bool ATILSaveImage(Atil::RowProviderInterface *pPipe, CString &fileName, const CString fileTypeUpper)
{
	bool savedOK = false;

	if (pPipe != NULL)
	{
		Atil::ImageFormatCodec *pCodec = new PngFormatCodec();

		if (Atil::FileWriteDescriptor::isCompatibleFormatCodec(pCodec, &(pPipe->dataModel()), pPipe->size()))
		{
			Atil::FileWriteDescriptor fileWriter(pCodec);
			Atil::FileSpecifier fs(Atil::StringBuffer((fileName.GetLength() + 1) * sizeof(TCHAR),
				(const Atil::Byte *)fileName.GetString(), Atil::StringBuffer::kUTF_16), Atil::FileSpecifier::kFilePath);
			
			if (fileWriter.setFileSpecifier(fs))
			{
				fileWriter.createImageFrame(pPipe->dataModel(), pPipe->size());
				
				Atil::FormatCodecPropertyInterface *pProp = fileWriter.getProperty(Atil::FormatCodecPropertyInterface::kCompression);
				if (pProp != NULL)
				{
					PngCompression *pComp = dynamic_cast<PngCompression*>(pProp);
					if (pComp != NULL)
					{
						pComp->selectCompression(PngCompressionType::kHigh);
						fileWriter.setProperty(pComp);
					}
					
					delete pProp;
					pProp = NULL;
				}
			}
			else
			{
			}

			Atil::FormatCodecPropertySetIterator* pPropsIter = fileWriter.newPropertySetIterator();
			
			if (pPropsIter)
			{
				for (pPropsIter->start(); !pPropsIter->endOfList(); pPropsIter->step())
				{
					Atil::FormatCodecPropertyInterface* pProp = pPropsIter->openProperty();
					if (pProp->isRequired())
						fileWriter.setProperty(pProp);

					pPropsIter->closeProperty();
				}
				delete pPropsIter;
			}

			try
			{
				fileWriter.writeImageFrame(pPipe);
				savedOK = true;
			}
			catch (Atil::ATILException *pExp)
			{
				savedOK = false;
				if (pExp != NULL)
					delete pExp;
			}

		}
		else
			savedOK = false;
	}

	return savedOK;
}
