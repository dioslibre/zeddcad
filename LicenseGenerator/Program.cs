﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using LicenseGenerator.Helpers;
using Portable.Licensing;

namespace LicenseGenerator
{
    class Program
    {
        static void Main()
        {
            try
            {
                Console.WriteLine(string.Format("[{0}] : GET MACHINE NAME.", DateTime.Now));
                var passPhrase = Environment.MachineName;

                Thread.Sleep(500);
                Console.WriteLine(string.Format("[{0}] : CREATE KEYGENERATOR.", DateTime.Now));
                var keyGenerator = Portable.Licensing.Security.Cryptography.KeyGenerator.Create();

                Thread.Sleep(500);
                Console.WriteLine(string.Format("[{0}] : GENERATE KEYPAIR.", DateTime.Now));
                var keyPair = keyGenerator.GenerateKeyPair();

                Thread.Sleep(500);
                Console.WriteLine(string.Format("[{0}] : GET PRIVATE KEY.", DateTime.Now));
                var privateKey = keyPair.ToEncryptedPrivateKeyString(passPhrase);

                Thread.Sleep(500);
                Console.WriteLine(string.Format("[{0}] : GET PUBLIC KEY.", DateTime.Now));
                var publicKey = keyPair.ToPublicKeyString();

                Thread.Sleep(500);
                Console.WriteLine(string.Format("[{0}] : GENERATE LICENSE.", DateTime.Now));
                var license = License.New()
                    .CreateAndSignWithPrivateKey(privateKey, passPhrase);

                Thread.Sleep(500);
                Console.WriteLine(string.Format("[{0}] : SAVE LICENSE TO DISK.", DateTime.Now));
                File.WriteAllText("License.lic", license.ToString(), Encoding.UTF8);

                Thread.Sleep(500);
                Console.WriteLine(string.Format("[{0}] : SAVE PUBLIC KEY TO DISK.", DateTime.Now));
                File.WriteAllText("PublicKey.key", publicKey, Encoding.UTF8);

                Thread.Sleep(500);
                Console.WriteLine(string.Format("[{0}] : ALL DONE. PRESS ANY KEY TO EXIT", DateTime.Now));

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            Console.Read();
        }
    }
}
