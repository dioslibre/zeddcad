﻿namespace LicenseGenerator.Helpers
{
    class UsbDeviceInfo
    {
        public UsbDeviceInfo(string letter, string serialNumber)
        {
            SerialNumber = serialNumber;
            Letter = letter;
        }
        public string SerialNumber { get; private set; }
        public string Letter { get; private set; }
    }
}
